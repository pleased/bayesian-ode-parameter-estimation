%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

   %%%% Algebraic Dynamic Model -  Nonlinear Regression Application %%%%         

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%% Case Study 1: Isothermal, Constant Volume CSTR (Example 3.2)

%%%% Reference: Marlin, T E. 2000. Process Control: Designing Processes and
%%%% Control Systems for Dynamic Performance. 2nd ed. Boston: McGraw-Hill.

%% Clear Memory and Command Window

close all;
clear;
clc;

%% Isothermal, Constant Volume CSTR Deterministic Function

% -> Derived From First-Principles (Conservation of Mass)
% -> Assumed: Perfect Step Change Input Disturbance in CA0 (Noise-Free)
% -> Algebraic Dynamic Model Available For Perfect Step Input Disturbance
% -> Reaction Kinetics: First Order

% Algebraic Dynamic Model Simulation Information

V = 2.1;% m^3 - Reactor Volume
F = 0.085; % m^3/min - Reactor Inlet/Outlet Flowrate
CA0_initial = 0.925; % - mole/m^3 Initial Reactor Inlet Concentration
delta_CA0 = 0.925; % - mole/m^3 Disturbance Step Change Magnitude in Reactor Inlet Concentration
k_reaction = 0.040; % - min^-1 Reaction Rate Constant
CA_initial = (F/(F + (k_reaction*V)))*CA0_initial; % - Initial Reactor Outlet Concentration

% Close-Form Solution For Unknown Algebraic Model Parameters

Kp = (F/(F + (k_reaction*V))); % - (mole/m^3)/(mole/m^3) Process Gain
tau = (V/(F + (k_reaction*V))); % - (min) Process Time Constant

% Model Simulation Time Limits

Upper_time_limit = 80; % min
Lower_time_limit = 0; % min
t = linspace(Lower_time_limit,Upper_time_limit,100);

% Explicit Algebraic Dynamic Model for Step Input Disturbance in CA0

CA = @(t) CA_initial + (Kp*delta_CA0*(1 - exp(-t/tau)));

% Visualise Explicit Algebraic Dynamic Model Deterministic Response

figure(1)
plot(t,CA(t),'b--','LineWidth',2)
%title('CSTR Dynamic Response: Step Change in C_A_0')
xlabel('Time (min)')
ylabel('C_A (mole/m^3)')
hold on
set(gca,'Box','off')

% Print Simulation Ground Truth Parameter Values

fprintf('\nTrue Parameter Values (Set During Simulation)\n\n')
fprintf('\tProcess Gain: %0.3f (dimensionless)\n',Kp)
fprintf('\tProcess Time Constant: %0.2f (min)\n',tau)

%% Data Generation Process

% -> Sampling Time Set To 4 Minutes (Sensor Measurement Every 4 Minutes)
% -> Sensor Measurements Taken Over an 80 Minute Data Collection Period
% -> Data Set Size: N = 20 Sensor Measurements

% Sensor Measurement Bounds

Sensor_upper_bound = Upper_time_limit;
Sensor_lower_bound = Lower_time_limit;

rng(0,'Twister')% Ensure Repeatability of Results

% Two-tail alpha, z-score and F value (Fisher F-Distribution)

z_score = 2.576; % z_score value - 99% Confidende/Credibility
alpha = 2.878; % t-table
F = 6.01; % F-Value From Fisher Distribution Table

% Generate Sensor Measurements

N = 20; % Number of Sensor Measurements Taken Over 80 Minute Data Collection Period
t_sensor = linspace(Sensor_lower_bound,Sensor_upper_bound,N)';
sensor_standard_deviation = 0.015; % Sensor Standard Deviation Parameter

fprintf('\tNoise Standard Deviation: %0.4f (mole/m^3)',sensor_standard_deviation)

% Addition Of Sensor Noise To Deterministic Function Data Set

CA_measured = CA(t_sensor) + normrnd(0,sensor_standard_deviation,N,1);

% Visualise Sensor Measurements Over The 80 Minute Data Collection Period

plot(t_sensor,CA_measured,'kx','MarkerSize',10,'LineWidth',2)
legend('Underlying Algebraic Dynamic Model','Sensor Measurements','Location','SouthEast')
legend('boxoff') 
set(gca,'Box','off')
hold on

%% Initial Model Parameter Guess and Number of Algorithm Iterations Used

% -> Conditions Used For:
      % (1) Variational Bayesian Nonlinear Regression (Algorithm 3)
      % (2) Nonlinear Least Squares (Gauss-Newton Implementation) (Algorithm 1)

% Randomly Initialise Unknown Model Parameters

rng('Shuffle','Twister')
Parameter_1 = ((1/10)*(Kp)) + (((10)*(Kp)) - ((1/10)*(Kp))).*rand(1,1);
rng('Shuffle','Twister')
Parameter_2 = ((1/10)*(tau)) + (((10)*(tau)) - ((1/10)*(tau))).*rand(1,1);

% Populate Vector Containing Initial Model Parameter Guess

Parameter_Guess = [Parameter_1;Parameter_2];

fprintf('\n\nInitial Parameter Guess [%0.4f,%0.4f]',Parameter_1,Parameter_2)

% -> Number Of Trial Iterations:
    % (2) Nonlinear Least Squares (Gauss-Newton Implementation) (Algorithm 1)
    
n = 2000;

% -> Empirical Iterations/Trials
    % (2) Nonlinear Least Squares (Gauss-Newton Implementation) (Algorithm 1)
    
max_trials = 500;

%% Variational Bayesian Nonlinear Regression 
%  Algorithm 3

% Start Algorithm Execution Stopwatch

tic

% Initialise Prior Hyperparameters

c0 = (10)^-6;
s0 = (10)^6;
m0 = Parameter_Guess;
A0 = [10^-2 0;0 10^-2];

% Randomly Initialise Posterior Variational Hyperparameters

mn_old = Parameter_Guess;
rng('Shuffle','Twister')
cn = ((1/10)*((10)^-5)) + (((10)*((10)^-5)) - ((1/10)*((10)^-5))).*rand(1,1);
rng('Shuffle','Twister')
sn = ((1/10)*((10)^5)) + (((10)*((10)^5)) - ((1/10)*((10)^5))).*rand(1,1);
rng('Shuffle','Twister')
An_diagonal = ((1/10)*(10^-1)) + (((10)*(10^-1)) - ((1/10)*(10^-1))).*rand(1,1);
An =  [An_diagonal 0;0 An_diagonal];

% Dimension of Parameters Space

M = length(m0);

% Tolerence to Establish Convergence

tol = 10^-08;

% Pre-allocate Memory For ELBO

ELBO = zeros(n,1);
ELBO(1,1) = -Inf;

% Perform Variational Bayesian Nonlinear Regression

for i = 1:1:n
    
    % Linearise Model About The Posterior Distribution Mode (Bivariate Gaussian)
    
    k = CA_measured - (CA_initial + (mn_old(1,1)*delta_CA0*(1 - exp(-t_sensor/mn_old(2,1)))));
    
    % Evaluate Jacobian Matrix Entries (Vectorised)
    
    J =  [-delta_CA0*(exp(-t_sensor/mn_old(2,1)) - 1) -(mn_old(1,1)*delta_CA0*t_sensor.*exp(-t_sensor/mn_old(2,1)))/mn_old(2,1)^2];
    
    % Evaluate ELBO (Evidence Lower Bound) 
    
    ELBO(i + 1,1) = ((N/2)*(psi(cn) + log(sn))) - ((N/2)*(log(2*pi)))...
        - (((cn*sn)/2)*(((k')*k) + (trace(An\((J')*J))))) ...
        - ((M/2)*(log(2*pi))) + ((1/2)*(log(det(A0)))) + ((M/2)*(psi(cn) + log(sn))) ...
        - (((cn*sn)/2)*((((mn_old - m0)')*A0*(mn_old - m0)) + (trace(An\(A0))))) ...
        - (gammaln(c0)) + ((c0 - 1)*(psi(cn) + log(sn))) - (c0*log(s0)) - ((cn*sn)/s0) ...
        + ((1/2)*(log(det(inv(An))))) + ((M/2)*(1 + log(2*pi))) ...
        + (cn) + (log(sn)) + (gammaln(cn)) - ((cn - 1)*(psi(cn)));
    
    if abs(ELBO(i + 1,1) - ELBO(i,1)) <= tol % Test ELBO Monotonic Condition
        
        break;
        
    elseif ELBO(i + 1,1) > ELBO(i,1) % Test ELBO Monotonic Condition
        
        % Updates for Variational Posterior Approximations
        
        % - q(Beta)
        % - q(Omega)
    
        % Update q(Beta) - Gamma Distribution
        
        cn = (M/2) + (N/2) + c0  ;
        sn = 1/((1/s0) + (0.5*(k')*k) + (0.5*trace((An\(J')*J) + (An\A0))) + (0.5*(((mn_old - m0)')*A0*(mn_old - m0))));
    
        % Update q(Omega) - Gaussian Distribution
        
        An = sn*cn*(((J')*J) + A0);
                
         for z = 1:1:50 % Update Intial Model Parameter Guess
                
            mn_new = (An)\((sn*cn*(((J')*(k + (J*mn_old))) + (A0*m0))));            
            
                if abs(m0 - mn_new) < ones(length(m0),1)*1e-03

                    break;

                end
                
                m0 = mn_new;
                
        end       
    
        % Update mn_old With mn_new
        
        mn_old = mn_new;
        
        % Initialise Empirical Trial Counter
        
        trials = 0;
    
     elseif (ELBO(i + 1,1) < ELBO(i,1)) && (trials <= max_trials) % Test ELBO Monotonic Condition
        
        % Save Current Posterior Variational Hyperparameter Soutions
        
        Posterior_Hyperparameters.cn(i,1) = cn;
        Posterior_Hyperparameters.sn(i,1) = sn;
        Posterior_Hyperparameters.mn_new(i) = {mn_new};
        Posterior_Hyperparameters.An(i) = {An};
        Posterior_Hyperparameters.ELBO(i,1) = ELBO(i + 1,1);   
     
        % Updates for Variational Posterior Approximation
        
        % - q(Beta)
        % - q(Omega)   
    
        % Update q(Beta) - Gamma Distribution
        
        cn = (M/2) + (N/2) + c0  ;
        sn = 1/((1/s0) + (0.5*(k')*k) + (0.5*trace((An\(J')*J) + (An\A0))) + (0.5*(((mn_old - m0)')*A0*(mn_old - m0))));
    
        % Update q(Omega) - Gaussian Distribution
        
        An = sn*cn*(((J')*J) + A0); 
        
        for z = 1:1:50 % Update Intial Model Parameter Guess
                
            mn_new = (An)\((sn*cn*(((J')*(k + (J*mn_old))) + (A0*m0))));            
            
                if abs(m0 - mn_new) < ones(length(m0),1)*1e-03

                    break;

                end
                
                m0 = mn_new;
               
        end   
        
        % Update mn_old With mn_new
        
        mn_old = mn_new;

        % Counter Update For Empirical Trials
        
        trials = trials + 1;
        
    else 
        
        % Revert Back To Saved Solutions
        break;
          
    end        
    
end

% End Of Algorithm Execution Time Stopwatch

t_end_Bayes = toc;

% Inferred Isothermal, Constant Volume CSTR Parameters

Bayesian_Model_Parameters = mn_new;

% Visualise Algebraic Dynamic Model Deterministic Response With
% The Bayesian MAP Estimate For The Model Parameters

figure(1)
plot(t,CA_initial + (Bayesian_Model_Parameters(1,1)*delta_CA0*(1 - exp(-t/Bayesian_Model_Parameters(2,1)))),'r','LineWidth',2)
legend('Underlying Algebraic Dynamic Model','Sensor Measurements','Algebraic Dynamic Model (MAP Estimate)','Location','SouthEast')
legend('boxoff') 
set(gca,'Box','off')
hold on

% Visualise If Evidence Lower Bound (ELBO) Has Converged

figure(2)
plot(1:i,ELBO(2:i + 1,1),'kx--','LineWidth',2,'MarkerSize',12)
title('Evidence Lower Bound')
xlabel('p^t^h iteration')
ylabel('ELBO (\Omega,\beta)')
set(gca,'XTick',1:i)
axis(([1 i -Inf max(ELBO)*500]))
set(gca,'Box','off')

% Posterior Parameter Covariance Matrix

Sn = inv(An);

%% Statistical Properties: Variational Bayesian Nonlinear Regression
%  Algorithm 3

% % Ground Truth Comparison - Quality Of Parameter Estimates

fprintf('\n\nBayesian Results (based on MAP Estimates)\n\n')
fprintf('\tProcess Gain: %0.3f (dimensionless)\n',Bayesian_Model_Parameters(1,1))
fprintf('\tProcess Time Constant: %0.2f (min)',Bayesian_Model_Parameters(2,1))
fprintf('\n\tPrecision Parameter Estimate: %0.1f (m^6/mole^2)',((cn - 1)*sn))
fprintf('\n\tNoise Variance: %0.7f (mole^2/m^6)',1/((cn - 1)*sn))
fprintf('\n\tNoise Standard Deviation: %0.4f (mole/m^3)\n',sqrt(1/((cn - 1)*sn)))

fprintf('\n\tProcess Gain %% Error: %0.2f%% \n',((abs(Bayesian_Model_Parameters(1,1) - Kp))/(Kp))*100)
fprintf('\tProcess Time Constant %% Error: %0.2f%% \n',((abs(Bayesian_Model_Parameters(2,1) - tau))/(tau))*100)
fprintf('\tNoise Standard Deviation %% Error: %0.2f%% \n',((abs((sqrt(1/((cn - 1)*sn)))- sensor_standard_deviation))/(sensor_standard_deviation))*100)

% Parameter Marginal Credibility Interval

COV_Bayes_Parameters = diag(Sn);

fprintf('\n\t99%% Marginal Credibility Interval For The Progress Gain: [%0.3f (dimensionless), %0.3f (dimensionless)]',Bayesian_Model_Parameters(1,1) - z_score*sqrt(COV_Bayes_Parameters(1,1)),Bayesian_Model_Parameters(1,1) + z_score*sqrt(COV_Bayes_Parameters(1,1)))
fprintf('\n\t99%% Marginal Credibility Interval For The Progress Time Constant: [%0.2f min, %0.2f min]',Bayesian_Model_Parameters(2,1) - z_score*sqrt(COV_Bayes_Parameters(2,1)),Bayesian_Model_Parameters(2,1) + z_score*sqrt(COV_Bayes_Parameters(2,1)))
fprintf('\n\t99%% Marginal Credibility Interval For The Precision Parameter: [%0.1f (m^6/mole^2), %0.1f (m^6/mole^2)]',((cn - 1)*sn)/3,2.028*((cn - 1)*sn))

% -> Visualise Parameter 1 Inference Results

figure(3)

% Visualise Marginal Posterior Distribution Over Parameters 1

x = linspace(0.94*Bayesian_Model_Parameters(1,1),1.06*Bayesian_Model_Parameters(1,1),1000);
plot(x,normpdf(x,Bayesian_Model_Parameters(1,1),sqrt(COV_Bayes_Parameters(1,1))) + 13,'r','LineWidth',2)
ylim([0 80])
xlim([0.49 0.55])
hold on

% Visualise 99% Credibility Interval For Parameter 1

x = Bayesian_Model_Parameters(1,1);
y = 5;
errhigh = z_score*sqrt(COV_Bayes_Parameters(1,1));
errlow =  z_score*sqrt(COV_Bayes_Parameters(1,1));
errorbar(x,y,errlow,errhigh,'horizontal','ro','LineWidth',2,'MarkerFaceColor','r','MarkerSize',8)
hold on
plot(Kp,y,'bx','LineWidth',3,'MarkerFaceColor','b','MarkerSize',10)
text(Bayesian_Model_Parameters(1,1),10,'K_p_,_M_A_P','fontsize',12)
text(Kp,10,'K_p_,_T_r_u_e','fontsize',12)
xlabel('K_p (dimensionless)')
set(gca,'YTick',[],'Box','off')
set(gca,'ycolor',[1 1 1])

% -> Visualise Parameter 2 Inference Results

figure(4)

% Visualise Marginal Posterior Distribution Over Parameters 2

x = linspace(0.94*Bayesian_Model_Parameters(2,1),1.06*Bayesian_Model_Parameters(2,1),1000);
plot(x,normpdf(x,Bayesian_Model_Parameters(2,1),sqrt(COV_Bayes_Parameters(2,1))) + 0.5,'r','LineWidth',2)
ylim([0 2.5])
xlim([12.4 14])
hold on

% Visualise 99% Credibility Interval For Parameter 2

x = Bayesian_Model_Parameters(2,1);
y = 0.2;
errhigh = z_score*sqrt(COV_Bayes_Parameters(2,1));
errlow =  z_score*sqrt(COV_Bayes_Parameters(2,1));
errorbar(x,y,errlow,errhigh,'horizontal','ro','LineWidth',2,'MarkerFaceColor','r','MarkerSize',8)
hold on
plot(tau,y,'bx','LineWidth',3,'MarkerFaceColor','b','MarkerSize',10)
text(Bayesian_Model_Parameters(2,1),0.32,'\tau_M_A_P','fontsize',12)
text(tau,0.32,'\tau_T_r_u_e','fontsize',12)
xlabel('\tau (min)')
set(gca,'YTick',[],'Box','off')
set(gca,'ycolor',[1 1 1])

% Algorithm Execution Time

fprintf('\n\n\tElapsed Algorithm Time: %0.4f seconds\n',t_end_Bayes)

% Joint Parameter Credibility Interval
% -> Ellipsoid Representation

figure(5)
[EigenVec, EigenVal] = eig(Sn); % Calculate Eigenvalues And Eigenvectors

[Largest_EigenVec_Index, Num] = find(EigenVal == max(max(EigenVal))); % Obtain The Index Of The Largest Eigenvector
Largest_EigenVec = EigenVec(:,Largest_EigenVec_Index);

Largest_EigenVal = max(max(EigenVal)); % Obtain The Largest Eigenvalue

if (Largest_EigenVec_Index == 1) % Obtain The Smallest Eigenvector And EigenValue
    
    Smallest_Eigen_Val = max(EigenVal(:,2));
    Smallest_Eigen_Vec = EigenVec(:,2);
    
else
    
    Smallest_Eigen_Val = max(EigenVal(:,1));
    Smallest_Eigen_Vec = EigenVec(:,1);
    
end

Angle = atan2(Largest_EigenVec(2),Largest_EigenVec(1)); % Calculate The Angle Between The Cartesian x-axis And The Largest Eigenvector

if Angle < 0
    
    Angle = Angle + 2*pi; % Shift Angle To Be Between 0 and 2pi
    
end

Average = Bayesian_Model_Parameters'; % Extract Mean Inferred Parameter Values

Chi_Value = sqrt(9.210); % Obtain Radius For The Error Ellipse
Theta_Grid = linspace(0,2*pi,200000);
Kp_Mean = Average(1);
tau_Mean = Average(2);
A1 = Chi_Value*sqrt(Largest_EigenVal);
B1 = Chi_Value*sqrt(Smallest_Eigen_Val);

Ellipse_X_r = A1*cos(Theta_Grid); % Ellipse Cartesian x-axis Coordinates
Ellipse_Y_r = B1*sin(Theta_Grid); % Ellipse Cartesian y-axis Coordinates

R = [cos(Angle) sin(Angle); -sin(Angle) cos(Angle)]; % Ellipse Rotation Matrix

r_Ellipse = [Ellipse_X_r;Ellipse_Y_r]'*R; % Rotate Ellipse

Bayes_Ellipse = plot(r_Ellipse(:,1) + Kp_Mean,r_Ellipse(:,2) + tau_Mean,'r','LineWidth',3); % Visualise Ellipse
hold on
plot(Bayesian_Model_Parameters(1,1),Bayesian_Model_Parameters(2,1),'o','Color','r','LineWidth',2,'MarkerSize',8,'MarkerFaceColor','r')
%title('99% Joint Parameter Parameter Credibility Region')
ylabel('\tau (min)')
xlabel('K_p (dimensionless)')
set(gca,'Box','off')
hold on

% Visualise The Expected Mean Response

figure(6)
mean_response_range = linspace(Lower_time_limit,Upper_time_limit,100)';

% -> Evaluate Jacobian Matrix (Vectorised)

J = [-delta_CA0*(exp(-mean_response_range/Bayesian_Model_Parameters(2,1)) - 1) -(Bayesian_Model_Parameters(1,1)*delta_CA0*mean_response_range.*exp(-mean_response_range/Bayesian_Model_Parameters(2,1)))/(Bayesian_Model_Parameters(2,1)^2)];

% -> Evaluate Covariance

Cov_Bayes = J*((An)\((J)'));

S2 = diag(Cov_Bayes);
f = [(CA_initial + (Bayesian_Model_Parameters(1,1)*delta_CA0*(1 - exp(-mean_response_range/Bayesian_Model_Parameters(2,1))))) + z_score*sqrt(S2);flip((CA_initial + (Bayesian_Model_Parameters(1,1)*delta_CA0*(1 - exp(-mean_response_range/Bayesian_Model_Parameters(2,1))))) - z_score*sqrt(S2),1)];
h = fill([mean_response_range; flip(mean_response_range,1)], f, [6 6 6]/10, 'EdgeColor', [5 5 5]/8);
set(h,'facealpha',1);
hold on
plot(t,CA_initial + (Bayesian_Model_Parameters(1,1)*delta_CA0*(1 - exp(-t/Bayesian_Model_Parameters(2,1)))),'r','LineWidth',2)
hold on
plot(t_sensor,CA_measured,'kx','MarkerSize',10,'LineWidth',2)
hold on
plot(t,CA(t),'b--','LineWidth',2)
hold on
%title('CSTR Dynamic Response: Step Change in C_A_0')
xlabel('Time (min)')
ylabel('C_A (mole/m^3)')
legend('99% Credibility Interval','Algebraic Dynamic Model (MAP Estimate)','Sensor Measurements','Underlying Algebraic Dynamic Model','Location','SouthEast')
legend('boxoff') 
ylim([0.45 1])
set(gca,'Box','off')
hold off

%% Nonlinear Least Squares (Gauss-Newton Implementation) 
%  Algorithm 1

% Start Algorithm Execution Stopwatch

tic;

% NSIG To Establish Convergence

NSIG = 4;

% Pre-allocate Memory For Least Squares Objective Function

Least_Squares_Objective = zeros(n,1);

% Pre-allocate Memory For Updated Model Parameters

Frequentist_Model_Parameters = zeros(n,length(Parameter_Guess));

% Perform Gauss-Newton Nonlinear Least Squares

for re_regress = 1:1:1 % Can Repeat Regression If Required
    
    % Pre-allocate Memory For Least Squares Objective Function
    
    Least_Squares_Objective = zeros(n,1);

    % Pre-allocate Memory For Updated Model Parameters
    
    Frequentist_Model_Parameters = zeros(n,length(Parameter_Guess));

    for i = 1:1:n

        % Calculate Model Output At Parameter Guess
        
        CA_model = (CA_initial + (Parameter_Guess(1,1)*delta_CA0*(1 - exp(-t_sensor/Parameter_Guess(2,1)))));

        % Evaluate Least Squares Objective Function
        
        Least_Squares_Objective(i,1) = sum((CA_measured - CA_model).^2);

        % Store Model Parameter Guess
        
        Frequentist_Model_Parameters(i,1:length(Parameter_Guess)) = Parameter_Guess';

        %Compute The Jacobian Matrix (Vectorised)
        
        J = [-delta_CA0*(exp(-t_sensor/Parameter_Guess(2,1)) - 1) -(Parameter_Guess(1,1)*delta_CA0*t_sensor.*exp(-t_sensor/Parameter_Guess(2,1)))/(Parameter_Guess(2,1)^2)];

        % Set Up Matrix A
        
        A = (J')*J;   

        % Set Up Vector b
        
        b = (J')*(CA_measured  - CA_model);    

        % Solve The Linear Equation To Obtain The Updated Parameter Step
        
        delta_parameter_update = A\b;    

        % Use the Bisection Rule To Select A Suitable Parameter Stepping Size
        
        if sum((CA_measured - ((CA_initial + ((Parameter_Guess(1,1) + delta_parameter_update(1,1))*delta_CA0*(1 - exp(-t_sensor/(Parameter_Guess(2,1) + delta_parameter_update(2,1)))))))).^2) < Least_Squares_Objective(i,1)

            rho = 1;

        else    

            rho = 1;

            for u = 1:1:max_trials

                rho = 0.5*rho;

                Least_Squares_Minimum = sum((CA_measured - (((CA_initial + ((Parameter_Guess(1,1) + rho*delta_parameter_update(1,1))*delta_CA0*(1 - exp(-t_sensor/(Parameter_Guess(2,1) + rho*delta_parameter_update(2,1))))))))).^2);

                if Least_Squares_Minimum < Least_Squares_Objective(i,1)

                    break;

                end

            end

        end

        % Update Parameter Guess With Appropriate Parameter Stepping Size
        
        Parameter_Guess = Parameter_Guess + rho*delta_parameter_update;
        fprintf('\nParameter Updates Per Iteration: [%0.4f,%0.4f]',Parameter_Guess(1,1),Parameter_Guess(2,1))
        
        % Recommended Convergence Criteria
        
        if (1/size(Parameter_Guess,1))*sum(abs(delta_parameter_update./Parameter_Guess)) <= 10^-(NSIG) 

            % Extract Values From Last Iteration At Convergence
            
            Frequentist_Model_Parameters(i + 1,1:length(Parameter_Guess)) = Parameter_Guess';
            Least_Squares_Objective(i + 1,1) = sum((CA_measured  - ((CA_initial + (Parameter_Guess(1,1)*delta_CA0*(1 - exp(-t_sensor/Parameter_Guess(2,1))))))).^2); 
            J = [-delta_CA0*(exp(-t_sensor/Parameter_Guess(2,1)) - 1) -(Parameter_Guess(1,1)*delta_CA0*t_sensor.*exp(-t_sensor/Parameter_Guess(2,1)))/(Parameter_Guess(2,1)^2)]; 
            Cov_A = (Least_Squares_Objective(i + 1,1)/(N - size(Parameter_Guess,1))).*inv((J')*J);

            break;        

        end

    end
    
    Parameter_Guess = Frequentist_Model_Parameters(i + 1,1:length(Parameter_Guess))';

end

% End Of Algorithm Execution Time Stopwatch

t_end_Frequentist = toc;

Frequentist_Model_Parameters = Frequentist_Model_Parameters(i + 1,1:length(Parameter_Guess))';

% Visualise Algebraic Dynamic Model Deterministic Function Response With
% The Least Squares (Frequentist ML) Estimate For The Model Parameters

figure(7)
plot(t,CA(t),'b--','LineWidth',2)
hold on
plot(t_sensor,CA_measured,'kx','MarkerSize',10,'LineWidth',2)
hold on
plot(t,CA_initial + (Frequentist_Model_Parameters(1,1)*delta_CA0*(1 - exp(-t/Frequentist_Model_Parameters(2,1)))),'Color',[0.4660, 0.6740, 0.1880],'LineWidth',2)
legend('Underlying Algebraic Dynamic Model','Sensor Measurements','Algebraic Dynamic Model (ML Estimate)','Location','SouthEast')
legend('boxoff') 
%title('CSTR Dynamic Response: Step Change in C_A_0')
xlabel('Time (min)')
ylabel('C_A (mole/m^3)')
set(gca,'Box','off')
hold off

% Visualise If Least Squares Objective Has Converged

figure(8)
plot(1:i+1,Least_Squares_Objective(1:i+1,1),'kx--','LineWidth',2,'MarkerSize',12)
title('Least Squares Objective Function')
xlabel('p^t^h iteration')
ylabel('LS(\Omega)')
set(gca,'XTick',1:i+1)
xlim([1 i+1])
set(gca,'Box','off')

%% Statistical Properties: Nonlinear Least Squares (Gauss-Newton Implementation)
%  Algorithm 1

% Ground Truth Comparison - Quality Of Parameter Estimates

fprintf('\n\nFrequentist Results (based on Maximum Likelihood Estimates)\n')
fprintf('\n\tProcess Gain: %0.3f (dimensionless)\n',Frequentist_Model_Parameters(1,1))
fprintf('\tProcess Time Constant: %0.2f (min)',Frequentist_Model_Parameters(2,1))
fprintf('\n\tNoise Variance: %0.7f (mole^2/m^6)',Least_Squares_Objective(i + 1,1)/((N*1) - (length(Parameter_Guess))))
fprintf('\n\tNoise Standard Deviation: %0.4f (mole/m^3)\n\n',sqrt(Least_Squares_Objective(i + 1,1)/((N*1) - (length(Parameter_Guess)))))

fprintf('\tProcess Gain %% Error: %0.2f%% \n',((abs(Frequentist_Model_Parameters(1,1) - Kp))/(Kp))*100)
fprintf('\tProcess Time Constant %% Error: %0.2f%% \n',((abs(Frequentist_Model_Parameters(2,1) - tau))/(tau))*100)
fprintf('\tNoise Standard Deviation %% Error: %0.2f%% \n',((abs(sqrt(Least_Squares_Objective(i + 1,1)/((N*1) - (length(Parameter_Guess)))) - sensor_standard_deviation))/(sensor_standard_deviation))*100)

% Parameter Marginal Confidence Interval

COV_Frequentist_Parameters = diag(Cov_A);

fprintf('\n\t99%% Marginal Confidence Interval For The Progress Gain: [%0.3f (dimensionless), %0.3f (dimensionless)]',Frequentist_Model_Parameters(1,1) - alpha*sqrt(COV_Frequentist_Parameters(1,1)),Frequentist_Model_Parameters(1,1) + alpha*sqrt(COV_Frequentist_Parameters(1,1)))
fprintf('\n\t99%% Marginal Confidence Interval For The Progress Time Constant: [%0.2f min, %0.2f min]',Frequentist_Model_Parameters(2,1) - alpha*sqrt(COV_Frequentist_Parameters(2,1)),Frequentist_Model_Parameters(2,1) + alpha*sqrt(COV_Frequentist_Parameters(2,1)))

% -> Visualise 99% Marginal Confidence Interval For Parameter 1

figure(9)

x = Frequentist_Model_Parameters(1,1);
y = 5;
errhigh = alpha*sqrt(COV_Frequentist_Parameters(1,1));
errlow =  alpha*sqrt(COV_Frequentist_Parameters(1,1));
errorbar(x,y,errlow,errhigh,'horizontal','o','Color',[0.4660, 0.6740, 0.1880],'LineWidth',2,'MarkerFaceColor',[0.4660, 0.6740, 0.1880],'MarkerSize',8)
hold on
plot(Kp,y,'bx','LineWidth',3,'MarkerFaceColor','b','MarkerSize',10)
text(Frequentist_Model_Parameters(1,1),5.15,'K_p_,_M_L','fontsize',12)
text(Kp,5.15,'K_p_,_T_r_u_e','fontsize',12)
xlim([0.49 0.55])
xlabel('K_p (dimensionless)')
set(gca,'YTick',[],'Box','off')
set(gca,'ycolor',[1 1 1])

% -> Visualise 99% Marginal Confidence Interval For Parameter 2

figure(10)

x = Frequentist_Model_Parameters(2,1);
y = 5;
errhigh = alpha*sqrt(COV_Frequentist_Parameters(2,1));
errlow =  alpha*sqrt(COV_Frequentist_Parameters(2,1));
errorbar(x,y,errlow,errhigh,'horizontal','o','Color',[0.4660, 0.6740, 0.1880],'LineWidth',2,'MarkerFaceColor',[0.4660, 0.6740, 0.1880],'MarkerSize',8)
hold on
plot(tau,y,'bx','LineWidth',3,'MarkerFaceColor','b','MarkerSize',10)
text(Frequentist_Model_Parameters(2,1),5.15,'\tau_M_L','fontsize',12)
text(tau - 0.3,5.15,'\tau_T_r_u_e','fontsize',12)
xlim([10 16.5])
xlabel('\tau (min)')
set(gca,'YTick',[],'Box','off')
set(gca,'ycolor',[1 1 1])

% Algorithm Execution Time

fprintf('\n\n\tElapsed Algorithm Time Is: %0.4f seconds\n',t_end_Frequentist)

% Joint Parameter Confidence Interval
% -> Ellipsoid Representation

figure(5)
p = length(Frequentist_Model_Parameters);

[EigenVec, EigenVal] = eig(Cov_A); % Calculate Eigenvalues And Eigenvectors

[Largest_EigenVec_Index, num] = find(EigenVal == max(max(EigenVal))); % Obtain The Index Of The Largest Eigenvector
Largest_EigenVec = EigenVec(:,Largest_EigenVec_Index);

Largest_EigenVal = max(max(EigenVal)); % Obtain The Largest Eigenvalue

if (Largest_EigenVec_Index == 1) % Obtain The Smallest Eigenvector And EigenValue
    
    Smallest_EigenVal = max(EigenVal(:,2));
    Smallest_EigenVec = EigenVec(:,2);
    
else
    
    Smallest_EigenVal = max(EigenVal(:,1));
    Smallest_EigenVec = EigenVec(:,1);
    
end

Angle = atan2(Largest_EigenVec(2),Largest_EigenVec(1)); % Calculate The Angle Between The Cartesian x-axis And The Largest Eigenvector

if Angle < 0
    
    Angle = Angle + 2*pi; % Shift Angle To Be Between 0 and 2pi
    
end

Average = Frequentist_Model_Parameters'; % Extract Mean Inferred Parameter Values

Chi_Value = sqrt(p*F); % Obtain Radius For The Error Ellipse
Theta_Grid = linspace(0,2*pi,200000);
Kp_Mean = Average(1);
tau_Mean = Average(2);

A1 = Chi_Value*sqrt(Largest_EigenVal);
B1 = Chi_Value*sqrt(Smallest_EigenVal);

Ellipse_X_r = A1*cos(Theta_Grid); % Ellipse Cartesian x-axis Coordinates
Ellipse_Y_r = B1*sin(Theta_Grid); % Ellipse Cartesian y-axis Coordinates

R = [cos(Angle) sin(Angle); -sin(Angle) cos(Angle)]; % Ellipse Rotation Matrix

r_Ellipse = [Ellipse_X_r;Ellipse_Y_r]'*R; % Rotate Ellipse

Freq_Ellipse = plot(r_Ellipse(:,1) + Kp_Mean,r_Ellipse(:,2) + tau_Mean,'Color',[0.4660, 0.6740, 0.1880],'LineWidth',3); % Visualise Ellipse
hold on
plot(Frequentist_Model_Parameters(1,1),Frequentist_Model_Parameters(2,1),'o','Color',[0.4660, 0.6740, 0.1880],'LineWidth',2,'MarkerSize',8,'MarkerFaceColor',[0.4660, 0.6740, 0.1880])
%title('99% Joint Parameter Confidence/Credibility Region')
ylabel('\tau (min)')
xlabel('K_p (dimensionless)')
set(gca,'Box','off')
hold on
plot(Kp,tau,'bx','LineWidth',3,'MarkerFaceColor','b','MarkerSize',10)
legend('Bayesian 99% Epplise','[K_p_,_M_A_P  \tau_M_A_P]^T','Frequentist 99% Ellispe','[K_p_,_M_L  \tau_M_L]^T','[K_p_,_T_r_u_e  \tau_T_r_u_e]^T','Location','SouthEast')
legend('boxoff') 
set(get(get(Bayes_Ellipse,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
set(get(get(Freq_Ellipse,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
hold off

% Visualise The Expected Mean Response

figure(11)

%  -> Evaluate Jacobian Matrix (Vectorised)

J = [-delta_CA0*(exp(-mean_response_range/Frequentist_Model_Parameters(2,1)) - 1) -(Frequentist_Model_Parameters(1,1)*delta_CA0*mean_response_range.*exp(-mean_response_range/Frequentist_Model_Parameters(2,1)))/(Frequentist_Model_Parameters(2,1)^2)];

% -> Evaluate Covariance

Cov_y = (Least_Squares_Objective(i + 1,1)/((N*1) - (length(Frequentist_Model_Parameters))))*J/((J')*J)*J';

CA_model = (CA_initial + (Frequentist_Model_Parameters(1,1)*delta_CA0*(1 - exp(-mean_response_range/Frequentist_Model_Parameters(2,1)))));
S2 = diag(Cov_y);
plot(mean_response_range,CA_model + alpha*sqrt(S2),'o','Color',[0 0 0],'MarkerSize',1.5,'MarkerFaceColor',[0 0 0]);%[5 5 5]/8)
hold on
p = plot(mean_response_range,CA_model - alpha*sqrt(S2),'o','Color',[0 0 0],'MarkerSize',1.5,'MarkerFaceColor',[0 0 0]);%[5 5 5]/8);
hold on
plot(t,CA_initial + (Frequentist_Model_Parameters(1,1)*delta_CA0*(1 - exp(-t/Frequentist_Model_Parameters(2,1)))),'Color',[0.4660, 0.6740, 0.1880],'LineWidth',2)
hold on
plot(t_sensor,CA_measured,'kx','MarkerSize',10,'LineWidth',2)
hold on
plot(t,CA(t),'b--','LineWidth',2)
%title('CSTR Dynamic Response: Step Change in C_A_0')
xlabel('Time (min)')
ylabel('C_A (mole/m^3)')
legend('99% Upper/Lower Confidence Limits','99% Lower Confidence Limit','Algebraic Dynamic Model (ML Estimate)','Sensor Measurements','Underlying Algebraic Dynamic Model','Location','SouthEast')
legend('boxoff') 
set(get(get(p,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
ylim([0.45 1])
set(gca,'Box','off')
hold off