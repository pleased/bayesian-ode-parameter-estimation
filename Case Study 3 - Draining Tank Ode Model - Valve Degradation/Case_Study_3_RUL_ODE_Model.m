%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

         %%%% Ordinary Differential Equation Regression %%%%     
            %%%% -> Linear In The ODE Model Parameters %%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%% Case Study 2: Draining Tank (Example 3.6)
%%%% Reference 1: Marlin, T E. 2000. Process Control: Designing Processes And
%%%% Control Systems For Dynamic Performance. 2nd Ed. Boston: McGraw-Hill.

%%%% Reference 2: Van Noortwijk, J. M. And Pandey, M. D. 2003. �A Stochastic 
%%%% Deterioration Process For Time-Dependent Reliability Analysis�. Proceedings 
%%%% Of The Eleventh IFIP WG 7.5. Working Conference On Reliability And Optimization 
%%%% Of Structural Systems. pp. 259�265.

%% Clear Memory And Command Window

close all;
clear;
clc;

%% Draining Tank Model

% -> Derived From First Principles (Conservation of Mass)
% -> No Input Disturbance Structure Assumed
% -> Input Disturbance In F0 Modeled With a Draw From a Gaussian Process
%    Between a Normal Operating Conditions Bandwidth
% -> ODE Model is Nonlinear In The Model State Variable L
% -> ODE Model is Linear In The Model Parameters

% Dynamic Model Simulation Information

F0_initial = 100; % m^3/h - Inlet Flowrate
F0_initial = F0_initial*(1/60); % m^3/min - Inlet Flowrate (Converted To Min)
L_initial = 7.0; % m - Initial Draining Tank Level
A = 7; % m^2 - Draining Tank Cross-Sectional Area
k_v = F0_initial/sqrt(L_initial); % (m^2.5/h) - Flow Restriction Coeffcient
delta_F0 = -10*(1/60); % m^3/min - Disturbance Step Change in Draining Tank Inlet Flowrate

% Degradation Model Coefficients - Model Adjusted From Van Noortwijk, J. M.
% And Pandey, M. D. (2003)

A_k = 2*10^-6; % m^2.5/min^2.07
b_k = 1.07; % dimensionless
k_v_t0 = k_v; % m^2.5/min

z_score = 2.576; % z_score value - 99% Confidence/Credibility

% Simulation Run Time

Sim_Time = 300; % Minutes

% Simulation Ground Truth Parameter Values

fprintf('\nTrue Parameter Values (Set During Simulation)\n\n')
fprintf('\t1/A: %0.4f (m^-2)\n',1/A)
fprintf('\tk_v/A: %0.4f (m^0.5/min)\n',k_v/A)

%% Load F0 Input Disturbance Data
%  Input Disturbance Generated Independently From A Draw From A Gaussian
%  Process
    
load('Case_Study_3_RUL_Input_Disturbance.mat')

% Read MATLAB Data To Simulink

RandomF0.time = Case_Study_3_Input_Disturbance(:,1);
RandomF0.signals.values = Case_Study_3_Input_Disturbance(:,2);
RandomF0.signals.dimensions = 1;

%% Data Generation Process - Performed In Simulink

% Sensor Measurement Sampling Time

SampleTime = 1; % Minute

% Sensor Measurement Details

% Flow Sensor

rng(1,'Twister') % Ensures Repeatability
Flow_Sensor_Seed = randi(1*10^9,1,1);
Flow_Sensor_Var = 2*10^-5;
Flow_Sensor_SDev = sqrt(Flow_Sensor_Var);

fprintf('\tFlow Sensor Noise Standard Deviation: %0.5f (m^3/min)\n',Flow_Sensor_SDev)

% Level Sensor

rng(2,'Twister') % Ensure Repeatability
Level_Sensor_Seed = randi(1*10^9,1,1);
Level_Sensor_Var = 4*10^-5;
Level_Sensor_SDev = sqrt(Level_Sensor_Var);

fprintf('\tLevel Sensor Noise Standard Deviation: %0.5f (m)\n',Level_Sensor_SDev)

% -> Run Simulink Model From MATLAB

open('Case_Study_3_RUL_Linear_ODE_Regression.slx')
sim('Case_Study_3_RUL_Linear_ODE_Regression.slx')
close_system('Case_Study_3_RUL_Linear_ODE_Regression.slx')

% -> Visualise Simulink Data Generation Results

% Flow Sensor Measurements

figure(1)
plot(F0_Sensor.F0_Noise_Free.Time,F0_Sensor.F0_Noise_Free.Data,'b--','LineWidth',2)
hold on
plot(F0_Sensor.F0_Noise.Time,F0_Sensor.F0_Noise.Data,'kx','MarkerSize',8,'LineWidth',2)
title('F_0 Exogenous Input Disturbance')
xlabel('Time (min)')
ylabel('F_0 (m^3/min)')
hold on
x_Upper_Limit = linspace(0,Sim_Time,1000);
plot(x_Upper_Limit,1.05*F0_initial*ones(length(x_Upper_Limit),1),'m--','LineWidth',2) % Upper NOC Threshold Value
hold on
x_Lower_Limit = linspace(0,Sim_Time,1000);
Lower = plot(x_Lower_Limit,0.95*F0_initial*ones(length(x_Lower_Limit),1),'m--','LineWidth',2); % Lower NOC Threshold Value
hold on
x_Steady = linspace(0,Sim_Time,Sim_Time/5);
plot(x_Steady,F0_initial.*ones(length(x_Steady),1),'m.','LineWidth',2,'MarkerSize',10)
legend('Underlying Exogenous Input Disturbance','Inlet Flow Rate Sensor Measurements','Upper/Lower NOC Limit','Lower NOC Limit','True Underlying Steady State','Location','SouthEast')
set(get(get(Lower,'Annotation'),'LegendInformation'),'IconDisplayStyle','off')
legend('Box','Off')
axis([0 Sim_Time 1.52 1.76])
set(gca,'Box','off')
hold off

% Tank Level Sensor Measurements

figure(2)
plot(L_Sensor.L_Noise_Free.Time,L_Sensor.L_Noise_Free.Data,'b--','LineWidth',2)
hold on
plot(L_Sensor.L_Noise.Time,L_Sensor.L_Noise.Data,'kx','MarkerSize',8,'LineWidth',2)
title('Draining Tank Dynamic Response: Random Input in F_0')
xlabel('Time (min)')
ylabel('L (m)')
set(gca,'Box','off')
x_Steady = linspace(0,Sim_Time,Sim_Time/5);
hold on
plot(x_Steady,L_initial.*ones(length(x_Steady),1),'m.','LineWidth',2,'MarkerSize',10)
legend('Underlying State Variable Dynamic Response','Liquid Level Sensor Measurements','True Underlying Steady State','Location','NorthEast')
legend('Box','Off')
ylim([6.95 7.4])
% hold on
% x_Upper_Limit = linspace(0,Sim_Time,Sim_Time);
% plot(x_Upper_Limit,7.7175.*ones(length(x_Upper_Limit),1),'m-','LineWidth',2) % Upper NOC Threshold Value
% hold on
% x_Lower_Limit = linspace(0,Sim_Time,Sim_Time);
% plot(x_Lower_Limit,6.3175.*ones(length(x_Lower_Limit),1),'m-','LineWidth',2) % Lower NOC Threshold Value
% legend('Underlying Tank Liquid Level','Tank Liquid Level Sensor Measurements','True Steady State','Upper NOC Limit','Lower NOC Limit','Location','Best')

% Number Of Liquid Level Sensor Measurements

N = length(L_Sensor.L_Noise.Data);

%% Assumed Valve Degradation Model - Adjusted From Van Noortwijk, J. M.
% And Pandey, M. D. (2003)

% Valve Degradation Model

kv_t = @(t)  (k_v_t0 + (A_k).*t.^(b_k))./A;

% Simulated Degradation Period

kv_time_range = linspace(0,50000,100);

% Visualise Degradation Model

figure(3)
plot(kv_time_range.*(1/(60*24)),kv_t(kv_time_range),'b--','LineWidth',2)
title('Random Variable Degradation Model - k_v')
xlabel('Time (day)')
ylabel('k_\nu/A (m^0^.^5/min)')
xlim([0 30])
hold on

fprintf('\tA_v/A: %0.2d (m^0.5/min^2.07)\n',A_k/A)

% Calculate Valve Replacement Time Based On Assumed Threshold

k_v_t0_Ac = k_v_t0/A;
kv_tf_Ac = 1.2*(k_v_t0/A);

tf_true = ((kv_tf_Ac - k_v_t0_Ac)*(1/(A_k/A)))^(1/b_k);

fprintf('\nValve Replacement Time: %0.2f minutes (~%0.2f days)\n',tf_true,tf_true*(1/(24*60)))
fprintf('\nIf Samples Are Taken Until t = %0.2f minutes To Estimate The Valve Replacement, Then,',Sim_Time)

RUL_true = tf_true - Sim_Time;

fprintf('\nThe RUL Is Calculated As:\n') 
fprintf('\n\t%0.2f minutes - %0.2f minutes = %0.2f minutes(~%0.2f days)\n',tf_true,Sim_Time,RUL_true,RUL_true*(1/(60*24)))

%% Initial Model Parameter Guess And Number Of Algorithm Iterations 

% -> Intial Guess Condition Used For:
    % (1) Gaussian Process ODE Regression (Algorithm 4)
    % (2) Nonlinear Least Squares ODE Regression (Gauss-Newton
    % Implementation) (Algorithm 2)
    
rng('Shuffle','Twister')
Parameter_1 = ((1/10)*(1/A)) + (((10)*(1/A)) - ((1/10)*(1/A))).*rand(1,1);
rng('Shuffle','Twister')
Parameter_2 = ((1/10)*(k_v/A)) + (((10)*(k_v/A)) - ((1/10)*(k_v/A))).*rand(1,1);
rng('Shuffle','Twister')
Parameter_3 = ((1/10)*(A_k/A)) + (((10)*(A_k/A)) - ((1/10)*(A_k/A))).*rand(1,1);

% Populate Vector Containing Initial Model Parameter Guess

Parameter_Guess = [Parameter_1;Parameter_2;Parameter_3];

fprintf('\nInitial Parameter Guess [%0.4f,%0.4f,%0.2d]\n',Parameter_1,Parameter_2,Parameter_3)

% -> Number Of Trial Iterations:
    % (2) Nonlinear Least Squares ODE Regression (Gauss-Newton 
    % Implementation) (Algorithm 2)
    
n = 200;

% -> Empirical Iterations/Trials
    % (2) Nonlinear Least Squares ODE Regression (Gauss-Newton 
    % Implementation) (Algorithm 2)
    
max_trials = 500;

%% Gaussian Process ODE Regression
%  (Algorithm 4)

% Start Algorithm Execution Stopwatch - Optimisation Routine

tic;

% -> Optimise Gaussian Process Kernel Hyperparameters Via Gradient Ascent

% Tank Level Data
    
Output_Data_L = L_Sensor.L_Noise.Data;
Input_t_L = L_Sensor.L_Noise.Time';

% Tank Inlet Flow Rate Data
    
Output_Data_F0 = F0_Sensor.F0_Noise.Data;  
Input_t_F0 = F0_Sensor.F0_Noise.Time';

% Collect Data In a Vector For Joint Optimisation

Output_Data = [Output_Data_L;Output_Data_F0];
Input_t = [Input_t_L Input_t_F0];

% Pre-Allocate Memory For 2 Optimisation Routines
% - > 10 Unknown Gaussian Process Parameters + Log Marginal Likelihood Function
% Value

Max_GP_Objective = zeros(2,11);

% Number Of Gradient Ascent Iterations

GA_Iterations = 3000;

% Stepping Parameter Size

Gamma = 0.01;

for run_repeat = 1:1:2
    
    fprintf('\nGaussian Process Hyperparameter Optimisation: %0.0f',run_repeat)
    
    % Randomly Initialise Each Log Unknown Gaussian Process Parameter
    
    % Tank Level Gaussian Process Parameters
    % Constrained To Be Positive
        
    rng('Shuffle','Twister')
    theta_1_L = -4.3 + 2.*randn(1,1);
    rng('Shuffle','Twister')
    theta_2_L = 1.93 + 2.*randn(1,1);
    rng('Shuffle','Twister')
    theta_3_L = -10.4 + 0.1.*randn(1,1);
    rng('Shuffle','Twister')
    theta_4_L = 6.95 + 2.*randn(1,1);
    rng('Shuffle','Twister')
    theta_5_L = 12.91 + 2.*randn(1,1);
    
    % Inlet Flow Rate Gaussian Process Parameters
    % Constrained To Be Positive
        
    rng('Shuffle','Twister')
    theta_1_F0 = 2.0 + 2.*randn(1,1);
    rng('Shuffle','Twister')
    theta_2_F0 = -5.95 + 2.*randn(1,1);
    rng('Shuffle','Twister')
    theta_3_F0 = -10.8 + 0.1.*randn(1,1);
    rng('Shuffle','Twister')
    theta_4_F0 = 15.5 + 2.*randn(1,1);
    rng('Shuffle','Twister')
    theta_5_F0 = 6.9 + 2.*randn(1,1);
    
    % Gaussian Process Parameters
    
    % Tank Level Gaussian Process Parameters
        
    sigf_square_1_L = exp(theta_1_L);
    sigf_square_2_L = exp(theta_2_L);
    noise_var_data_L = exp(theta_3_L);
    %sqrt(noise_var_data_L)
    l_square_1_L = exp(theta_4_L);
    l_square_2_L = exp(theta_5_L);
    
    % Inlet Flow Rate Gaussian Process Parameters
        
    sigf_square_1_F0 = exp(theta_1_F0);
    sigf_square_2_F0 = exp(theta_2_F0);
    noise_var_data_F0 = exp(theta_3_F0);
    %sqrt(noise_var_data_F0)
    l_square_1_F0 = exp(theta_4_F0);
    l_square_2_F0 = exp(theta_5_F0);
    
    % Pre-Allocate Memory For Kernel Matrices
    
    % Tank Level Gaussian Process Matrices
        
    B_Data_L = zeros(length(Input_t_L),length(Input_t_L));
    B_theta_1_L = zeros(length(Input_t_L),length(Input_t_L));
    B_theta_2_L = zeros(length(Input_t_L),length(Input_t_L));
    B_theta_3_L = zeros(length(Input_t_L),length(Input_t_L));
    B_theta_4_L = zeros(length(Input_t_L),length(Input_t_L));
    B_theta_5_L = zeros(length(Input_t_L),length(Input_t_L));
    
    % Inlet Flow Rate Gaussian Process Matrices
        
    B_Data_F0 = zeros(length(Input_t_F0),length(Input_t_F0));
    B_theta_1_F0 = zeros(length(Input_t_F0),length(Input_t_F0));
    B_theta_2_F0 = zeros(length(Input_t_F0),length(Input_t_F0));
    B_theta_3_F0 = zeros(length(Input_t_F0),length(Input_t_F0));
    B_theta_4_F0 = zeros(length(Input_t_F0),length(Input_t_F0));
    B_theta_5_F0 = zeros(length(Input_t_F0),length(Input_t_F0));

    % Pre-Allocate Memory For Gaussian Process Objective Function
    
    GP_Objective = zeros(GA_Iterations,1);
    
     % Construct Kernel Matrix With Data For The Tank Level
    
    j = 1;
    
    for i = 1:1:length(Input_t_L)
        
        g = 1;
        
        for z = 1:1:length(Input_t_L)
            
            B_Data_L(j,g) = sigf_square_1_L*exp((-1/(2*(l_square_1_L)))*((Input_t_L(1,j) - Input_t_L(1,g)))^2) + ...
                        + sigf_square_2_L*exp((-1/(2*(l_square_2_L)))*((Input_t_L(1,j) - Input_t_L(1,g)))^2);
            
            g = g + 1;
            
        end
        
        j = j + 1;
        
    end
    
    B_Data_L = B_Data_L + noise_var_data_L.*eye(length(Input_t_L),length(Input_t_L));

    % Construct Kernel Matrix With Data For The Inlet Flow Rate
    
    j = 1;
    
    for i = 1:1:length(Input_t_F0)
        
        g = 1;
        
        for z = 1:1:length(Input_t_F0)
            
            B_Data_F0(j,g) = sigf_square_1_F0*exp((-1/(2*(l_square_1_F0)))*((Input_t_F0(1,j) - Input_t_F0(1,g)))^2) + ...
                        + sigf_square_2_F0*exp((-1/(2*(l_square_2_F0)))*((Input_t_F0(1,j) - Input_t_F0(1,g)))^2);
            
            g = g + 1;
            
        end
        
        j = j + 1;
        
    end
    
    B_Data_F0 = B_Data_F0 + noise_var_data_F0.*eye(length(Input_t_L),length(Input_t_L));
    
    B_Data = [B_Data_L zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) B_Data_F0];
         
    % Evaluate Gaussian Process Objective Function For Initial Guess Of
    % Gaussian Process Hyperparameters
    
    GP_Objective(1,1) = -0.5*(2*sum(log(diag(chol(B_Data))))) ...
                        -0.5*((Output_Data)')/(B_Data)*Output_Data...
                        -0.5*length(Input_t)*log(2*pi);
                    
    for p = 1:1:GA_Iterations
        
    % Construct Kernel Matrix With Data For The Tank Level
    
    j = 1;
    
    for i = 1:1:length(Input_t_L)
        
        g = 1;
        
        for z = 1:1:length(Input_t_L)
            
            B_Data_L(j,g) = sigf_square_1_L*exp((-1/(2*(l_square_1_L)))*((Input_t_L(1,j) - Input_t_L(1,g))^2)) + ...
                        + sigf_square_2_L*exp((-1/(2*(l_square_2_L)))*((Input_t_L(1,j) - Input_t_L(1,g))^2));
            
            g = g + 1;
            
        end
        
        j = j + 1;
        
    end
    
    B_Data_L = B_Data_L + noise_var_data_L.*eye(length(Input_t_L),length(Input_t_L));
    
    % Construct Kernel Matrix With Data For The Inlet Flowrate
    
    j = 1;
    
    for i = 1:1:length(Input_t_F0)
        
        g = 1;
        
        for z = 1:1:length(Input_t_F0)
            
            B_Data_F0(j,g) = sigf_square_1_F0*exp((-1/(2*(l_square_1_F0)))*((Input_t_F0(1,j) - Input_t_F0(1,g)))^2) + ...
                        + sigf_square_2_F0*exp((-1/(2*(l_square_2_F0)))*((Input_t_F0(1,j) - Input_t_F0(1,g)))^2);
            
            g = g + 1;
            
        end
        
        j = j + 1;
        
    end
    
    B_Data_F0 = B_Data_F0 + noise_var_data_F0.*eye(length(Input_t_L),length(Input_t_L));
    
    B_Data = [B_Data_L zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) B_Data_F0]; 
    
    % Tank Level Derivative Matrices
    
    % Construct Theta_1_L Derivative Matrix
    
    j = 1;
        
    for i = 1:1:length(Input_t_L)
        
        g = 1;
        
        for z = 1:1:length(Input_t_L)
            
            B_theta_1_L(j,g) = sigf_square_1_L*exp((-1/(2*(l_square_1_L)))*((Input_t_L(1,j) - Input_t_L(1,g))^2));

            g = g + 1;
            
        end
        
        j = j + 1;
        
    end       
    
    % Construct Theta_2_L Derivative Matrix
    
    j = 1;
        
    for i = 1:1:length(Input_t_L)
        
        g = 1;
        
        for z = 1:1:length(Input_t_L)
            
            B_theta_2_L(j,g) = sigf_square_2_L*exp((-1/(2*(l_square_2_L)))*((Input_t_L(1,j) - Input_t_L(1,g))^2));

            g = g + 1;
            
        end
        
        j = j + 1;
        
    end    
    
    % Construct Theta_3_L Derivative Matrix
    
    B_theta_3_L = noise_var_data_L.*eye(length(Input_t_L),length(Input_t_L));
    
    % Construct Theta_4_L Derivative Matrix
    
    j = 1;
        
    for i = 1:1:length(Input_t_L)
        
        g = 1;
        
        for z = 1:1:length(Input_t_L)
            
            B_theta_4_L(j,g) = ((sigf_square_1_L*((Input_t_L(1,j) - Input_t_L(1,g))^2))/(2*l_square_1_L))*exp((-1/(2*(l_square_1_L)))*((Input_t_L(1,j) - Input_t_L(1,g))^2));

            g = g + 1;
            
        end
        
        j = j + 1;
        
    end         
    
    % Construct Theta_5_L Derivative Matrix
    
    j = 1;
        
    for i = 1:1:length(Input_t_L)
        
        g = 1;
        
        for z = 1:1:length(Input_t_L)
            
            B_theta_5_L(j,g) = ((sigf_square_2_L*((Input_t_L(1,j) - Input_t_L(1,g))^2))/(2*l_square_2_L))*exp((-1/(2*(l_square_2_L)))*((Input_t_L(1,j) - Input_t_L(1,g))^2));

            g = g + 1;
            
        end
        
        j = j + 1;
        
    end         

    % Inlet Flow Rate Derivative Matrices  
    
    % Construct Theta_1_F0 Derivative Matrix
    
    j = 1;
        
    for i = 1:1:length(Input_t_F0)
        
        g = 1;
        
        for z = 1:1:length(Input_t_F0)
            
            B_theta_1_F0(j,g) = sigf_square_1_F0*exp((-1/(2*(l_square_1_F0)))*((Input_t_F0(1,j) - Input_t_F0(1,g))^2));

            g = g + 1;
            
        end
        
        j = j + 1;
        
    end      
    
    % Construct Theta_2_F0 Derivative Matrix
    
    j = 1;
        
    for i = 1:1:length(Input_t_F0)
        
        g = 1;
        
        for z = 1:1:length(Input_t_F0)
            
            B_theta_2_F0(j,g) = sigf_square_2_F0*exp((-1/(2*(l_square_2_F0)))*((Input_t_F0(1,j) - Input_t_F0(1,g))^2));

            g = g + 1;
            
        end
        
        j = j + 1;
        
    end   
    
    % Construct Theta_3_F0 Derivative Matrix
    
    B_theta_3_F0 = noise_var_data_F0.*eye(length(Input_t_F0),length(Input_t_F0));    
    
    % Construct Theta_4_F0 Derivative Matrix
    
    j = 1;
        
    for i = 1:1:length(Input_t_F0)
        
        g = 1;
        
        for z = 1:1:length(Input_t_F0)
            
            B_theta_4_F0(j,g) = ((sigf_square_1_F0*((Input_t_F0(1,j) - Input_t_F0(1,g))^2))/(2*l_square_1_F0))*exp((-1/(2*(l_square_1_F0)))*((Input_t_F0(1,j) - Input_t_F0(1,g))^2));

            g = g + 1;
            
        end
        
        j = j + 1;
        
    end     
    
    % Construct Theta_5_F0 Derivative Matrix
    
    j = 1;
        
    for i = 1:1:length(Input_t_F0)
        
        g = 1;
        
        for z = 1:1:length(Input_t_F0)
            
            B_theta_5_F0(j,g) = ((sigf_square_2_F0*((Input_t_F0(1,j) - Input_t_F0(1,g))^2))/(2*l_square_2_F0))*exp((-1/(2*(l_square_2_F0)))*((Input_t_F0(1,j) - Input_t_F0(1,g))^2));

            g = g + 1;
            
        end
        
        j = j + 1;
        
    end     

    % Calculate alpha
    
    alpha = (B_Data)\Output_Data;
    
    % Tank Level Gaussian Process Parameter Updates
    % -> Gradient Ascent
    
    new_theta_1_L = theta_1_L + Gamma*(0.5*trace(((alpha*(alpha')) - (inv([B_Data_L zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) B_Data_F0])))*([B_theta_1_L zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) B_theta_1_F0])));
    new_theta_2_L = theta_2_L + Gamma*(0.5*trace(((alpha*(alpha')) - (inv([B_Data_L zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) B_Data_F0])))*([B_theta_2_L zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) B_theta_2_F0])));
    new_theta_3_L = theta_3_L + Gamma*(0.5*trace(((alpha*(alpha')) - (inv([B_Data_L zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) B_Data_F0])))*([B_theta_3_L zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) B_theta_3_F0])));
    new_theta_4_L = theta_4_L + Gamma*(0.5*trace(((alpha*(alpha')) - (inv([B_Data_L zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) B_Data_F0])))*([B_theta_4_L zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) B_theta_4_F0])));
    new_theta_5_L = theta_5_L + Gamma*(0.5*trace(((alpha*(alpha')) - (inv([B_Data_L zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) B_Data_F0])))*([B_theta_5_L zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) B_theta_5_F0])));
    
    % Inlet Flow Rate Gaussian Process Parameter Updates
    % -> Gradient Ascent
    
    new_theta_1_F0 = theta_1_F0 + Gamma*(0.5*trace(((alpha*(alpha')) - (inv([B_Data_L zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) B_Data_F0])))*([B_theta_1_L zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) B_theta_1_F0])));
    new_theta_2_F0 = theta_2_F0 + Gamma*(0.5*trace(((alpha*(alpha')) - (inv([B_Data_L zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) B_Data_F0])))*([B_theta_2_L zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) B_theta_2_F0])));
    new_theta_3_F0 = theta_3_F0 + Gamma*(0.5*trace(((alpha*(alpha')) - (inv([B_Data_L zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) B_Data_F0])))*([B_theta_3_L zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) B_theta_3_F0])));
    new_theta_4_F0 = theta_4_F0 + Gamma*(0.5*trace(((alpha*(alpha')) - (inv([B_Data_L zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) B_Data_F0])))*([B_theta_4_L zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) B_theta_4_F0])));
    new_theta_5_F0 = theta_5_F0 + Gamma*(0.5*trace(((alpha*(alpha')) - (inv([B_Data_L zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) B_Data_F0])))*([B_theta_5_L zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) B_theta_5_F0])));   

    % Tank Level Gaussian Process Log Parameter Assignment
    
    theta_1_L = new_theta_1_L;
    theta_2_L = new_theta_2_L;
    theta_3_L = new_theta_3_L;
    theta_4_L = new_theta_4_L;
    theta_5_L = new_theta_5_L;
    
    % Inlet Flow Rate Gaussian Process Log Parameter Assignment
    
    theta_1_F0 = new_theta_1_F0;
    theta_2_F0 = new_theta_2_F0;
    theta_3_F0 = new_theta_3_F0;
    theta_4_F0 = new_theta_4_F0;
    theta_5_F0 = new_theta_5_F0;
    
    % Tank Level Gaussian Process Parameters
    
    sigf_square_1_L = exp(theta_1_L);
    sigf_square_2_L = exp(theta_2_L);
    noise_var_data_L = exp(theta_3_L);
    l_square_1_L = exp(theta_4_L);
    l_square_2_L = exp(theta_5_L);
    
    % Inlet Flow Rate Gaussian Process Parameters
    
    sigf_square_1_F0 = exp(theta_1_F0);
    sigf_square_2_F0 = exp(theta_2_F0);
    noise_var_data_F0 = exp(theta_3_F0);
    l_square_1_F0 = exp(theta_4_F0);
    l_square_2_F0 = exp(theta_5_F0);    
    
    % Tank Level Gaussian Process Parameters Used To Construct
    % Matrices
    
    sigf_estimated_1_L = sqrt(exp(theta_1_L));
    sigf_estimated_2_L = sqrt(exp(theta_2_L));
    noise_var_data_estimated_L = exp(theta_3_L);
    l_estimated_1_L = sqrt(exp(theta_4_L));
    l_estimated_2_L = sqrt(exp(theta_5_L));
    
    % Inlet Flow Rate Gaussian Process Parameters Used To Construct
    % Matrices
    
    sigf_estimated_1_F0 = sqrt(exp(theta_1_F0));
    sigf_estimated_2_F0 = sqrt(exp(theta_2_F0));
    noise_var_data_estimated_F0 = exp(theta_3_F0);
    l_estimated_1_F0 = sqrt(exp(theta_4_F0));
    l_estimated_2_F0 = sqrt(exp(theta_5_F0));    
    
    % Evaluate Gaussian Process Objective Function To Establish Whether
    % Convergence Is Reached After Each Iteration
    
    GP_Objective(p + 1,1) = -0.5*(2*sum(log(diag(chol(B_Data))))) ...
                            -0.5*((Output_Data)')/(B_Data)*Output_Data...
                            -0.5*length(Input_t)*log(2*pi);  

    if p >= 2 && abs(abs(GP_Objective(p + 1,1)) - abs(GP_Objective(p,1))) < 0.1
        
        break;
        
    end
     
    end
    
    % Save Optimised Parameter Values For Each Optimisation Routine
    
    Max_GP_Objective(run_repeat,:) = [sigf_estimated_1_L sigf_estimated_2_L noise_var_data_estimated_L l_estimated_1_L l_estimated_2_L ...
        sigf_estimated_1_F0 sigf_estimated_2_F0 noise_var_data_estimated_F0 l_estimated_1_F0 l_estimated_2_F0 GP_Objective(p + 1,1)];
    
%     Visualise If Objective Function Maximum Is Reached
%     figure(run_repeat + 17)
%     plot(0:p,GP_Objective(1:p + 1,1),'k-','LineWidth',2)
%     axis([0 p -Inf Inf])
%     xlabel('p^t^h iteration')
%     ylabel('Log Marginal Likelihood')
    
end

% Extract Maximum Gaussian Process Objective Function Value With Associated Optimised Parameters

[Max_Num,Max_Index] = max(Max_GP_Objective(:,11));

% Tank Liquid Level Gaussian Process Parameters Used To Construct Kernel
% Matrices

sigf_estimated_1_L = Max_GP_Objective(Max_Index,1);
sigf_estimated_2_L = Max_GP_Objective(Max_Index,2);
noise_var_data_estimated_L = Max_GP_Objective(Max_Index,3);
l_estimated_1_L = Max_GP_Objective(Max_Index,4);
l_estimated_2_L = Max_GP_Objective(Max_Index,5);

% Tank Inlet Flow Rate Gaussian Process Parameters Used To Construct Kernel
% Matrices

sigf_estimated_1_F0 = Max_GP_Objective(Max_Index,6);
sigf_estimated_2_F0 = Max_GP_Objective(Max_Index,7);
noise_var_data_estimated_F0 = Max_GP_Objective(Max_Index,8);
l_estimated_1_F0 = Max_GP_Objective(Max_Index,9);
l_estimated_2_F0 = Max_GP_Objective(Max_Index,10);

% End Of Algorithm Execution Time Stopwatch - Optimisation Routine

t_end_optimisation = toc;

% Start Algorithm Execution Stopwatch - ode Regression

tic;

% Construct Gaussian Process To Infer The Underlying State Variable L

Predict_Past = 0;
Predict_Future = 0;

% State Variable L Prediction Range

L_Predict_t = linspace(0 - Predict_Past, Sim_Time + Predict_Future,length(Input_t_L));
F0_Predict_t = linspace(0 - Predict_Past, Sim_Time + Predict_Future,length(Input_t_F0));

% Pre-Allocate Memory For Kernel Matrices For State Variable L

B_L_aa = zeros(length(L_Predict_t),length(L_Predict_t));
B_L_ab = zeros(length(L_Predict_t),length(Input_t_L));
B_L_bb = zeros(length(Input_t_L),length(Input_t_L));
B_F0_bb = zeros(length(Input_t_F0),length(Input_t_F0));

% Populate Kernel Matrices In An Elementwise Manner

j = 1;

for i = 1:1:length(L_Predict_t)

    g = 1;

    for z = 1:1:length(L_Predict_t)

        B_L_aa(j,g) = ((sigf_estimated_1_L)^2)*exp((-1/(2*(((l_estimated_1_L))^2)))*((L_Predict_t(1,j) - L_Predict_t(1,g))^2)) + ...
                    + ((sigf_estimated_2_L)^2)*exp((-1/(2*(((l_estimated_2_L))^2)))*((L_Predict_t(1,j) - L_Predict_t(1,g))^2));

        g = g + 1;

    end

    j = j + 1;

end

% Addition Of "Jitter" For Numerical Stability

B_L_aa = B_L_aa + (1*10^-12)*eye(length(L_Predict_t),length(L_Predict_t));

j = 1;

for i = 1:1:length(L_Predict_t)

    g = 1;

    for z = 1:1:length(Input_t_L)

        B_L_ab(j,g) = ((sigf_estimated_1_L)^2)*exp((-1/(2*(((l_estimated_1_L))^2)))*((L_Predict_t(1,j) - Input_t_L(1,g))^2)) + ...
                    + ((sigf_estimated_2_L)^2)*exp((-1/(2*(((l_estimated_2_L))^2)))*((L_Predict_t(1,j) - Input_t_L(1,g))^2));

        g = g + 1;

    end

    j = j + 1;

end

B_ab_L = [B_L_ab zeros(length(L_Predict_t),length(Input_t_L))];
B_ba_L = (B_ab_L)';

j = 1;

for i = 1:1:length(Input_t_L)

    g = 1;

    for z = 1:1:length(Input_t_L)

        B_L_bb(j,g) = ((sigf_estimated_1_L)^2)*exp((-1/(2*(((l_estimated_1_L))^2)))*((Input_t_L(1,j) - Input_t_L(1,g))^2)) + ...
                    + ((sigf_estimated_2_L)^2)*exp((-1/(2*(((l_estimated_2_L))^2)))*((Input_t_L(1,j) - Input_t_L(1,g))^2));

        g = g + 1;

    end

    j = j + 1;

end

L_bb_Total = B_L_bb + noise_var_data_estimated_L.*eye(length(Input_t_L),length(Input_t_L));

j = 1;

for i = 1:1:length(Input_t_F0)

    g = 1;

    for z = 1:1:length(Input_t_F0)

        B_F0_bb(j,g) = ((sigf_estimated_1_F0)^2)*exp((-1/(2*(((l_estimated_1_F0))^2)))*((Input_t_F0(1,j) - Input_t_F0(1,g))^2)) + ...
                    + ((sigf_estimated_2_F0)^2)*exp((-1/(2*(((l_estimated_2_F0))^2)))*((Input_t_F0(1,j) - Input_t_F0(1,g))^2));

        g = g + 1;

    end

    j = j + 1;

end

F0_bb_Total = B_F0_bb + noise_var_data_estimated_F0.*eye(length(Input_t_F0),length(Input_t_F0));

B_bb_Total_L = [L_bb_Total zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) F0_bb_Total];

% Use Standard Results For Conditioning On A Gaussian Distribution To
% Obtain a Distribution Over L Function Values

mu_L = (B_ab_L)/(B_bb_Total_L)*Output_Data;
covariance_L = B_L_aa - (B_ab_L)/(B_bb_Total_L)*B_ba_L;

% Visualise Inferred L State Variable Function Values - MAP Estimate Used

figure(3 + run_repeat)
S2_L = diag(covariance_L);
f = [mu_L + 2.576*sqrt(S2_L); flip(mu_L - 2.576*sqrt(S2_L),1)];
h = fill([L_Predict_t'; flip(L_Predict_t',1)], f, [6 6 6]/10, 'EdgeColor', [5 5 5]/8);
set(h,'facealpha',1);
hold on
plot(L_Sensor.L_Noise_Free.Time,L_Sensor.L_Noise_Free.Data,'b--','LineWidth',2)
hold on
plot(L_Sensor.L_Noise.Time,L_Sensor.L_Noise.Data,'kx','MarkerSize',8,'LineWidth',2)
title('Draining Tank Dynamic Response: Random Input in F_0')
xlabel('Time (min)')
ylabel('L (m)')
hold on
plot(L_Predict_t,mu_L,'r','LineWidth',2)
hold on
x_Steady = linspace(0,Sim_Time,Sim_Time/5);
hold on
Steady = plot(x_Steady,L_initial.*ones(length(x_Steady),1),'m.','LineWidth',2,'MarkerSize',10);
legend('99% Credibility Interval','Underlying State Variable Dynamic Response','Liquid Level Sensor Measurements','L (State Variable) - Gaussian Process MAP Estimate','True Underlying Steady State','Location','NorthEast')
set(get(get(Steady,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
legend('Box','Off')
set(gca,'Box','off')
ylim([6.95 7.4])
% x_Steady = linspace(0,Sim_Time,Sim_Time/5);
% hold on
% plot(x_Steady,L_initial.*ones(length(x_Steady),1),'m.','LineWidth',2,'MarkerSize',10)
% hold on
% x_Upper_Limit = linspace(0,Sim_Time,Sim_Time);
% plot(x_Upper_Limit,7.7175.*ones(length(x_Upper_Limit),1),'m-','LineWidth',2) % Upper NOC Threshold Value
% hold on
% x_Lower_Limit = linspace(0,Sim_Time,Sim_Time);
% plot(x_Lower_Limit,6.3175.*ones(length(x_Lower_Limit),1),'m-','LineWidth',2) % Lower NOC Threshold Value
% legend('99% Prediction Interval (Future Observations)','Underlying Tank Liquid Level','Liquid Level Sensor Measurements','L (State Variabel) - MAP Estimate','True Steady State','Upper NOC Limit','Lower NOC Limit','Location','Best')

% Construct Gaussian Process To Infer Underlying Input Disturbance F0

% Pre-Allocate Memory For Kernel Matrices For Input Disturbance F0

B_F0_aa = zeros(length(F0_Predict_t),length(F0_Predict_t));
B_F0_ab = zeros(length(F0_Predict_t),length(Input_t_F0));

% Populate Kernel Matrices In An Elementwise Manner

j = 1;

for i = 1:1:length(L_Predict_t)

    g = 1;

    for z = 1:1:length(L_Predict_t)

        B_F0_aa(j,g) = ((sigf_estimated_1_F0)^2)*exp((-1/(2*(((l_estimated_1_F0))^2)))*((F0_Predict_t(1,j) - F0_Predict_t(1,g))^2)) + ...
                    + ((sigf_estimated_2_F0)^2)*exp((-1/(2*(((l_estimated_2_F0))^2)))*((F0_Predict_t(1,j) - F0_Predict_t(1,g))^2));

        g = g + 1;

    end

    j = j + 1;

end

% Addition Of "Jitter" For Numerical Stability

B_F0_aa = B_F0_aa + (1*10^-18)*eye(length(L_Predict_t),length(L_Predict_t));

j = 1;

for i = 1:1:length(F0_Predict_t)

    g = 1;

    for z = 1:1:length(Input_t_F0)

        B_F0_ab(j,g) = ((sigf_estimated_1_F0)^2)*exp((-1/(2*(((l_estimated_1_F0))^2)))*((F0_Predict_t(1,j) - Input_t_F0(1,g))^2)) + ...
                    + ((sigf_estimated_2_F0)^2)*exp((-1/(2*(((l_estimated_2_F0))^2)))*((F0_Predict_t(1,j) - Input_t_F0(1,g))^2));

        g = g + 1;

    end

    j = j + 1;

end

B_ab_F0 = [zeros(length(F0_Predict_t),length(Input_t_F0)) B_F0_ab];
B_ba_F0 = (B_ab_F0)';

B_bb_Total_F0 = [L_bb_Total zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) F0_bb_Total];

% Use Standard Results For Conditioning On A Gaussian Distribution To
% Obtain a Distribution Over F0 Function Values

mu_F0 = (B_ab_F0)/(B_bb_Total_F0)*Output_Data;
covariance_F0 = B_F0_aa - (B_ab_F0)/(B_bb_Total_F0)*B_ba_F0;

% Visualise Inferred F0 State Variable Function Values - MAP Estimate Used

figure(4 + run_repeat)
S2_F0 = diag(covariance_F0);
f = [mu_F0 + 2.576*sqrt(S2_F0); flip(mu_F0 - 2.576*sqrt(S2_F0),1)];
h = fill([F0_Predict_t'; flip(F0_Predict_t',1)], f, [6 6 6]/10, 'EdgeColor', [5 5 5]/8);
set(h,'facealpha',1);
hold on
plot(F0_Sensor.F0_Noise_Free.Time,F0_Sensor.F0_Noise_Free.Data,'b--','LineWidth',2)
hold on
plot(F0_Sensor.F0_Noise.Time,F0_Sensor.F0_Noise.Data,'kx','MarkerSize',8,'LineWidth',2)
title('F_0 Exogenous Input Disturbance')
xlabel('Time (min)')
ylabel('F_0 (m^3/min)')
hold on
plot(F0_Predict_t,mu_F0,'r','LineWidth',2)
hold on
x_Steady = linspace(0,Sim_Time,Sim_Time/5);
Steady = plot(x_Steady,F0_initial.*ones(length(x_Steady),1),'m.','LineWidth',2,'MarkerSize',10);
hold on
x_Upper_Limit = linspace(0,Sim_Time,1000);
Upper = plot(x_Upper_Limit,1.05*F0_initial*ones(length(x_Upper_Limit),1),'m--','LineWidth',2); % Upper NOC Threshold Value
hold on
x_Lower_Limit = linspace(0,Sim_Time,1000);
Lower = plot(x_Lower_Limit,0.95*F0_initial*ones(length(x_Lower_Limit),1),'m--','LineWidth',2); % Lower NOC Threshold Value
axis([0 Sim_Time 1.52 1.76])
legend('99% Confidence Interval','Underlying Exogenous Input Disturbance','Inlet Flowrate Sensor Measurements','F_0 (Exogenous Input) - Gaussian Process MAP Estimate','True Steady State','Upper NOC Limit','Lower NOC Limit','Location','SouthEast')
set(get(get(Lower,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
set(get(get(Upper,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
set(get(get(Steady,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
legend('Box','Off')
set(gca,'Box','off')
hold off

% Construct Gaussian Process To Infer Underlying State Variable Derivative

% State Variable Derivative d(L)/dt Prediction Range

dL_Predict_t = linspace(0 - Predict_Past, Sim_Time + Predict_Future,length(Input_t_L));

% Pre-Allocate Memory For Kernel Matrices For State Variable Derivative

B_L_Dev_aa = zeros(length(dL_Predict_t),length(dL_Predict_t));
B_L_Dev_ab = zeros(length(dL_Predict_t),length(Input_t_L));

j = 1;

for i = 1:1:length(dL_Predict_t)

    g = 1;

    for z = 1:1:length(dL_Predict_t)        

        B_L_Dev_aa(j,g) = (-sigf_estimated_1_L^2/((l_estimated_1_L^4)))*(((dL_Predict_t(1,g))^2) - (2*(dL_Predict_t(1,j))*(dL_Predict_t(1,g))) + ((dL_Predict_t(1,j))^2) - ((l_estimated_1_L^2)))*exp((-1/(2*(l_estimated_1_L^2)))*((dL_Predict_t(1,j) - dL_Predict_t(1,g))^2)) + ...
                    (-sigf_estimated_2_L^2/((l_estimated_2_L^4)))*(((dL_Predict_t(1,g))^2) - (2*(dL_Predict_t(1,j))*(dL_Predict_t(1,g))) + ((dL_Predict_t(1,j))^2) - ((l_estimated_2_L^2)))*exp((-1/(2*(l_estimated_2_L^2)))*((dL_Predict_t(1,j) - dL_Predict_t(1,g))^2));                       
       
                g = g + 1;

    end

    j = j + 1;

end

% Addition Of "Jitter" For Numerical Stability

B_L_Dev_aa = B_L_Dev_aa + (1*10^-18).*eye(length(dL_Predict_t),length(dL_Predict_t)); 

j = 1;

for i = 1:1:length(dL_Predict_t)

    g = 1;

    for z = 1:1:length(Input_t_L)        

        B_L_Dev_ab(j,g) = (-(sigf_estimated_1_L^2)/((l_estimated_1_L^2)))*((dL_Predict_t(1,j) - Input_t_L(1,g)))*exp((-1/(2*(l_estimated_1_L^2)))*((dL_Predict_t(1,j) - Input_t_L(1,g))^2)) + ...
                    (-(sigf_estimated_2_L^2)/((l_estimated_2_L^2)))*((dL_Predict_t(1,j) - Input_t_L(1,g)))*exp((-1/(2*(l_estimated_2_L^2)))*((dL_Predict_t(1,j) - Input_t_L(1,g))^2));
                       
        g = g + 1;

    end

    j = j + 1;

end

B_ab_L_Dev = [B_L_Dev_ab zeros(length(dL_Predict_t),length(Input_t_F0))];

B_ba_L_Dev = (B_ab_L_Dev)';

B_L_Dev_bb_Total = [L_bb_Total zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) F0_bb_Total];

% Use Standard Results For Conditioning On A Gaussian Distribution To
% Obtain a Distribution Over State Variable Derivative Function Values

mu_dL = (B_ab_L_Dev)/(B_L_Dev_bb_Total)*Output_Data;
covariance_dL = B_L_Dev_aa - (B_ab_L_Dev)/(B_L_Dev_bb_Total)*B_ba_L_Dev;

% Visualise Inferred State Variable Derivative Function Values - MAP
% Estimate Used

figure(5 + run_repeat)
S2_dL = diag(covariance_dL);
f = [mu_dL + 2.576*sqrt(S2_dL); flip(mu_dL - 2.576*sqrt(S2_dL),1)];
h = fill([dL_Predict_t'; flip(dL_Predict_t',1)], f, [6 6 6]/10, 'EdgeColor', [5 5 5]/8);
set(h,'facealpha',1);
hold on
plot(L_Derivative_Information.Time,L_Derivative_Information.Data,'b--','LineWidth',2)
plot(dL_Predict_t,mu_dL,'r','LineWidth',2)
title('Inferred State Derivative')
xlabel('Time (min)')
ylabel('L'' (m/min)')
legend('99% Confidence Interval','Underlying State Derivative (Numerical Integration)','State Derivative - Gaussian MAP Estimate','Location','NorthEast')
legend('Box','Off')
set(gca,'Box','off')
ylim([-4*10^-3 8*10^-3])

% Prior Hyperparameters

m0 = Parameter_Guess;
S0 = eye(length(m0),length(m0));

% Format The Required Basis Functions

Phi = [mu_F0 -sqrt(mu_L) -((Input_t_L').^b_k).*sqrt(mu_L)]; 

% Regression Covariance Matrix

Regress_Covariance = diag(covariance_dL).*eye(length(dL_Predict_t),length(dL_Predict_t));

% Posterior Distribution Results

An = ((inv(S0)) + ((Phi')/(Regress_Covariance)*Phi)); % Precision Matrix
Sn = inv(An); % Posterior Distribution Covariance Matrix
wn = (An)\(((Phi')/(Regress_Covariance)*mu_dL) + (S0\(m0))); % Posterior Distribution Mean Vector

% End Of Algorithm Execution Time Stopwatch - ode Regression

t_end_Bayes = toc;
Bayes_Model_Parameters = wn;

%% Marginalise Out Parameter 1/A

% - > Corresponds To Extracting Relevant Mean Vector And Covariance 
% Matrix Entries

wn_marg = [wn(2,1);wn(3,1)];
Sn_marg = [Sn(2,2) Sn(2,3); Sn(3,2) Sn(3,3)];

%% Statistical Properties: Gaussian Process ODE Regression
% Algorithm 4

% Ground Truth Comparison - Qaulity Of Parameter Estimates

fprintf('\n\nBayesian Results (based on MAP Estimates)\n\n')
fprintf('\tk_v_0/A: %0.4f (m^0.5/min)\n',wn_marg(1,1))
fprintf('\tA_v/A: %0.2d (m^0.5/min^2.07)\n',wn_marg(2,1))
fprintf('\tFlow Sensor Noise Variance: %0.9f (m^6/min^2)\n',noise_var_data_estimated_F0)
fprintf('\tFlow Sensor Noise Standard Deviation: %0.5f (m^3/min)\n',sqrt(noise_var_data_estimated_F0))
fprintf('\tLevel Sensor Noise Variance: %0.9f (m^2)\n',noise_var_data_estimated_L)
fprintf('\tLevel Sensor Noise Standard Deviation: %0.5f (m)\n\n',sqrt(noise_var_data_estimated_L))

fprintf('\tk_v_0/A %% Error: %0.2f%% \n',((abs(wn_marg(1,1) - (k_v/A)))/(k_v/A))*100)
fprintf('\tA_v/A %% Error: %0.2f%% \n',((abs(wn_marg(2,1) - (A_k/A)))/(A_k/A))*100)
fprintf('\tLevel Sensor Noise Standard Deviation %% Error: %0.2f%% \n',((abs(sqrt(noise_var_data_estimated_L) - Level_Sensor_SDev))/(Level_Sensor_SDev))*100)
fprintf('\tFlow Sensor Noise Standard Deviation %% Error: %0.2f%% \n',((abs(sqrt(noise_var_data_estimated_F0) - Flow_Sensor_SDev))/(Flow_Sensor_SDev))*100)

% Parameter Marginal Credibility Interval

Cov_Bayes_Parameters =  diag(Sn_marg);

fprintf('\n\t99%% Marginal Credibility Interval For k_v/A: [%0.4f (m^0.5/min), %0.4f (m^0.5/min)]',wn_marg(1,1) - z_score*sqrt(Cov_Bayes_Parameters(1,1)), wn_marg(1,1) + z_score*sqrt(Cov_Bayes_Parameters(1,1)))
fprintf('\n\t99%% Marginal Credibility Interval For A_v/A: [%0.2d (m^0.5/min^2.07), %0.2d (m^0.5/h^2.07)]\n',wn_marg(2,1) - z_score*sqrt(Cov_Bayes_Parameters(2,1)), wn_marg(2,1) + z_score*sqrt(Cov_Bayes_Parameters(2,1)))

% Bayes Mean Valve Replacement Time

tf_Bayes = ((1.2*wn(2,1) - wn(2,1))*(1/(wn(3,1))))^(1/b_k);
fprintf('\nBayesian Valve Replacement Time: %0.2f minutes (~%0.2f days)\n',tf_Bayes,tf_Bayes*(1/(24*60)))

% Bayes Upper Valve Replacement Time

tf_Bayes_Lower = ((1.2*(wn(2,1) + z_score*sqrt(Cov_Bayes_Parameters(1,1))) - (wn(2,1) + z_score*sqrt(Cov_Bayes_Parameters(1,1))))*(1/((wn(3,1) + z_score*sqrt(Cov_Bayes_Parameters(2,1))) )))^(1/b_k)*(1/(24*60));

% Bayes Lower Valve Replacement Time

tf_Bayes_Upper = ((1.2*(wn(2,1) - z_score*sqrt(Cov_Bayes_Parameters(1,1))) - (wn(2,1) - z_score*sqrt(Cov_Bayes_Parameters(1,1))))*(1/((wn(3,1) - z_score*sqrt(Cov_Bayes_Parameters(2,1))) )))^(1/b_k)*(1/(24*60));

% -> Visualise Parameter 1 Inference Results

figure(run_repeat + 6)

% Visualise Marginal Posterior Distribution over Parameter 1

x = linspace(0.94*wn(2,1),1.06*wn(2,1),1000);
plot(x,normpdf(x,wn(2,1),sqrt(Sn(2,2))) + 200,'r','LineWidth',2)
set(gca,'YTick',[],'Box','off')
set(gca,'YColor',[1 1 1])
ylim([0 2100])
xlim([wn(2,1) - 3.5*sqrt(Sn(2,2)) wn(2,1) + 3.5*sqrt(Sn(2,2)) ])
hold on

% Visualise 99% Credibility Interval For Parameter 1

x = wn(2,1);
y = 100;
errhigh = z_score*sqrt(Sn(2,2));
errlow = z_score*sqrt(Sn(2,2));
errorbar(x,y,errlow,errhigh,'horizontal','ro','LineWidth',2,'MarkerFaceColor','r','MarkerSize',8)
hold on
plot(k_v/A,y,'bx','LineWidth',3,'MarkerFaceColor','b','MarkerSize',10)
text(wn(2,1) - 0.0001,220,'(k_\nu_o/A)_M_A_P','FontSize',12)
text(k_v/A - 0.00005,220,'(k_\nu_o/A)_T_r_u_e','FontSize',12)
xlabel('k_\nu_o/A (m^0^.^5/min)')
set(gca,'YTick',[],'Box','off')
set(gca,'YColor',[1 1 1])

% -> Visualise Parameter 2 Inference Results

figure(run_repeat + 7)

% Visualise Marginal Posterior Distribution over Parameter 2

x = linspace(0.1*wn(3,1),1.5*wn(3,1),1000);
plot(x,normpdf(x,wn(3,1),sqrt(Sn(3,3))) + 0.2*10^7,'r','LineWidth',2)
set(gca,'YTick',[],'Box','off')
set(gca,'YColor',[1 1 1])
ylim([0 2.3*10^7])
xlim([wn(3,1) - 3.5*sqrt(Sn(3,3)) wn(3,1) + 3.5*sqrt(Sn(3,3)) ])
hold on

% Visualise 99% Credibility Interval For Parameter 2

x = wn(3,1);
y = 0.1*10^7;
errhigh = z_score*sqrt(Sn(3,3));
errlow = z_score*sqrt(Sn(3,3));
errorbar(x,y,errlow,errhigh,'horizontal','ro','LineWidth',2,'MarkerFaceColor','r','MarkerSize',8)
hold on
plot(A_k/A,y,'bx','LineWidth',3,'MarkerFaceColor','b','MarkerSize',10)
text(wn(3,1) - 0.2*10^-7,0.25*10^7,'(A_\nu/A)_M_A_P','FontSize',12)
text(A_k/A - 0.1*10^-7,0.25*10^7,'(A_\nu/A)_T_r_u_e','FontSize',12)
xlabel('A_\nu/A (m^0^.^5/min^2^.^0^7)')
set(gca,'YTick',[],'Box','off')
set(gca,'YColor',[1 1 1])

% Algorithm Execution Time

fprintf('\n\tGaussian Process Hyperparameter Optimisation Time: %0.4f seconds\n',t_end_optimisation)
fprintf('\n\tRegression Time: %0.4f seconds\n',t_end_Bayes)
fprintf('\n\tElapsed Algorithm Time: %0.4f seconds\n',t_end_Bayes + t_end_optimisation)

% Joint Parameter Credibilty Interval
% ->  Ellipsoid Representation

figure(run_repeat + 8)
[EigenVec, EigenVal] = eig(Sn_marg); % Calculate Eigenvalues And Eigenvectors

[Largest_EigenVec_Index, Num] = find(EigenVal == max(max(EigenVal))); % Obtain The Index Of The Largest Eigenvector
Largest_EigenVec = EigenVec(:,Largest_EigenVec_Index);

Largest_EigenVal = max(max(EigenVal)); % Obtain The Largest Eigenvalue

if (Largest_EigenVec_Index == 1) % Obtain The Smallest Eigenvector And EigenValue
    
    Smallest_EigenVal = max(EigenVal(:,2));
    Smallest_Eigen_Vec = EigenVec(:,2);
    
else
    
    Smallest_EigenVal = max(EigenVal(:,1));
    Smallest_Eigen_Vec = EigenVec(:,1);
    
end

Angle = atan2(Largest_EigenVec(2),Largest_EigenVec(1)); % Calculate The Angle Between The Cartesian x-axis And The Largest Eigenvector

if Angle < 0
    
    Angle = Angle + 2*pi; % Shift Angle To Be Between 0 and 2pi
    
end

Average = wn_marg'; % Extract Mean Inferred Parameter Values

Chi_Value = sqrt(9.210); % Obtain Radius For The Error Ellipse
Theta_Grid = linspace(0,2*pi,40000);
k_v_Ac = Average(1);
A_Ac = Average(2);

A1 = Chi_Value*sqrt(Largest_EigenVal);
B1 = Chi_Value*sqrt(Smallest_EigenVal); 

Ellipse_X_r = A1*cos(Theta_Grid); % Ellipse Cartesian x-axis Coordinates
Ellipse_Y_r = B1*sin(Theta_Grid); % Ellipse Cartesian y-axis Coordinates

R = [cos(Angle) sin(Angle); -sin(Angle) cos(Angle)]; % Ellipse Rotation Matrix

r_Ellipse = [Ellipse_X_r;Ellipse_Y_r]'*R; % Rotate Ellipse

Bayes_Ellipse = plot(r_Ellipse(:,1) + k_v_Ac,r_Ellipse(:,2) + A_Ac,'r','LineWidth',2); % Visualise Ellipse
hold on
plot(wn_marg(1,1),wn_marg(2,1),'o','Color','r','LineWidth',2,'MarkerSize',8,'MarkerFaceColor','r')
ylabel('A_\nu/A (m^0^.^5/min^2^.^0^7)')
xlabel('k_\nu_o/A (m^0^.^5/min)')
hold on

% Visualise The Expected Mean Response
    
figure(run_repeat + 9)

Mean_Response_Range = linspace(0,50000,100);
Phi_New = [ones(length(linspace(0,50000,100)),1) ((Mean_Response_Range).^b_k)'];
Mean_Vector_Response = Phi_New*wn_marg;
Mean_response_Variance = Phi_New*Sn_marg*(Phi_New)';

S2_Mean = diag(Mean_response_Variance);
f = [Mean_Vector_Response + 2.576*sqrt(S2_Mean);flip(Mean_Vector_Response - 2.576*sqrt(S2_Mean),1)];
h = fill([(Mean_Response_Range.*(1/(60*24)))'; flip((Mean_Response_Range.*(1/(60*24)))',1)], f, [6 6 6]/10, 'EdgeColor', [5 5 5]/8);
set(h,'facealpha',1);
hold on
plot(kv_time_range.*(1/(60*24)),kv_t(kv_time_range),'b--','LineWidth',2)
hold on
plot((Mean_Response_Range.*(1/(60*24)))',Mean_Vector_Response,'r','LineWidth',2)
title('Valve Degradation Model')
xlabel('Time (day)')
ylabel('k_\nu/A (m^0^.^5/min)')
xlim([0 30])
legend('99% Credibility Interval','True Underlying Valve Degradation Model','Valve Degradation Model (MAP Estimate)','Location','SouthEast')
legend('BoxOff')
set(gca,'Box','off')
hold off

%% Nonlinear Least Squares ODE Regression (Gauss-Newton Implementation)
%  Algorithm 2

% Data Required For Regression

Output_Data = L_Sensor.L_Noise.Data;
Input_t = L_Sensor.L_Noise.Time;

% Start Algorithm Execution Stopwatch

tic;

% NSIG To Establish Convergence

NSIG = 4;

% Pre-Allocate Memory For Least Squares Objective Function

Least_Squares_Objective = zeros(n,1);

% Pre-Allocate Memory For Updated Model Parameters Per Iteration

Frequentist_Model_Parameters = zeros(n,length(Parameter_Guess));

% Pre-Allocate Memory For Numerical Integration Results

L_Time_Range = zeros(length(Output_Data),1);
L_Results = zeros(length(Output_Data),1);
GR1 = zeros(length(Output_Data),1);
GR2 = zeros(length(Output_Data),1);
GR3 = zeros(length(Output_Data),1);

% Pre-Allocate Memory For Numerical Integration Results - Bisection Rule

L_Time_Range_rho_1 = zeros(length(Output_Data),1);
L_Results_rho_1 = zeros(length(Output_Data),1);
L_Time_Range_rho_halved = zeros(length(Output_Data),1);
L_Results_rho_halved = zeros(length(Output_Data),1);

% Pre-Alloctate Memory For Convergence Evaluation

L_Time_Range_rho = zeros(length(Output_Data),1);
L_Results_rho = zeros(length(Output_Data),1);

for re_regress = 1:1:1 % Can Repeat Regression If Required
    
    % Pre-Allocate Memory For Least Squares Objective Function
    
    Least_Squares_Objective = zeros(n,1);

    % Pre-Allocate Memory For Updated Model Parameters
    
    Frequentist_Model_Parameters = zeros(n,length(Parameter_Guess));

    % Pre-Allocate Memory For Numerical Integration Results
    
    L_Time_Range = zeros(length(Output_Data),1);
    L_Results = zeros(length(Output_Data),1);
    GR1 = zeros(length(Output_Data),1);
    GR2 = zeros(length(Output_Data),1);
    GR3 = zeros(length(Output_Data),1);
    
    % Pre-Allocate Memory For Numerical Integration Results - Bisection Rule
    
    L_Time_Range_rho_1 = zeros(length(Output_Data),1);
    L_Results_rho_1 = zeros(length(Output_Data),1);
    L_Time_Range_rho_halved = zeros(length(Output_Data),1);
    L_Results_rho_halved = zeros(length(Output_Data),1);

    % Pre-Alloctate Memory For Convergence Evaluation
    
    L_Time_Range_rho = zeros(length(Output_Data),1);
    L_Results_rho = zeros(length(Output_Data),1);

    for j = 1:1:n
        
        Frequentist_Model_Parameters(j,1:length(Parameter_Guess)) = (Parameter_Guess)';

        i = 0; % Initialise Input Disturbance Counter
        Intial_L = [L_initial;0;0;0]; % Initial Value For "Experimental" Conditions
        
        K = Parameter_Guess.*eye(length(Parameter_Guess),length(Parameter_Guess));

        for tstart = 0:1:Sim_Time

            i = i + 1; % Increment Input Disturbance Counter
            tspan = [tstart tstart + 0.5 tstart + 1]; % Numerical Integration Time Span

            % Set Up Matrix Differential Equation
            
            Matrix_Diff_Equation = @(t,y) ...
                 [Parameter_Guess(1,1)*F0_Sensor.F0_Noise.Data(i,1) - Parameter_Guess(2,1)*sqrt(y(1)) - Parameter_Guess(3,1)*((t)^b_k)*sqrt(y(1)); ... % L = y(1)
                 (-1/(2*sqrt(y(1))))*(Parameter_Guess(2,1) + Parameter_Guess(3,1)*((t)^b_k))*y(2) + F0_Sensor.F0_Noise.Data(i,1)*K(1,1); ... % GR2 = y(2)
                 (-1/(2*sqrt(y(1))))*(Parameter_Guess(2,1) + Parameter_Guess(3,1)*((t)^b_k))*y(3) - sqrt(y(1))*K(2,2); % GR3 = y(3)
                 (-1/(2*sqrt(y(1))))*(Parameter_Guess(2,1) + Parameter_Guess(3,1)*((t)^b_k))*y(4) - ((t)^b_k)*sqrt(y(1))*K(3,3)]; % GR4 = y(4)

            % Use ode45 To Numerically Integrate Matrix Differential Equation
            
            [t_temp, y_temp] = ode45(Matrix_Diff_Equation,tspan,Intial_L);
            L_Time_Range(tstart + 1,1) = t_temp(1,1);
            Intial_L = [y_temp(3,1) y_temp(3,2) y_temp(3,3) y_temp(3,4)];
            L_Results(tstart + 1,1) = y_temp(1,1);
            GR1(tstart + 1,1) = y_temp(1,2);
            GR2(tstart + 1,1) = y_temp(1,3);       
            GR3(tstart + 1,1) = y_temp(1,4); 

        end

        % Compute Integrated Model Values
        
        t_sampling_period = L_Time_Range(L_Sensor.L_Noise.Time + 1,1);
        D = 1; % Sensitivity Matrix
        L_Model = D*L_Results(t_sampling_period + 1,1); % Extract ode Model Predicted Results

        % Evaluate Least Squares Objective Function
        
        Least_Squares_Objective(j,1) = sum(((Output_Data) - L_Model).^2);

        % Set Up Sensitivity Matrices - Jacobian (Vectorised)
        
        J = [GR1 GR2 GR3]; % <- From ode45 Numerical Integration

        % Set Up Linear Equations To Minimise The Simple Least Squares Error Function
        % -> Use The Reduced Sensitivity Matrix Gr 
       
        A_R = ((J)')*(D')*D*J;
        b_R = ((J)')*(D')*(Output_Data - L_Model);
        
        % Step Update
        
        delta_k_R = A_R\b_R; % Solve System Of Linear Equations

        % Bisection Rule For Determining The Stepping Parameter rho
        % -> Performing a One Dimensional Line Search

        % Set Up Matrix Differential Equation For rho = 1
        
        k_rho_1 = 1*K*delta_k_R;

        i = 0; % Initialise Input Disturbance Counter
        Intial_L = [L_initial;0;0;0]; % Initial Value For "Experimental" Conditions

        for tstart = 0:1:Sim_Time

            i = i + 1; % Increment Input Disturbance Counter
            tspan = [tstart tstart + 0.5 tstart + 1]; % Numerical Integration Time Span

            % Set Up Matrix Differential Equation For rho = 1
            
            Matrix_Diff_Equation_rho_1 = @(t,y) ...            
                 [(Parameter_Guess(1,1) + k_rho_1(1,1))*F0_Sensor.F0_Noise.Data(i,1) - (Parameter_Guess(2,1) + k_rho_1(2,1))*sqrt(y(1)) - (Parameter_Guess(3,1) + k_rho_1(3,1))*((t)^b_k)*sqrt(y(1)); ... % L = y(1)
                 (-1/(2*sqrt(y(1))))*((Parameter_Guess(2,1) + k_rho_1(2,1)) + (Parameter_Guess(3,1) + k_rho_1(3,1))*((t)^b_k))*y(2) + F0_Sensor.F0_Noise.Data(i,1)*K(1,1); ... % GR2 = y(2)
                 (-1/(2*sqrt(y(1))))*((Parameter_Guess(2,1) + k_rho_1(2,1)) + (Parameter_Guess(3,1) + k_rho_1(3,1))*((t)^b_k))*y(3) - sqrt(y(1))*K(2,2); % GR3 = y(3)
                 (-1/(2*sqrt(y(1))))*((Parameter_Guess(2,1) + k_rho_1(2,1)) + (Parameter_Guess(3,1) + k_rho_1(3,1))*((t)^b_k))*y(4) - ((t)^b_k)*sqrt(y(1))*K(3,3)]; % GR4 = y(4)

            % Use ode45 To Numerically Integrate Matrix Differential Equation
            
            [t_temp_rho_1, y_temp_rho_1] = ode45(Matrix_Diff_Equation_rho_1,tspan,Intial_L);
            L_Time_Range_rho_1(tstart + 1,1) = t_temp_rho_1(1,1);
            Intial_L = [y_temp_rho_1(3,1) y_temp_rho_1(3,2) y_temp_rho_1(3,3) y_temp_rho_1(3,4)];
            L_Results_rho_1(tstart + 1,1) = y_temp_rho_1(1,1);

        end

        % Compute Integrated Model Values
        
        t_sampling_period_rho_1 = L_Time_Range_rho_1(L_Sensor.L_Noise.Time + 1,1);
        D = 1; % Sensitivity Matrix
        L_Model_rho_1 = D*L_Results_rho_1(t_sampling_period_rho_1 + 1,1); % Extract ode Model Predicted Results

        if sum(((Output_Data) - L_Model_rho_1).^2) < Least_Squares_Objective(j,1)

            rho = 1; % Stepping Parameter Size

        else

            rho = 1; % Stepping Parameter Size

            for u = 1:1:max_trials

                rho = 0.5*rho; % Keep Halving Stepping Parameter Size

                % Set Up Matrix Differential Equation For New rho
                
                k_rho_halved = rho*K*delta_k_R;

                i = 0; % Initialise Input Disturbance Counter
                Intial_L = [L_initial;0;0;0]; % Initial Value For "Experimental" Conditions

                for tstart = 0:1:Sim_Time

                    i = i + 1; % Increment Input Disturbance Counter                
                    tspan = [tstart tstart + 0.5 tstart + 1]; % Numerical Integration Time Span

                    % Set Up Matrix Differential Equation
                    
                    Matrix_Diff_Equation_rho_halved = @(t,y) ...                    
                        [(Parameter_Guess(1,1) + k_rho_halved(1,1))*F0_Sensor.F0_Noise.Data(i,1) - (Parameter_Guess(2,1) + k_rho_halved(2,1))*sqrt(y(1)) - (Parameter_Guess(3,1) + k_rho_halved(3,1))*((t)^b_k)*sqrt(y(1)); ... % L = y(1)
                        (-1/(2*sqrt(y(1))))*((Parameter_Guess(2,1) + k_rho_halved(2,1)) + (Parameter_Guess(3,1) + k_rho_halved(3,1))*((t)^b_k))*y(2) + F0_Sensor.F0_Noise.Data(i,1)*K(1,1); ... % GR2 = y(2)
                        (-1/(2*sqrt(y(1))))*((Parameter_Guess(2,1) + k_rho_halved(2,1)) + (Parameter_Guess(3,1) + k_rho_halved(3,1))*((t)^b_k))*y(3) - sqrt(y(1))*K(2,2); % GR3 = y(3)
                        (-1/(2*sqrt(y(1))))*((Parameter_Guess(2,1) + k_rho_halved(2,1)) + (Parameter_Guess(3,1) + k_rho_halved(3,1))*((t)^b_k))*y(4) - ((t)^b_k)*sqrt(y(1))*K(3,3)]; % GR4 = y(4)

                    % Use ode45 To Numerically Integrate Matrix Differential Equation
                    
                    [t_temp_rho_halved, y_temp_rho_halved] = ode45(Matrix_Diff_Equation_rho_halved,tspan,Intial_L);
                    L_Time_Range_rho_halved(tstart + 1,1) = t_temp_rho_halved(1,1);
                    Intial_L = [y_temp_rho_halved(3,1) y_temp_rho_halved(3,2) y_temp_rho_halved(3,3) y_temp_rho_halved(3,4)];
                    L_Results_rho_halved(tstart + 1,1) = y_temp_rho_halved(1,1);

                end

                % Compute Integrated Model Values
                
                t_sampling_period_rho_halved = L_Time_Range_rho_halved(L_Sensor.L_Noise.Time + 1,1);
                D = 1; % Sensitivity Matrix
                L_Model_rho_halved = D*L_Results_rho_halved(t_sampling_period_rho_halved + 1,1); % Extract ode Model Predicted Results

                if sum(((Output_Data) - L_Model_rho_halved).^2) < Least_Squares_Objective(j,1)

                    break;

                end           

            end

        end

        Parameter_Guess = Parameter_Guess + rho*K*delta_k_R;
        fprintf('\nParameter Updates Per Iteration: [%0.4f,%0.4f,%0.2d]',Parameter_Guess(1,1),Parameter_Guess(2,1),Parameter_Guess(3,1))

        if (1/length(Parameter_Guess))*sum(abs(K*delta_k_R./Parameter_Guess)) <= 10^(-NSIG) % Break Iteration If Convergence Is Achieved

            Frequentist_Model_Parameters(j + 1,1:length(Parameter_Guess)) = (Parameter_Guess)';

            i = 0; % Initialise Input Disturbance Counter
            Intial_L = [L_initial;0;0;0]; % Initial Value For "Experimental" Conditions    

            for tstart = 0:1:Sim_Time

                i = i + 1; % Increment Input Disturbance Counter
                tspan = [tstart tstart + 0.5 tstart + 1]; % Numerical Integration Time Span

                % Set Up Matrix Differential Equation
                
                Matrix_Diff_Equation = @(t,y) ...
                    [Parameter_Guess(1,1)*F0_Sensor.F0_Noise.Data(i,1) - Parameter_Guess(2,1)*sqrt(y(1)) - Parameter_Guess(3,1)*((t)^b_k)*sqrt(y(1)); ... % L = y(1)
                    (-1/(2*sqrt(y(1))))*(Parameter_Guess(2,1) + Parameter_Guess(3,1)*((t)^b_k))*y(2) + F0_Sensor.F0_Noise.Data(i,1)*K(1,1); ... % GR2 = y(2)
                    (-1/(2*sqrt(y(1))))*(Parameter_Guess(2,1) + Parameter_Guess(3,1)*((t)^b_k))*y(3) - sqrt(y(1))*K(2,2); % GR3 = y(3)
                    (-1/(2*sqrt(y(1))))*(Parameter_Guess(2,1) + Parameter_Guess(3,1)*((t)^b_k))*y(4) - ((t)^b_k)*sqrt(y(1))*K(3,3)]; % GR4 = y(4)

                % Use ode45 To Numerically Integrate Matrix Differential Equation
                
                [t_temp, y_temp] = ode45(Matrix_Diff_Equation,tspan,Intial_L);
                L_Time_Range(tstart + 1,1) = t_temp(1,1);
                Intial_L = [y_temp(3,1) y_temp(3,2) y_temp(3,3) y_temp(3,4)];
                L_Results(tstart + 1,1) = y_temp(1,1);
                GR1(tstart + 1,1) = y_temp(1,2);
                GR2(tstart + 1,1) = y_temp(1,3); 
                GR3(tstart + 1,1) = y_temp(1,4); 

            end

            % Compute Integrated Model Values
            
            t_sampling_period = L_Time_Range(L_Sensor.L_Noise.Time + 1,1);
            D = 1; % Sensitivity Matrix
            L_Model = D*L_Results(t_sampling_period + 1,1); % Extract ode Model Predicted Results

            % Evaluate Least Squares Objective Function
            
            Least_Squares_Objective(j + 1,1) = sum(((Output_Data) - L_Model).^2);
            Matrix_A = ((([(1/K(1,1)).*GR1 (1/K(2,2)).*GR2 (1/K(3,3)).*GR3])')*[(1/K(1,1)).*GR1 (1/K(2,2)).*GR2 (1/K(3,3)).*GR3]);
            Noise_estimate = (Least_Squares_Objective(j + 1,1)/(N - length(Parameter_Guess)));
            Cov_A = (Least_Squares_Objective(j + 1,1)/(N - length(Parameter_Guess))).*inv((([(1/K(1,1)).*GR1 (1/K(2,2)).*GR2 (1/K(3,3)).*GR3])')*[(1/K(1,1)).*GR1 (1/K(2,2)).*GR2 (1/K(3,3)).*GR3]);
            
            break;

        end
               
    end
    
   % Parameter_Guess = Frequentist_Model_Parameters(j + 1,1:length(Parameter_Guess))';
       
end

% End Of Algorithm Execution Time Stopwatch

t_end_Frequentist = toc;

Frequentist_Model_Parameters = (Frequentist_Model_Parameters(j + 1,1:length(Parameter_Guess)))';

% Visualise If Least Squares Objective Function Has Converged To a Minimum

figure(run_repeat + 10)
plot(1:j+1,Least_Squares_Objective(1:j+1),'kx--','LineWidth',2,'MarkerSize',12)
title('Least Squares Objective Function')
xlabel('p^t^h iteration')
ylabel('LS(w)')
set(gca,'XTick',1:j+1)
xlim([1 j+1])

%% Maximum Likelihood Regression Parameters Required

Frequentist_Model_Parameters_Second = [Frequentist_Model_Parameters(2,1);Frequentist_Model_Parameters(3,1)];
Cov_A_Second = [Cov_A(2,2) Cov_A(2,3) ; Cov_A(3,2) Cov_A(3,3)];

%% Statistical Properties: Nonlinear Least Squares ODE Regression (Gauss-Newton Implementation)
% Algorithm (2)

% Ground Truth Comparison - Quality of Parameter Estimates

fprintf('\n\nFrequentist Parameter (Maximum Likelihood Estimates)\n\n')
fprintf('\tk_v_0/Ac: %0.4f (m^0.5/min)\n',Frequentist_Model_Parameters_Second(1,1))
fprintf('\tA_v/Ac: %0.2d (m^0.5/min^2.07)\n',Frequentist_Model_Parameters_Second(2,1))
fprintf('\tLevel Sensor Noise Variance: %0.9f (m^2)\n',(Least_Squares_Objective(j + 1,1)/(N - length(Parameter_Guess))))
fprintf('\tLevel Sensor Noise Standard Deviation: %0.5f (m)\n\n',sqrt((Least_Squares_Objective(j + 1,1)/(N - length(Parameter_Guess)))))

fprintf('\tk_v/Ac: %% Error: %0.2f%% \n',((abs(Frequentist_Model_Parameters_Second(1,1) - (k_v/A)))/(k_v/A))*100)
fprintf('\tA_v/Ac: %% Error: %0.2f%% \n',((abs(Frequentist_Model_Parameters_Second(2,1) - (A_k/A)))/(A_k/A))*100)
fprintf('\tLevel Sensor Noise Standard Deviation %% Error: %0.2f%% \n',((abs((sqrt((Least_Squares_Objective(j + 1,1)/(N - length(Parameter_Guess))))) - Level_Sensor_SDev))/(Level_Sensor_SDev))*100)

% Parameter Marginal Confidence Interval

Cov_Frequentist_Parameters =  diag(Cov_A_Second);

fprintf('\n\t99%% Marginal Confidence Interval For k_v/A is: [%0.4f (), %0.4f ()]',Frequentist_Model_Parameters_Second(1,1) - z_score*sqrt(Cov_Frequentist_Parameters(1,1)), Frequentist_Model_Parameters_Second(1,1) + z_score*sqrt(Cov_Frequentist_Parameters(1,1)))
fprintf('\n\t99%% Marginal Confidence Interval For A_v/Ac is: [%0.2d (), %0.2d ()]\n',Frequentist_Model_Parameters_Second(2,1) - z_score*sqrt(Cov_Frequentist_Parameters(2,1)), Frequentist_Model_Parameters_Second(2,1) + z_score*sqrt(Cov_Frequentist_Parameters(2,1)))

% Frequentist Mean Valve Replacement Time

tf_Freq = ((1.2*Frequentist_Model_Parameters_Second(1,1) - Frequentist_Model_Parameters_Second(1,1))*(1/(Frequentist_Model_Parameters_Second(2,1) )))^(1/b_k);
fprintf('\nFrequentist Valve Replacement Time: %0.2f minutes (~%0.2f days)\n',tf_Freq,tf_Freq*(1/(24*60)))

% Frequentist Upper Valve Replacement Time

tf_Freq_Lower = ((1.2*(Frequentist_Model_Parameters_Second(1,1) + z_score*sqrt(Cov_Frequentist_Parameters(1,1))) - (Frequentist_Model_Parameters_Second(1,1) + z_score*sqrt(Cov_Frequentist_Parameters(1,1))))*(1/((Frequentist_Model_Parameters_Second(2,1) + z_score*sqrt(Cov_Frequentist_Parameters(2,1))) )))^(1/b_k)*(1/(24*60));

% Frequentist Lower Valve Replacement Time

tf_Freq_Upper = ((1.2*(Frequentist_Model_Parameters_Second(1,1) - z_score*sqrt(Cov_Frequentist_Parameters(1,1))) - (Frequentist_Model_Parameters_Second(1,1) - z_score*sqrt(Cov_Frequentist_Parameters(1,1))))*(1/((Frequentist_Model_Parameters_Second(2,1) - z_score*sqrt(Cov_Frequentist_Parameters(2,1))) )))^(1/b_k)*(1/(24*60));

% -> Visualise 99% Marginal Confidence Interval For Parameter 1

figure(12 + run_repeat)

x = Frequentist_Model_Parameters_Second(1,1);
y = 35;
errhigh = z_score*sqrt(Cov_Frequentist_Parameters(1,1));
errlow = z_score*sqrt(Cov_Frequentist_Parameters(1,1));
errorbar(x,y,errlow,errhigh,'horizontal','o','Color',[0.4660 0.6740 0.1880],'LineWidth',2,'MarkerFaceColor',[0.4660 0.6740 0.1880],'MarkerSize',8)
hold on
plot(k_v/A,y,'bx','LineWidth',3,'MarkerFaceColor','b','MarkerSize',10)
text(Frequentist_Model_Parameters_Second(1,1),35.15,'(k_\nu_o/A)_M_L','FontSize',12)
text(k_v/A,35.15,'(k_\nu_o/A)_T_r_u_e','FontSize',12)
xlabel('k_\nu_o/A (m^0^.^5/min)')
set(gca,'YTick',[],'Box','off')
set(gca,'YColor',[1 1 1])
%xlim([1.6*10^-7 3.3*10^-7])

% Visualise 99% Marginal Confidence Interval For Parameter 2

figure(13 + run_repeat)

x = Frequentist_Model_Parameters_Second(2,1);
y = 35;
errhigh = z_score*sqrt(Cov_Frequentist_Parameters(2,1));
errlow = z_score*sqrt(Cov_Frequentist_Parameters(2,1));
errorbar(x,y,errlow,errhigh,'horizontal','o','Color',[0.4660 0.6740 0.1880],'LineWidth',2,'MarkerFaceColor',[0.4660 0.6740 0.1880],'MarkerSize',8)
hold on
plot(A_k/A,y,'bx','LineWidth',3,'MarkerFaceColor','b','MarkerSize',10)
text(Frequentist_Model_Parameters_Second(2,1),35.15,'(A_\nu/A)_M_L','FontSize',12)
text(A_k/A,35.15,'(A_\nu/A)_T_r_u_e','FontSize',12)
xlabel('A_\nu/A (1/min)')
set(gca,'YTick',[],'Box','off')
set(gca,'YColor',[1 1 1])
xlim([1.6*10^-7 3.3*10^-7])

% Algorithm Execution Time

fprintf('\n\tElapsed Algorithm Time: %0.4f seconds\n',t_end_Frequentist)

% Joint Parameter Confidence Interval
% ->  Ellipsoid Representation

figure(run_repeat + 8)
F_dist = 3.86;
p = length(Frequentist_Model_Parameters_Second);

[EigenVec, EigenVal] = eig(Cov_A_Second); % Calculate Eigenvalues And Eigenvectors

[Largest_EigenVec_Index, num] = find(EigenVal == max(max(EigenVal))); % Obtain The Index Of The Largest Eigenvector
Largest_EigenVec = EigenVec(:,Largest_EigenVec_Index);

Largest_EigenVal = max(max(EigenVal)); % Obtain The Largest Eigenvalue

if (Largest_EigenVec_Index == 1) % Obtain The Smallest Eigenvector And EigenValue
    
    Smallest_EigenVal = max(EigenVal(:,2));
    Smallest_EigenVec = EigenVec(:,2);
    
else
    
    Smallest_EigenVal = max(EigenVal(:,1));
    Smallest_EigenVec = EigenVec(:,1);
    
end

Angle = atan2(Largest_EigenVec(2),Largest_EigenVec(1)); % Calculate The Angle Between The Cartesian x-axis And The Largest Eigenvector

if Angle < 0
    
    Angle = Angle + 2*pi; % Shift Angle To Be Between 0 and 2pi
    
end

Average = Frequentist_Model_Parameters_Second'; % Extract Mean Inferred Parameter Values

Chi_Value = sqrt(p*F_dist); % Obtain Radius For The Error Ellipse
Theta_Grid = linspace(0,2*pi,500);
kv_Ac_Mean = Average(1);
Ak_Ac_Mean = Average(2);

A1 = Chi_Value*sqrt(Largest_EigenVal);
B1 = Chi_Value*sqrt(Smallest_EigenVal);

Ellipse_X_r = A1*cos(Theta_Grid); % Ellipse Cartesian x-axis Coordinates
Ellipse_Y_r = B1*sin(Theta_Grid); % Ellipse Cartesian y-axis Coordinates

R = [cos(Angle) sin(Angle); -sin(Angle) cos(Angle)]; % Ellipse Rotation Matrix

r_Ellipse = [Ellipse_X_r;Ellipse_Y_r]'*R; % Rotate Ellipse

Freq_Ellipse = plot(r_Ellipse(:,1) + kv_Ac_Mean,r_Ellipse(:,2) + Ak_Ac_Mean,'Color',[0.4660, 0.6740, 0.1880],'LineWidth',2); % Visualise Ellipse
hold on
plot(Frequentist_Model_Parameters_Second(1,1),Frequentist_Model_Parameters_Second(2,1),'o','Color',[0.4660, 0.6740, 0.1880],'LineWidth',2,'MarkerSize',8,'MarkerFaceColor',[0.4660, 0.6740, 0.1880])
plot(k_v/A,A_k/A,'bx','LineWidth',3,'MarkerFaceColor','b','MarkerSize',10)
legend('Bayesian 99% Epplise','[(k_\nu_o/A)_M_A_P  (A_\nu/A)_M_A_P]^T','Frequentist 99% Ellispe','[(k_\nu_o/A)_M_L  (A_\nu/A)_M_L]^T','[(k_\nu_o/A)_T_r_u_e  (A_\nu/A)_T_r_u_e]^T','Location','SouthEast')
legend('BoxOff')
set(get(get(Bayes_Ellipse,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
set(get(get(Freq_Ellipse,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
ylabel('A_\nu/A (m^0^.^5/min^2^.^0^7)')
xlabel('k_\nu_o/A (m^0^.^5/min)')
ylim([0.75*10^-7 3.6*10^-7])
hold off

% Expected Mean Response Values

figure(run_repeat + 15)
    
plot(r_Ellipse(:,1) + kv_Ac_Mean,r_Ellipse(:,2) + Ak_Ac_Mean,'Color',[0.4660, 0.6740, 0.1880],'LineWidth',2) % Visialise Ellipse
hold on
plot(Frequentist_Model_Parameters_Second(1,1),Frequentist_Model_Parameters_Second(2,1),'x','Color',[0.4660, 0.6740, 0.1880],'LineWidth',2,'MarkerSize',12)
ylabel('A_\nu/A (m^0^.^5/min^2^.^0^7)')
xlabel('k_\nu_o/A (m^0^.^5/min)')
hold on

Number_frequentist_samples = 1000;

% -> Select At Random a Angle and Chi_Value To Sample From Confidence
% Region
    
Theta_Draw = 2*pi*rand(Number_frequentist_samples,1)';
Chi_Value = 1.001*sqrt(p*F_dist)*rand(Number_frequentist_samples,1)';
A_Chi_Drawn = Chi_Value.*sqrt(Largest_EigenVal);
B_Chi_Drawn = Chi_Value.*sqrt(Smallest_EigenVal);
Ellipse_X_r_Sample = A_Chi_Drawn.*cos(Theta_Draw); % Ellipse Cartesian x-axis Coordinates For Drawn Samples
Ellipse_Y_r_Sample = B_Chi_Drawn.*sin(Theta_Draw); % Ellipse Cartesian y-axis Coordinates For Drawn Samples
r_Ellipse_Sample = [Ellipse_X_r_Sample;Ellipse_Y_r_Sample]'*R; % Rotate Ellipse

%  -> Visualise Samples Drawn From Ellipse
    
plot(r_Ellipse_Sample(:,1) + kv_Ac_Mean,r_Ellipse_Sample(:,2) + Ak_Ac_Mean,'kx')
hold on

%  -> Visualise Samples on Ellipse Boundaries
    
plot(r_Ellipse(:,1) + kv_Ac_Mean,r_Ellipse(:,2) + Ak_Ac_Mean,'kx')
hold on

figure(run_repeat + 16)

Frequentist_Sample_1_Parameter = [r_Ellipse(:,1) + kv_Ac_Mean;r_Ellipse_Sample(:,1) + kv_Ac_Mean];
Frequentist_Sample_2_Parameter = [r_Ellipse(:,2) + Ak_Ac_Mean;r_Ellipse_Sample(:,2) + Ak_Ac_Mean];

% Pre-Allocate Memory For Function Evaluations

Function_Evaluation_Frequentist = zeros(length(Number_frequentist_samples),length(Mean_Response_Range));

Frequentist_Curves = @(t,Frequentist_Sample_1,Frequentist_Sample_2) Frequentist_Sample_1 + Frequentist_Sample_2.*t.^(b_k);

for plot_samples_line = 1:1:length(Frequentist_Sample_1_Parameter)

    Function_Evaluation_Frequentist(plot_samples_line,:) = (Frequentist_Curves(Mean_Response_Range,Frequentist_Sample_1_Parameter(plot_samples_line,1),Frequentist_Sample_2_Parameter(plot_samples_line,1)))';
    hold on

end

% Extract Minimun and Maximum Function Values At Each Time Point

% Pre-Allocate Memory For Minimum And Maximum Function Values
   
Minimum_function = zeros(length(Mean_Response_Range),1);  
Maximum_function = zeros(length(Mean_Response_Range),1);  
    
for extract_values = 1:1:length(Mean_Response_Range)    
    
    Minimum_function(extract_values,1) = min(Function_Evaluation_Frequentist(:,extract_values));
    Maximum_function(extract_values,1) = max(Function_Evaluation_Frequentist(:,extract_values));

end
 
plot(((Mean_Response_Range)*(1/(24*60)))',Minimum_function,'o','Color',[0 0 0],'MarkerSize',1.5,'MarkerFaceColor',[0 0 0])
hold on
Lower = plot(((Mean_Response_Range)*(1/(24*60)))',Maximum_function,'o','Color',[0 0 0],'MarkerSize',1.5,'MarkerFaceColor',[0 0 0]);
hold on
plot(kv_time_range.*(1/(60*24)),kv_t(kv_time_range),'b--','LineWidth',2)
hold on
plot(Mean_Response_Range*(1/(24*60)),Frequentist_Model_Parameters_Second(1,1) + Frequentist_Model_Parameters_Second(2,1).*Mean_Response_Range.^(b_k),'Color',[0.4660, 0.6740, 0.1880],'LineWidth',2)
title('Valve Degradation Model')
xlabel('Time (day)')
ylabel('k_\nu/A (m^0^.^5/min)')
legend('99% Upper/Lower Confidence Limit','99% Lower EMR Limit','True Underlying Valve Degradation Model','Valve Degradation Model (ML Estimate)','Location','SouthEast')
set(get(get(Lower,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
legend('BoxOff')
xlim([0 30])
hold off

%% Cost-Benefit Trade-Off Analysis For Draining Tank Process Unit

% Ground Truth Parameter Value

k_v_A = 0.09; % m^0.5/min
 
% Assumed Draining Tank Profit Function

Profit = @(t) -5*(1000*t.*(k_v_A.^2 - 1.44*k_v_A + 0.0648) + 10*(t.^2 - 28*t + 196)) + 9800;

% Cost Analysis Evaluation Range

t = linspace(0,40,3000);

% Visualise Profit Function For Ground Truth Parameter Value

figure(19 + run_repeat)
plot(t,100.*Profit(t),'b-','LineWidth',2.5)
xlabel('Time [Days]')
ylabel('Profit [R]')
hold on

% Evaluate Profit Function Over Selected Time Range To Find Maximum Profit
% Value

Profit_time_range = Profit(t);

% Obtain Maximum Profit Value Entry

[Row_Entry,Column_Entry] = find(Profit_time_range == max(Profit_time_range(:)));

% Obtain Time Index At Which The Maximum Profit Occurs

t_max_profit = t(Row_Entry,Column_Entry);
fprintf('\nTrue Maximum Profit Reached At: %0.02f days',t_max_profit)

plot(t_max_profit,100*max(Profit_time_range(:)),'mx','MarkerSize',15,'LineWidth',3)
xlim([16.4 17.2])
hold on

% Bayesian Predicted Maximum Profit
% Calculated From Expected Value Under The Marginal Posterior Distribution
% Obtained From Algorithm 4

t_Bayesian_Max_Profit = (1400 - 5000*((wn_marg(1,1)^2) + (Sn_marg(1,1)) - (1.44*wn_marg(1,1)) + 0.0648))/100;
fprintf('\n\nBayesian Maximum Profit Reached At: %0.02f days',t_Bayesian_Max_Profit)
plot(t_Bayesian_Max_Profit,100*Profit(t_Bayesian_Max_Profit),'rx','LineWidth',3,'MarkerSize',15)
hold on

% Frequentist Predicted Maximum Profit - Based On Least Squares
% Estimate [t_Freq = argmax(t) {p(t,k_o_hat)}] Obtained From Algorithm 2
% - (Coincides With Frequentist ML Estimate)

t_Frequentist_Max_Profit_hat = (1400 - 5000*((Frequentist_Model_Parameters_Second(1,1)^2) - (1.44*(Frequentist_Model_Parameters_Second(1,1))) + 0.0648))/100;
fprintf('\n\nFrequentist Maximum Profit Reached At: %0.02f days',t_Frequentist_Max_Profit_hat)
plot(t_Frequentist_Max_Profit_hat,100*Profit(t_Frequentist_Max_Profit_hat),'x','Color',[0.4660, 0.6740, 0.1880],'LineWidth',3,'MarkerSize',15)
hold on
legend('Underlying Draining Tank Profit Function','Point Of Maximum Profit','Bayesian Profit Point','Frequentist Profit Point','Location','SouthEast')
legend('BoxOff')
set(gca,'Box','off')