Project: Bayesian Parameter Estimation for Process Monitoring
Author: M Basson, Department of Process Engineering, Stellenbosch University, South Africa
Email for queries: kroon@sun.ac.za

Project Description: This project focuses on estimating the parameters of (lumped system) algebraic dynamic 
models that are nonlinear in the model parameters and (lumped system) ordinary differential equation dynamic 
models that are linear in the model parameters with applications to Process Monitoring via parameter tracking.

The code associated with the project is based on two case studies inspired by Marlin (2000) - see reference below

Case Study Reference: Marlin, T E. 2000. Process Control: Designing Processes And Control Systems For Dynamic
Performance. 2nd Ed. Boston: McGraw-Hill. 

The case studies include:

(1) an isothermal, constant volume CSTR with first-order reaction kinetics
(2) a liquid draining tank with a partial flow restriction

These case studies are further extended for Process Monitoring applications via parameter tracking
by including:

(1) an isothermal, constant volume CSTR (first-order reaction kinetics) with catalyst decay
(2) a liquid draining tank with valve degradation

Concepts required to understand the project and the associated code:

(1) Bayesian statistical inference
(2) Bayesian linear regression
(2) Gaussian process non-parametric regression
(4) Variational Bayesian inference (mean-field family-based)
(5) Gradient ascent optimization

Installation: The user requires MATLAB R2018b (or higher) to successfully execute the MATLAB script files 
and Simulink simulation environments. After installing MATLAB R2018b, the MATLAB script files associated with
each case study can be executed (ensure the MATLAB script file has access to the directory path where the
Simulink simulation file is stored). When executing the MATLAB script file, MATLAB will automatically call the
appropriate Simulink simulation file, run the Simulink model, followed by saving and closing the Simulink simulation
file. When running the MATLAB script files, results may vary slightly from run to run, because of the gradient-based
optimization routine used to optimize the Gaussian process hyperparameters and the CAVI optimization routine used
for variational Bayesian inference.

The project uses four different algorithms to regress the dynamic model parameters, namely:

Algorithm 1: Nonlinear simple least squares regression [Gauss-Newton implementation] (Algebraic dynamic model - Case Study 1) 
Algorithm 2: Variational Bayesian nonlinear regression (Algebraic dynamic model - Case Study 1)
Algorithm 3: ODE simple least squares regression [Gauss-Newton implementation] (ODE Dynamic model - Case study 2 and 3)
Algorithm 4: Gaussian process-based ODE regression (ODE Dynamic model - Case study 2 and 3)

Algorithm 3 and 4 are also implemented for the extended case studies 2 (CSTR with catalyst decay) and 3 (Draining tank with
valve degradation)

License Used: University of Illinois/NCSA Open Source License

(Reference 1: https://otm.illinois.edu/disclose-protect/illinois-open-source-license)
(Reference 2: https://opensource.org/licenses/NCSA) 
(Reference 3: https://en.wikipedia.org/wiki/University_of_Illinois/NCSA_Open_Source_License)

The author would like to acknowledge the following supervising staff for their support and contributions to
the project:

(1) Prof Lidia Auret 
(2) Dr Steve Kroon 
(3) Prof Roelof Coetzer 
(4) Dr Jamie Cripwell 

Financial assistance from SASOL towards the proposed research project is hereby acknowledged. Opinions expressed 
and conclusions arrived at are those of the author and are not necessarily beneficial to SASOL.    
