%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

         %%%% Ordinary Differential Equation Regression %%%%     
            %%%% -> Linear In The ODE Model Parameters %%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%% Case Study 3: Draining Tank (Example 3.6)

%%%% Reference: Marlin, T E. 2000. Process Control: Designing Processes And
%%%% Control Systems For Dynamic Performance. 2nd ed. Boston: McGraw-Hill.

%% Clear Memory And Command Window

close all;
clear;
clc;

%% Draining Tank Model

% -> Derived From First Principles (Conservation of Mass)
% -> No Input Disturbance Structure Assumed
% -> Input Disturbance In F0 Modeled With a Draw From a Gaussian Process
%    Between a Normal Operating Conditions Bandwidth
% -> ODE Model is Nonlinear In The Model State Variable L
% -> ODE Model is Linear In The Model Parameters

% Dynamic Model Simulation Information

F0_initial = 100; % m^3/h - Inlet Flow Rate
F0_initial = F0_initial*(1/60); % m^3/min - Inlet Flow Rate (Converted To Minutes)
L_initial = 7.0; % m - Initial Draining Tank Level
A = 7; % m^2 - Draining Tank Cross-Sectional Area
k_v = F0_initial/sqrt(L_initial); % (m^2.5/min) - Flow Restriction Coeffcient
delta_F0 = -10*(1/60); % m^3/min - Disturbance Step Change in Draining Tank Inlet Flowrate

% Simulation Run Time

Sim_Time = 120; % Minutes

% Simulation Ground Truth Parameter Values

fprintf('\nTrue Parameter Values (Set During Simulation)\n\n')
fprintf('\t1/A: %0.4f (1/m^2)\n',1/A)
fprintf('\tk_v/A: %0.4f (m^0.5/min)\n',k_v/A)

%% Load F0 Input Disturbance Data
%  Input Disturbance Generated Independently From A Draw From A Gaussian
%  Process

load('Case_Study_3_Input_Disturbance.mat')

% Read MATLAB Data To Simulink

RandomF0.time = Case_Study_3_Input_Disturbance(:,1);
RandomF0.signals.values = Case_Study_3_Input_Disturbance(:,2);
RandomF0.signals.dimensions = 1;

%% Data Generation Process - Performed In Simulink

% Sensor Measurement Sampling Time

SampleTime = 1; % Minutes

% Sensor Measurement Details

% Flow Sensor

rng(1,'Twister') % Ensures Repeatability
Flow_Sensor_Seed = randi(1*10^9,1,1);
Flow_Sensor_Var = 2*10^-5;
Flow_Sensor_SDev = sqrt(Flow_Sensor_Var);

fprintf('\tFlow Sensor Noise Standard Deviation: %0.5f (m^3/min)\n',Flow_Sensor_SDev)

% Level Sensor

rng(2,'Twister') % Ensure Repeatability
Level_Sensor_Seed = randi(1*10^9,1,1);
Level_Sensor_Var = 4*10^-5;
Level_Sensor_SDev = sqrt(Level_Sensor_Var);

fprintf('\tLevel Sensor Noise Standard Deviation: %0.5f (m)\n',Level_Sensor_SDev)

% -> Run Simulink Model From MATLAB

open('Case_Study_3_Linear_ODE_Regression.slx')
sim('Case_Study_3_Linear_ODE_Regression.slx')
close_system('Case_Study_3_Linear_ODE_Regression.slx')

% -> Visualise Simulink Data Generation Results

% Flow Sensor Measurements

figure(1)
plot(F0_Sensor.F0_Noise_Free.Time,F0_Sensor.F0_Noise_Free.Data,'b--','LineWidth',2)
hold on
plot(F0_Sensor.F0_Noise.Time,F0_Sensor.F0_Noise.Data,'kx','MarkerSize',8,'LineWidth',2)
%title('F_0 Exogenous Input Disturbance')
xlabel('Time (min)')
ylabel('F_0 (m^3/min)')
hold on
x_Upper_Limit = linspace(0,Sim_Time,1000);
plot(x_Upper_Limit,1.05*F0_initial*ones(length(x_Upper_Limit),1),'m--','LineWidth',2) % Upper NOC Threshold Value
hold on
x_Lower_Limit = linspace(0,Sim_Time,1000);
Lower = plot(x_Lower_Limit,0.95*F0_initial*ones(length(x_Lower_Limit),1),'m--','LineWidth',2); % Lower NOC Threshold Value
hold on
x_Steady = linspace(0,Sim_Time,Sim_Time/5);
plot(x_Steady,F0_initial.*ones(length(x_Steady),1),'m.','LineWidth',2,'MarkerSize',10)
legend('Underlying Exogenous Input Disturbance','Inlet Flow Rate Sensor Measurements','Upper/Lower NOC Limit','Lower NOC Limit','True Underlying Steady State','Location','SouthEast')
set(get(get(Lower,'Annotation'),'LegendInformation'),'IconDisplayStyle','off')
legend('Box','Off')
axis([0 Sim_Time 1.52 1.76])
set(gca,'Box','off')
hold off

% Tank Level Sensor Measurements

figure(2)
plot(L_Sensor.L_Noise_Free.Time,L_Sensor.L_Noise_Free.Data,'b--','LineWidth',2)
hold on
plot(L_Sensor.L_Noise.Time,L_Sensor.L_Noise.Data,'kx','MarkerSize',8,'LineWidth',2)
%title('Draining Tank Dynamic Response: Random Input in F_0')
xlabel('Time (min)')
ylabel('L (m)')
set(gca,'Box','off')
x_Steady = linspace(0,Sim_Time,Sim_Time/5);
hold on
plot(x_Steady,L_initial.*ones(length(x_Steady),1),'m.','LineWidth',2,'MarkerSize',10)
legend('Underlying State Variable Dynamic Response','Liquid Level Sensor Measurements','True Underlying Steady State','Location','NorthEast')
legend('Box','Off')
ylim([6.8 7.1])
% hold on
% x_Upper_Limit = linspace(0,Sim_Time,Sim_Time);
% plot(x_Upper_Limit,7.7175.*ones(length(x_Upper_Limit),1),'m-','LineWidth',2) % Upper NOC Threshold Value
% hold on
% x_Lower_Limit = linspace(0,Sim_Time,Sim_Time);
% plot(x_Lower_Limit,6.3175.*ones(length(x_Lower_Limit),1),'m-','LineWidth',2) % Lower NOC Threshold Value
% legend('Underlying Tank Liquid Level','Tank Liquid Level Sensor Measurements','True Steady State','Upper NOC Limit','Lower NOC Limit','Location','Best')

% Number Of Liquid Level Sensor Measurements

N = length(L_Sensor.L_Noise.Data);

%% Initial Model Parameter Guess And Number Of Algorithm Iterations 

% -> Intial Guess Condition Used For:
    % (1) Gaussian Process ODE Regression (Algorithm 4)
    % (2) Nonlinear Least Squares ODE Regression (Gauss-Newton 
    % Implementation) (Algorithm 2)

rng('Shuffle','Twister')
Parameter_1 = ((1/10)*(1/A)) + (((10)*(1/A)) - ((1/10)*(1/A))).*rand(1,1);
rng('Shuffle','Twister')
Parameter_2 = ((1/10)*(k_v/A)) + (((10)*(k_v/A)) - ((1/10)*(k_v/A))).*rand(1,1);

% Populate Vector Containing Initial Model Parameter Guesses

Parameter_Guess = [Parameter_1;Parameter_2];

fprintf('\nInitial Parameter Guess [%0.4f,%0.4f]\n',Parameter_1,Parameter_2)

% -> Number Of Trial Iterations:
    % (2) Nonlinear Least Squares ODE Regression (Gauss-Newton 
    % Implementation) (Algorithm 2)
  
n = 2000;

% -> Empirical Iterations/Trials
    % (2) Nonlinear Least Squares ODE Regression (Gauss-Newton 
    % Implementation) (Algorithm 2)
     
max_trials = 500;

%% Gaussian Process ODE Regression
%  (Algorithm 4)

% Start Algorithm Execution Stopwatch - Optimisation Routine

tic;

% -> Optimise Gaussian Process Kernel Hyperparameters Via Gradient Ascent

% Tank Level Data

Output_Data_L = L_Sensor.L_Noise.Data;
Input_t_L = L_Sensor.L_Noise.Time';

% Tank Inlet Flow Rate Data

Output_Data_F0 = F0_Sensor.F0_Noise.Data;  
Input_t_F0 = F0_Sensor.F0_Noise.Time';

% Collect Data In a Vector For Joint Optimisation

Output_Data = [Output_Data_L;Output_Data_F0];
Input_t = [Input_t_L Input_t_F0];

% Pre-allocate Memory For 10 Optimisation Routines
% - > 10 Unknown Gaussian Process Parameters + Log Marginal Likelihood Function
% Value

Max_GP_Objective = zeros(10,11);

% Number Of Gradient Ascent Iterations

GA_Iterations = 3000;

% Stepping Parameter Size

Gamma = 0.01;

for run_repeat = 1:1:10
    
    fprintf('\nGaussian Process Hyperparameter Optimisation: %0.0f',run_repeat)
    
    % Randomly Initialise Each Log Unknown Gaussian Process Parameter
    
    % Tank Level Gaussian Process Parameters
    % Constrained To Be Positive
        
    rng('Shuffle','Twister')
    theta_1_L = 1.37 + 0.2.*randn(1,1);
    rng('Shuffle','Twister')
    theta_2_L = -2.12 + 0.2.*randn(1,1);
    rng('Shuffle','Twister')
    theta_3_L = -10.1 + 0.1.*randn(1,1);
    rng('Shuffle','Twister')
    theta_4_L = 6.13 + 0.2.*randn(1,1);
    rng('Shuffle','Twister')
    theta_5_L = 2.66 + 0.2.*randn(1,1);
    
    % Inlet Flow Rate Gaussian Process Parameters
    % Constrained To Be Positive
        
    rng('Shuffle','Twister')
    theta_1_F0 = 3.62 + 0.2.*randn(1,1);
    rng('Shuffle','Twister')
    theta_2_F0 = -4.28 + 0.2.*randn(1,1);
    rng('Shuffle','Twister')
    theta_3_F0 = -10.8 + 0.1.*randn(1,1);
    rng('Shuffle','Twister')
    theta_4_F0 = 7.35 + 0.2.*randn(1,1);
    rng('Shuffle','Twister')
    theta_5_F0 = 2.49 + 0.2.*randn(1,1);
    
    % Gaussian Process Parameters
    
    % Tank Level Gaussian Process Parameters
        
    sigf_square_1_L = exp(theta_1_L);
    sigf_square_2_L = exp(theta_2_L);
    noise_var_data_L = exp(theta_3_L);
    %sqrt(noise_var_data_L)
    l_square_1_L = exp(theta_4_L);
    l_square_2_L = exp(theta_5_L);
    
    % Inlet Flow Rate Gaussian Process Parameters
        
    sigf_square_1_F0 = exp(theta_1_F0);
    sigf_square_2_F0 = exp(theta_2_F0);
    noise_var_data_F0 = exp(theta_3_F0);
    %sqrt(noise_var_data_F0)
    l_square_1_F0 = exp(theta_4_F0);
    l_square_2_F0 = exp(theta_5_F0);
    
    % Pre-Allocate Memory For Kernel Matrices
    
    % Tank Level Gaussian Process Matrices
        
    B_Data_L = zeros(length(Input_t_L),length(Input_t_L));
    B_theta_1_L = zeros(length(Input_t_L),length(Input_t_L));
    B_theta_2_L = zeros(length(Input_t_L),length(Input_t_L));
    B_theta_3_L = zeros(length(Input_t_L),length(Input_t_L));
    B_theta_4_L = zeros(length(Input_t_L),length(Input_t_L));
    B_theta_5_L = zeros(length(Input_t_L),length(Input_t_L));
    
    % Inlet Flow Rate Gaussian Process Matrices
        
    B_Data_F0 = zeros(length(Input_t_F0),length(Input_t_F0));
    B_theta_1_F0 = zeros(length(Input_t_F0),length(Input_t_F0));
    B_theta_2_F0 = zeros(length(Input_t_F0),length(Input_t_F0));
    B_theta_3_F0 = zeros(length(Input_t_F0),length(Input_t_F0));
    B_theta_4_F0 = zeros(length(Input_t_F0),length(Input_t_F0));
    B_theta_5_F0 = zeros(length(Input_t_F0),length(Input_t_F0));

    % Pre-Allocate Memory For Gaussian Process Objective Function
    
    GP_Objective = zeros(GA_Iterations,1);
    
    % Construct Kernel Matrix With Data For The Tank Level
    
    j = 1;
    
    for i = 1:1:length(Input_t_L)
        
        g = 1;
        
        for z = 1:1:length(Input_t_L)
            
            B_Data_L(j,g) = sigf_square_1_L*exp((-1/(2*(l_square_1_L)))*((Input_t_L(1,j) - Input_t_L(1,g)))^2) + ...
                        + sigf_square_2_L*exp((-1/(2*(l_square_2_L)))*((Input_t_L(1,j) - Input_t_L(1,g)))^2);
            
            g = g + 1;
            
        end
        
        j = j + 1;
        
    end
    
    B_Data_L = B_Data_L + noise_var_data_L.*eye(length(Input_t_L),length(Input_t_L));

    % Construct Kernel Matrix With Data For The Inlet Flow Rate
    
    j = 1;
    
    for i = 1:1:length(Input_t_F0)
        
        g = 1;
        
        for z = 1:1:length(Input_t_F0)
            
            B_Data_F0(j,g) = sigf_square_1_F0*exp((-1/(2*(l_square_1_F0)))*((Input_t_F0(1,j) - Input_t_F0(1,g)))^2) + ...
                        + sigf_square_2_F0*exp((-1/(2*(l_square_2_F0)))*((Input_t_F0(1,j) - Input_t_F0(1,g)))^2);
            
            g = g + 1;
            
        end
        
        j = j + 1;
        
    end
    
    B_Data_F0 = B_Data_F0 + noise_var_data_F0.*eye(length(Input_t_L),length(Input_t_L));
    
    B_Data = [B_Data_L zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) B_Data_F0];
         
    % Evaluate Gausian Process Objective Function For Initial Guess Of
    % Gaussian Process Hyperparameters
    
    GP_Objective(1,1) = -0.5*(2*sum(log(diag(chol(B_Data))))) ...
                        -0.5*((Output_Data)')/(B_Data)*Output_Data...
                        -0.5*length(Input_t)*log(2*pi);
                    
    for p = 1:1:GA_Iterations
        
    % Construct Kernel Matrix With Data For The Tank Level
    
    j = 1;
    
    for i = 1:1:length(Input_t_L)
        
        g = 1;
        
        for z = 1:1:length(Input_t_L)
            
            B_Data_L(j,g) = sigf_square_1_L*exp((-1/(2*(l_square_1_L)))*((Input_t_L(1,j) - Input_t_L(1,g))^2)) + ...
                        + sigf_square_2_L*exp((-1/(2*(l_square_2_L)))*((Input_t_L(1,j) - Input_t_L(1,g))^2));
            
            g = g + 1;
            
        end
        
        j = j + 1;
        
    end
    
    B_Data_L = B_Data_L + noise_var_data_L.*eye(length(Input_t_L),length(Input_t_L));
    
    % Construct Kernel Matrix With Data For The Inlet Flowrate
    
    j = 1;
    
    for i = 1:1:length(Input_t_F0)
        
        g = 1;
        
        for z = 1:1:length(Input_t_F0)
            
            B_Data_F0(j,g) = sigf_square_1_F0*exp((-1/(2*(l_square_1_F0)))*((Input_t_F0(1,j) - Input_t_F0(1,g)))^2) + ...
                        + sigf_square_2_F0*exp((-1/(2*(l_square_2_F0)))*((Input_t_F0(1,j) - Input_t_F0(1,g)))^2);
            
            g = g + 1;
            
        end
        
        j = j + 1;
        
    end
    
    B_Data_F0 = B_Data_F0 + noise_var_data_F0.*eye(length(Input_t_L),length(Input_t_L));
    
    B_Data = [B_Data_L zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) B_Data_F0]; 
    
    % Tank Level Derivative Matrices
    
    % Construct Theta_1_L Derivative Matrix
    
    j = 1;
        
    for i = 1:1:length(Input_t_L)
        
        g = 1;
        
        for z = 1:1:length(Input_t_L)
            
            B_theta_1_L(j,g) = sigf_square_1_L*exp((-1/(2*(l_square_1_L)))*((Input_t_L(1,j) - Input_t_L(1,g))^2));

            g = g + 1;
            
        end
        
        j = j + 1;
        
    end       
    
    % Construct Theta_2_L Derivative Matrix
    
    j = 1;
        
    for i = 1:1:length(Input_t_L)
        
        g = 1;
        
        for z = 1:1:length(Input_t_L)
            
            B_theta_2_L(j,g) = sigf_square_2_L*exp((-1/(2*(l_square_2_L)))*((Input_t_L(1,j) - Input_t_L(1,g))^2));

            g = g + 1;
            
        end
        
        j = j + 1;
        
    end    
    
    % Construct Theta_3_L Derivative Matrix
    
    B_theta_3_L = noise_var_data_L.*eye(length(Input_t_L),length(Input_t_L));
    
    % Construct Theta_4_L Derivative Matrix
    
    j = 1;
        
    for i = 1:1:length(Input_t_L)
        
        g = 1;
        
        for z = 1:1:length(Input_t_L)
            
            B_theta_4_L(j,g) = ((sigf_square_1_L*((Input_t_L(1,j) - Input_t_L(1,g))^2))/(2*l_square_1_L))*exp((-1/(2*(l_square_1_L)))*((Input_t_L(1,j) - Input_t_L(1,g))^2));

            g = g + 1;
            
        end
        
        j = j + 1;
        
    end         
    
    % Construct Theta_5_L Derivative Matrix
    
    j = 1;
        
    for i = 1:1:length(Input_t_L)
        
        g = 1;
        
        for z = 1:1:length(Input_t_L)
            
            B_theta_5_L(j,g) = ((sigf_square_2_L*((Input_t_L(1,j) - Input_t_L(1,g))^2))/(2*l_square_2_L))*exp((-1/(2*(l_square_2_L)))*((Input_t_L(1,j) - Input_t_L(1,g))^2));

            g = g + 1;
            
        end
        
        j = j + 1;
        
    end         

    % Inlet Flow Rate Derivative Matrices  
    
    % Construct Theta_1_F0 Derivative Matrix
    
    j = 1;
        
    for i = 1:1:length(Input_t_F0)
        
        g = 1;
        
        for z = 1:1:length(Input_t_F0)
            
            B_theta_1_F0(j,g) = sigf_square_1_F0*exp((-1/(2*(l_square_1_F0)))*((Input_t_F0(1,j) - Input_t_F0(1,g))^2));

            g = g + 1;
            
        end
        
        j = j + 1;
        
    end      
    
    % Construct Theta_2_F0 Derivative Matrix
    
    j = 1;
        
    for i = 1:1:length(Input_t_F0)
        
        g = 1;
        
        for z = 1:1:length(Input_t_F0)
            
            B_theta_2_F0(j,g) = sigf_square_2_F0*exp((-1/(2*(l_square_2_F0)))*((Input_t_F0(1,j) - Input_t_F0(1,g))^2));

            g = g + 1;
            
        end
        
        j = j + 1;
        
    end   
    
    % Construct Theta_3_F0 Derivative Matrix
    
    B_theta_3_F0 = noise_var_data_F0.*eye(length(Input_t_F0),length(Input_t_F0));    
    
    % Construct Theta_4_F0 Derivative Matrix
    
    j = 1;
        
    for i = 1:1:length(Input_t_F0)
        
        g = 1;
        
        for z = 1:1:length(Input_t_F0)
            
            B_theta_4_F0(j,g) = ((sigf_square_1_F0*((Input_t_F0(1,j) - Input_t_F0(1,g))^2))/(2*l_square_1_F0))*exp((-1/(2*(l_square_1_F0)))*((Input_t_F0(1,j) - Input_t_F0(1,g))^2));

            g = g + 1;
            
        end
        
        j = j + 1;
        
    end     
    
    % Construct Theta_5_F0 Derivative Matrix
    
    j = 1;
        
    for i = 1:1:length(Input_t_F0)
        
        g = 1;
        
        for z = 1:1:length(Input_t_F0)
            
            B_theta_5_F0(j,g) = ((sigf_square_2_F0*((Input_t_F0(1,j) - Input_t_F0(1,g))^2))/(2*l_square_2_F0))*exp((-1/(2*(l_square_2_F0)))*((Input_t_F0(1,j) - Input_t_F0(1,g))^2));

            g = g + 1;
            
        end
        
        j = j + 1;
        
    end     

    % Calculate alpha
    
    alpha = (B_Data)\Output_Data;
    
    % Tank Level Gaussian Process Parameter Updates
    % -> Gradient Ascent
    
    new_theta_1_L = theta_1_L + Gamma*(0.5*trace(((alpha*(alpha')) - (inv([B_Data_L zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) B_Data_F0])))*([B_theta_1_L zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) B_theta_1_F0])));
    new_theta_2_L = theta_2_L + Gamma*(0.5*trace(((alpha*(alpha')) - (inv([B_Data_L zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) B_Data_F0])))*([B_theta_2_L zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) B_theta_2_F0])));
    new_theta_3_L = theta_3_L + Gamma*(0.5*trace(((alpha*(alpha')) - (inv([B_Data_L zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) B_Data_F0])))*([B_theta_3_L zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) B_theta_3_F0])));
    new_theta_4_L = theta_4_L + Gamma*(0.5*trace(((alpha*(alpha')) - (inv([B_Data_L zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) B_Data_F0])))*([B_theta_4_L zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) B_theta_4_F0])));
    new_theta_5_L = theta_5_L + Gamma*(0.5*trace(((alpha*(alpha')) - (inv([B_Data_L zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) B_Data_F0])))*([B_theta_5_L zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) B_theta_5_F0])));
    
    % Inlet Flow Rate Gaussian Process Parameter Updates
    % -> Gradient Ascent
    
    new_theta_1_F0 = theta_1_F0 + Gamma*(0.5*trace(((alpha*(alpha')) - (inv([B_Data_L zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) B_Data_F0])))*([B_theta_1_L zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) B_theta_1_F0])));
    new_theta_2_F0 = theta_2_F0 + Gamma*(0.5*trace(((alpha*(alpha')) - (inv([B_Data_L zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) B_Data_F0])))*([B_theta_2_L zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) B_theta_2_F0])));
    new_theta_3_F0 = theta_3_F0 + Gamma*(0.5*trace(((alpha*(alpha')) - (inv([B_Data_L zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) B_Data_F0])))*([B_theta_3_L zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) B_theta_3_F0])));
    new_theta_4_F0 = theta_4_F0 + Gamma*(0.5*trace(((alpha*(alpha')) - (inv([B_Data_L zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) B_Data_F0])))*([B_theta_4_L zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) B_theta_4_F0])));
    new_theta_5_F0 = theta_5_F0 + Gamma*(0.5*trace(((alpha*(alpha')) - (inv([B_Data_L zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) B_Data_F0])))*([B_theta_5_L zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) B_theta_5_F0])));   
    
    % Tank Level Gaussian Process Log Parameter Assignment
    
    theta_1_L = new_theta_1_L;
    theta_2_L = new_theta_2_L;
    theta_3_L = new_theta_3_L;
    theta_4_L = new_theta_4_L;
    theta_5_L = new_theta_5_L;
    
    % Inlet Flow Rate Gaussian Process Log Parameter Assignment
    
    theta_1_F0 = new_theta_1_F0;
    theta_2_F0 = new_theta_2_F0;
    theta_3_F0 = new_theta_3_F0;
    theta_4_F0 = new_theta_4_F0;
    theta_5_F0 = new_theta_5_F0;
    
    % Tank Level Gaussian Process Parameters
    
    sigf_square_1_L = exp(theta_1_L);
    sigf_square_2_L = exp(theta_2_L);
    noise_var_data_L = exp(theta_3_L);
    l_square_1_L = exp(theta_4_L);
    l_square_2_L = exp(theta_5_L);
    
    % Inlet Flow Rate Gaussian Process Parameters
    
    sigf_square_1_F0 = exp(theta_1_F0);
    sigf_square_2_F0 = exp(theta_2_F0);
    noise_var_data_F0 = exp(theta_3_F0);
    l_square_1_F0 = exp(theta_4_F0);
    l_square_2_F0 = exp(theta_5_F0);    
    
    % Tank Level Gaussian Process Parameters Used To Construct
    % Matrices
    
    sigf_estimated_1_L = sqrt(exp(theta_1_L));
    sigf_estimated_2_L = sqrt(exp(theta_2_L));
    noise_var_data_estimated_L = exp(theta_3_L);
    l_estimated_1_L = sqrt(exp(theta_4_L));
    l_estimated_2_L = sqrt(exp(theta_5_L));
    
    % Inlet Flow Rate Gaussian Process Parameters Used To Construct
    % Matrices
    
    sigf_estimated_1_F0 = sqrt(exp(theta_1_F0));
    sigf_estimated_2_F0 = sqrt(exp(theta_2_F0));
    noise_var_data_estimated_F0 = exp(theta_3_F0);
    l_estimated_1_F0 = sqrt(exp(theta_4_F0));
    l_estimated_2_F0 = sqrt(exp(theta_5_F0));    
    
    % Evaluate Gaussian Process Objective Function To Establish Whether
    % Convergence Is Reached After Each Iteration
    
    GP_Objective(p + 1,1) = -0.5*(2*sum(log(diag(chol(B_Data))))) ...
                            -0.5*((Output_Data)')/(B_Data)*Output_Data...
                            -0.5*length(Input_t)*log(2*pi);  

    if p >= 2 && abs(abs(GP_Objective(p + 1,1)) - abs(GP_Objective(p,1))) < 0.1
        
        break;
        
    end
     
    end
    
    % Save Optimised Parameter Values For Each Optimisation Routine
    
    Max_GP_Objective(run_repeat,:) = [sigf_estimated_1_L sigf_estimated_2_L noise_var_data_estimated_L l_estimated_1_L l_estimated_2_L ...
        sigf_estimated_1_F0 sigf_estimated_2_F0 noise_var_data_estimated_F0 l_estimated_1_F0 l_estimated_2_F0 GP_Objective(p + 1,1)];
    
%     Visualise If Objective Function Maximum Is Reached
%     figure(run_repeat + 17)
%     plot(0:p,GP_Objective(1:p + 1,1),'k-','LineWidth',2)
%     axis([0 p -Inf Inf])
%     xlabel('p^t^h iteration')
%     ylabel('Log Marginal Likelihood')
    
end

% Extract Maximum Gaussian Process Objective Function Value With Associated Optimised Parameters

[Max_Num,Max_Index] = max(Max_GP_Objective(:,11));

% Tank Liquid Level Gaussian Process Parameters Used To Construct Kernel
% Matrices

sigf_estimated_1_L = Max_GP_Objective(Max_Index,1);
sigf_estimated_2_L = Max_GP_Objective(Max_Index,2);
noise_var_data_estimated_L = Max_GP_Objective(Max_Index,3);
l_estimated_1_L = Max_GP_Objective(Max_Index,4);
l_estimated_2_L = Max_GP_Objective(Max_Index,5);

% Tank Inlet Flow Rate Gaussian Process Parameters Used To Construct Kernel
% Matrices

sigf_estimated_1_F0 = Max_GP_Objective(Max_Index,6);
sigf_estimated_2_F0 = Max_GP_Objective(Max_Index,7);
noise_var_data_estimated_F0 = Max_GP_Objective(Max_Index,8);
l_estimated_1_F0 = Max_GP_Objective(Max_Index,9);
l_estimated_2_F0 = Max_GP_Objective(Max_Index,10);

% End Of Algorithm Execution Time Stopwatch - Optimisation Routine

t_end_optimisation = toc;

% Start Algorithm Execution Stopwatch - ode Regression

tic;

% Construct Gaussian Process To Infer The Underlying State Variable L

Predict_Past = 0;
Predict_Future = 0;

% State Variable L Prediction Range

L_Predict_t = linspace(0 - Predict_Past, Sim_Time + Predict_Future,length(Input_t_L));
F0_Predict_t = linspace(0 - Predict_Past, Sim_Time + Predict_Future,length(Input_t_F0));

% Pre-Allocate Memory For Kernel Matrices For State Variable L

B_L_aa = zeros(length(L_Predict_t),length(L_Predict_t));
B_L_ab = zeros(length(L_Predict_t),length(Input_t_L));
B_L_bb = zeros(length(Input_t_L),length(Input_t_L));
B_F0_bb = zeros(length(Input_t_F0),length(Input_t_F0));

% Populate Kernel Matrices In An Elementwise Manner

j = 1;

for i = 1:1:length(L_Predict_t)

    g = 1;

    for z = 1:1:length(L_Predict_t)

        B_L_aa(j,g) = ((sigf_estimated_1_L)^2)*exp((-1/(2*(((l_estimated_1_L))^2)))*((L_Predict_t(1,j) - L_Predict_t(1,g))^2)) + ...
                    + ((sigf_estimated_2_L)^2)*exp((-1/(2*(((l_estimated_2_L))^2)))*((L_Predict_t(1,j) - L_Predict_t(1,g))^2));

        g = g + 1;

    end

    j = j + 1;

end

% Addition Of "Jitter" For Numerical Stability

B_L_aa = B_L_aa + (1*10^-18)*eye(length(L_Predict_t),length(L_Predict_t));

j = 1;

for i = 1:1:length(L_Predict_t)

    g = 1;

    for z = 1:1:length(Input_t_L)

        B_L_ab(j,g) = ((sigf_estimated_1_L)^2)*exp((-1/(2*(((l_estimated_1_L))^2)))*((L_Predict_t(1,j) - Input_t_L(1,g))^2)) + ...
                    + ((sigf_estimated_2_L)^2)*exp((-1/(2*(((l_estimated_2_L))^2)))*((L_Predict_t(1,j) - Input_t_L(1,g))^2));

        g = g + 1;

    end

    j = j + 1;

end

B_ab_L = [B_L_ab zeros(length(L_Predict_t),length(Input_t_L))];
B_ba_L = (B_ab_L)';

j = 1;

for i = 1:1:length(Input_t_L)

    g = 1;

    for z = 1:1:length(Input_t_L)

        B_L_bb(j,g) = ((sigf_estimated_1_L)^2)*exp((-1/(2*(((l_estimated_1_L))^2)))*((Input_t_L(1,j) - Input_t_L(1,g))^2)) + ...
                    + ((sigf_estimated_2_L)^2)*exp((-1/(2*(((l_estimated_2_L))^2)))*((Input_t_L(1,j) - Input_t_L(1,g))^2));

        g = g + 1;

    end

    j = j + 1;

end

L_bb_Total = B_L_bb + noise_var_data_estimated_L.*eye(length(Input_t_L),length(Input_t_L));

j = 1;

for i = 1:1:length(Input_t_F0)

    g = 1;

    for z = 1:1:length(Input_t_F0)

        B_F0_bb(j,g) = ((sigf_estimated_1_F0)^2)*exp((-1/(2*(((l_estimated_1_F0))^2)))*((Input_t_F0(1,j) - Input_t_F0(1,g))^2)) + ...
                    + ((sigf_estimated_2_F0)^2)*exp((-1/(2*(((l_estimated_2_F0))^2)))*((Input_t_F0(1,j) - Input_t_F0(1,g))^2));

        g = g + 1;

    end

    j = j + 1;

end

F0_bb_Total = B_F0_bb + noise_var_data_estimated_F0.*eye(length(Input_t_F0),length(Input_t_F0));

B_bb_Total_L = [L_bb_Total zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) F0_bb_Total];

% Use Standard Results For Conditioning On A Gaussian Distribution To
% Obtain a Distribution Over L Function Values

mu_L = (B_ab_L)/(B_bb_Total_L)*Output_Data;
covariance_L = B_L_aa - (B_ab_L)/(B_bb_Total_L)*B_ba_L;

% Visualise Inferred L State Variable Function Values - MAP Estimate Used

figure(3 + run_repeat)
S2_L = diag(covariance_L);
f = [mu_L + 2.576*sqrt(S2_L); flip(mu_L - 2.576*sqrt(S2_L),1)];
h = fill([L_Predict_t'; flip(L_Predict_t',1)], f, [6 6 6]/10, 'EdgeColor', [5 5 5]/8);
set(h,'facealpha',1);
hold on
plot(L_Sensor.L_Noise_Free.Time,L_Sensor.L_Noise_Free.Data,'b--','LineWidth',2)
hold on
plot(L_Sensor.L_Noise.Time,L_Sensor.L_Noise.Data,'kx','MarkerSize',8,'LineWidth',2)
%title('Draining Tank Dynamic Response: Random Input in F_0')
xlabel('Time (min)')
ylabel('L (m)')
hold on
plot(L_Predict_t,mu_L,'r','LineWidth',2)
hold on
x_Steady = linspace(0,Sim_Time,Sim_Time/5);
hold on
Steady = plot(x_Steady,L_initial.*ones(length(x_Steady),1),'m.','LineWidth',2,'MarkerSize',10);
legend('99% Credibility Interval','Underlying State Variable Dynamic Response','Liquid Level Sensor Measurements','L (State Variable) - Gaussian Process MAP Estimate','True Underlying Steady State','Location','NorthEast')
set(get(get(Steady,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
legend('Box','Off')
set(gca,'Box','off')
ylim([6.8 7.1])
% x_Steady = linspace(0,Sim_Time,Sim_Time/5);
% hold on
% plot(x_Steady,L_initial.*ones(length(x_Steady),1),'m.','LineWidth',2,'MarkerSize',10)
% hold on
% x_Upper_Limit = linspace(0,Sim_Time,Sim_Time);
% plot(x_Upper_Limit,7.7175.*ones(length(x_Upper_Limit),1),'m-','LineWidth',2) % Upper NOC Threshold Value
% hold on
% x_Lower_Limit = linspace(0,Sim_Time,Sim_Time);
% plot(x_Lower_Limit,6.3175.*ones(length(x_Lower_Limit),1),'m-','LineWidth',2) % Lower NOC Threshold Value
% legend('99% Prediction Interval (Future Observations)','Underlying Tank Liquid Level','Liquid Level Sensor Measurements','L (State Variabel) - MAP Estimate','True Steady State','Upper NOC Limit','Lower NOC Limit','Location','Best')
hold off

% Construct Gaussian Process To Infer Underlying Input Disturbance F0

% Pre-Allocate Memory For Kernel Matrices For Input Disturbance F0

B_F0_aa = zeros(length(F0_Predict_t),length(F0_Predict_t));
B_F0_ab = zeros(length(F0_Predict_t),length(Input_t_F0));

% Populate Kernel Matrices In An Elementwise Manner

j = 1;

for i = 1:1:length(L_Predict_t)

    g = 1;

    for z = 1:1:length(L_Predict_t)

        B_F0_aa(j,g) = ((sigf_estimated_1_F0)^2)*exp((-1/(2*(((l_estimated_1_F0))^2)))*((F0_Predict_t(1,j) - F0_Predict_t(1,g))^2)) + ...
                    + ((sigf_estimated_2_F0)^2)*exp((-1/(2*(((l_estimated_2_F0))^2)))*((F0_Predict_t(1,j) - F0_Predict_t(1,g))^2));

        g = g + 1;

    end

    j = j + 1;

end

% Addition Of "Jitter" For Numerical Stability

B_F0_aa = B_F0_aa + (1*10^-18)*eye(length(L_Predict_t),length(L_Predict_t));

j = 1;

for i = 1:1:length(F0_Predict_t)

    g = 1;

    for z = 1:1:length(Input_t_F0)

        B_F0_ab(j,g) = ((sigf_estimated_1_F0)^2)*exp((-1/(2*(((l_estimated_1_F0))^2)))*((F0_Predict_t(1,j) - Input_t_F0(1,g))^2)) + ...
                    + ((sigf_estimated_2_F0)^2)*exp((-1/(2*(((l_estimated_2_F0))^2)))*((F0_Predict_t(1,j) - Input_t_F0(1,g))^2));

        g = g + 1;

    end

    j = j + 1;

end

B_ab_F0 = [zeros(length(F0_Predict_t),length(Input_t_F0)) B_F0_ab];
B_ba_F0 = (B_ab_F0)';

B_bb_Total_F0 = [L_bb_Total zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) F0_bb_Total];

% Use Standard Results For Conditioning On A Gaussian Distribution To
% Obtain a Distribution Over F0 Function Values

mu_F0 = (B_ab_F0)/(B_bb_Total_F0)*Output_Data;
covariance_F0 = B_F0_aa - (B_ab_F0)/(B_bb_Total_F0)*B_ba_F0;

% Visualise Inferred F0 State Variable Function Values - MAP Estimate Used

figure(4 + run_repeat)
S2_F0 = diag(covariance_F0);
f = [mu_F0 + 2.576*sqrt(S2_F0); flip(mu_F0 - 2.576*sqrt(S2_F0),1)];
h = fill([F0_Predict_t'; flip(F0_Predict_t',1)], f, [6 6 6]/10, 'EdgeColor', [5 5 5]/8);
set(h,'facealpha',1);
hold on
plot(F0_Sensor.F0_Noise_Free.Time,F0_Sensor.F0_Noise_Free.Data,'b--','LineWidth',2)
hold on
plot(F0_Sensor.F0_Noise.Time,F0_Sensor.F0_Noise.Data,'kx','MarkerSize',8,'LineWidth',2)
%title('F_0 Exogenous Input Disturbance')
xlabel('Time (min)')
ylabel('F_0 (m^3/min)')
hold on
plot(F0_Predict_t,mu_F0,'r','LineWidth',2)
hold on
x_Steady = linspace(0,Sim_Time,Sim_Time/5);
Steady = plot(x_Steady,F0_initial.*ones(length(x_Steady),1),'m.','LineWidth',2,'MarkerSize',10);
hold on
x_Upper_Limit = linspace(0,Sim_Time,1000);
Upper = plot(x_Upper_Limit,1.05*F0_initial*ones(length(x_Upper_Limit),1),'m--','LineWidth',2); % Upper NOC Threshold Value
hold on
x_Lower_Limit = linspace(0,Sim_Time,1000);
Lower = plot(x_Lower_Limit,0.95*F0_initial*ones(length(x_Lower_Limit),1),'m--','LineWidth',2); % Lower NOC Threshold Value
axis([0 Sim_Time 1.52 1.76])
legend('99% Confidence Interval','Underlying Exogenous Input Disturbance','Inlet Flow Rate Sensor Measurements','F_0 (Exogenous Input) - Gaussian Process MAP Estimate','True Steady State','Upper NOC Limit','Lower NOC Limit','Location','SouthEast')
set(get(get(Lower,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
set(get(get(Upper,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
set(get(get(Steady,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
legend('Box','Off')
set(gca,'Box','off')
hold off

% Construct Gaussian Process To Infer Underlying State Variable Derivative

% State Variable Derivative d(L)/dt Prediction Range

dL_Predict_t = linspace(0 - Predict_Past, Sim_Time + Predict_Future,length(Input_t_L));

% Pre-Allocate Memory For Kernel Matrices For State Variable Derivative

B_L_Dev_aa = zeros(length(dL_Predict_t),length(dL_Predict_t));
B_L_Dev_ab = zeros(length(dL_Predict_t),length(Input_t_L));

j = 1;

for i = 1:1:length(dL_Predict_t)

    g = 1;

    for z = 1:1:length(dL_Predict_t)        

        B_L_Dev_aa(j,g) = (-sigf_estimated_1_L^2/((l_estimated_1_L^4)))*(((dL_Predict_t(1,g))^2) - (2*(dL_Predict_t(1,j))*(dL_Predict_t(1,g))) + ((dL_Predict_t(1,j))^2) - ((l_estimated_1_L^2)))*exp((-1/(2*(l_estimated_1_L^2)))*((dL_Predict_t(1,j) - dL_Predict_t(1,g))^2)) + ...
                    (-sigf_estimated_2_L^2/((l_estimated_2_L^4)))*(((dL_Predict_t(1,g))^2) - (2*(dL_Predict_t(1,j))*(dL_Predict_t(1,g))) + ((dL_Predict_t(1,j))^2) - ((l_estimated_2_L^2)))*exp((-1/(2*(l_estimated_2_L^2)))*((dL_Predict_t(1,j) - dL_Predict_t(1,g))^2));                       
       
                g = g + 1;

    end

    j = j + 1;

end

% Addition Of "Jitter" For Numerical Stability

B_L_Dev_aa = B_L_Dev_aa + (1*10^-12).*eye(length(dL_Predict_t),length(dL_Predict_t)); 

j = 1;

for i = 1:1:length(dL_Predict_t)

    g = 1;

    for z = 1:1:length(Input_t_L)        

        B_L_Dev_ab(j,g) = (-(sigf_estimated_1_L^2)/((l_estimated_1_L^2)))*((dL_Predict_t(1,j) - Input_t_L(1,g)))*exp((-1/(2*(l_estimated_1_L^2)))*((dL_Predict_t(1,j) - Input_t_L(1,g))^2)) + ...
                    (-(sigf_estimated_2_L^2)/((l_estimated_2_L^2)))*((dL_Predict_t(1,j) - Input_t_L(1,g)))*exp((-1/(2*(l_estimated_2_L^2)))*((dL_Predict_t(1,j) - Input_t_L(1,g))^2));
                       
        g = g + 1;

    end

    j = j + 1;

end

B_ab_L_Dev = [B_L_Dev_ab zeros(length(dL_Predict_t),length(Input_t_F0))];

B_ba_L_Dev = (B_ab_L_Dev)';

B_L_Dev_bb_Total = [L_bb_Total zeros(length(Input_t_L),length(Input_t_L));zeros(length(Input_t_F0),length(Input_t_F0)) F0_bb_Total];

% Use Standard Results For Conditioning On A Gaussian Distribution To
% Obtain a Distribution Over State Variable Derivative Function Values

mu_dL = (B_ab_L_Dev)/(B_L_Dev_bb_Total)*Output_Data;
covariance_dL = B_L_Dev_aa - (B_ab_L_Dev)/(B_L_Dev_bb_Total)*B_ba_L_Dev;

% Visualise Inferred State Variable Derivative Function Values - MAP
% Estimate Used

figure(5 + run_repeat)
S2_dL = diag(covariance_dL);
f = [mu_dL + 2.576*sqrt(S2_dL); flip(mu_dL - 2.576*sqrt(S2_dL),1)];
h = fill([dL_Predict_t'; flip(dL_Predict_t',1)], f, [6 6 6]/10, 'EdgeColor', [5 5 5]/8);
set(h,'facealpha',1);
hold on
plot(L_Derivative_Information.Time,L_Derivative_Information.Data,'b--','LineWidth',2)
plot(dL_Predict_t,mu_dL,'r','LineWidth',2)
%title('Inferred State Derivative')
xlabel('Time (min)')
ylabel('L'' (m/min)')
legend('99% Confidence Interval','Underlying State Derivative (Numerical Integration)','State Derivative - Gaussian MAP Estimate','Location','NorthWest')
legend('Box','Off')
set(gca,'Box','off')

% Prior Hyperparameters

m0 = Parameter_Guess;
S0 = eye(length(m0),length(m0));

% Format The Required Basis Functions

Phi = [mu_F0 -sqrt(mu_L)]; 

% Regression Covariance Matrix

Regress_Covariance = diag(covariance_dL).*eye(length(dL_Predict_t),length(dL_Predict_t));

% Posterior Distribution Results
An = ((inv(S0)) + ((Phi')/(Regress_Covariance)*Phi)); % Precision Matrix
Sn = inv(An); % Posterior Distribution Covariance Matrix
wn = (An)\(((Phi')/(Regress_Covariance)*mu_dL) + (S0\(m0)));% Posterior Distribution Mean Vector

% End Of Algorithm Execution Time Stopwatch - ode Regression

t_end_Bayes = toc;
Bayes_Model_Parameters = wn;

%% Statistical Properties: Gaussian Process ODE Regression
% Algorithm 4

% Ground Truth Comparison - Qaulity Of Parameter Estimates

fprintf('\n\nBayesian Results (based on MAP Estimates)\n\n')
fprintf('\t1/A: %0.4f (1/m^2)\n',Bayes_Model_Parameters(1,1))
fprintf('\tk_v/A: %0.4f (m^0.5/min)\n',Bayes_Model_Parameters(2,1))
fprintf('\tLevel Sensor Noise Variance: %0.9f (m^2)\n',noise_var_data_estimated_L)
fprintf('\tLevel Sensor Noise Standard Deviation: %0.5f (m)\n',sqrt(noise_var_data_estimated_L))
fprintf('\tFlow Sensor Noise Variance: %0.9f (m^6/min^2)\n',noise_var_data_estimated_F0)
fprintf('\tFlow Sensor Noise Standard Deviation: %0.5f (m^3/min)\n\n',sqrt(noise_var_data_estimated_F0))

fprintf('\t1/A %% Error: %0.2f%% \n',((abs(Bayes_Model_Parameters(1,1) - (1/A)))/(1/A))*100)
fprintf('\tk_v/A %% Error: %0.2f%% \n',((abs(Bayes_Model_Parameters(2,1) - (k_v/A)))/(k_v/A))*100)
fprintf('\tLevel Sensor Noise Standard Deviation %% Error: %0.2f%% \n',((abs(sqrt(noise_var_data_estimated_L) - Level_Sensor_SDev))/(Level_Sensor_SDev))*100)
fprintf('\tFlow Sensor Noise Standard Deviation %% Error: %0.2f%% \n',((abs(sqrt(noise_var_data_estimated_F0) - Flow_Sensor_SDev))/(Flow_Sensor_SDev))*100)

% Parameter Marginal Credibility Interval

Cov_Bayes_Parameters =  diag(Sn);

z_score = 2.576; % z_score value - 99% Confidence/Credibility
fprintf('\n\t99%% Marginal Credibility Interval For 1/A: [%0.4f (1/m^2), %0.4f (1/m^2)]',Bayes_Model_Parameters(1,1) - z_score*sqrt(Cov_Bayes_Parameters(1,1)), Bayes_Model_Parameters(1,1) + z_score*sqrt(Cov_Bayes_Parameters(1,1)))
fprintf('\n\t99%% Marginal Credibility Interval For k_v/A: [%0.4f (m^0.5/min), %0.4f (m^0.5/min)]\n',Bayes_Model_Parameters(2,1) - z_score*sqrt(Cov_Bayes_Parameters(2,1)), Bayes_Model_Parameters(2,1) + z_score*sqrt(Cov_Bayes_Parameters(2,1)))

% -> Visualise Parameter 1 Inference Results

figure(6 + run_repeat)

% Visualise Marginal Posterior Distribution over Parameter 1

x = linspace(0.94*Bayes_Model_Parameters(1,1),1.06*Bayes_Model_Parameters(1,1),1000);
plot(x,normpdf(x,Bayes_Model_Parameters(1,1),sqrt(Cov_Bayes_Parameters(1,1))) + 50,'r','LineWidth',2)
set(gca,'YTick',[],'Box','off')
set(gca,'YColor',[1 1 1])
ylim([0 400])
xlim([Bayes_Model_Parameters(1,1) - 4.5*sqrt(Cov_Bayes_Parameters(1,1)) Bayes_Model_Parameters(1,1) + 4.5*sqrt(Cov_Bayes_Parameters(1,1)) ])
hold on

% Visualise 99% Credibility Interval For Parameter 1

x = Bayes_Model_Parameters(1,1);
y = 25;
errhigh = z_score*sqrt(Cov_Bayes_Parameters(1,1));
errlow = z_score*sqrt(Cov_Bayes_Parameters(1,1));
errorbar(x,y,errlow,errhigh,'horizontal','ro','LineWidth',2,'MarkerFaceColor','r','MarkerSize',8)
hold on
plot(1/A,y,'bx','LineWidth',3,'MarkerFaceColor','b','MarkerSize',10)
text(Bayes_Model_Parameters(1,1) - 0.00045,45,'(1/A)_M_A_P','FontSize',12)
text(1/A,70,'(1/A)_T_r_u_e','FontSize',12)
xlabel('1/A (m^-^2)')
set(gca,'YTick',[],'Box','off')
set(gca,'YColor',[1 1 1])
hold off

% -> Visualise Parameter 2 Inference Results

figure(7 + run_repeat)

% Visualise Marginal Posterior Distribution over Parameter 2

x = linspace(0.94*Bayes_Model_Parameters(2,1),1.06*Bayes_Model_Parameters(2,1),1000);
plot(x,normpdf(x,Bayes_Model_Parameters(2,1),sqrt(Cov_Bayes_Parameters(2,1))) + 50,'r','LineWidth',2)
set(gca,'YTick',[],'Box','off')
set(gca,'YColor',[1 1 1])
ylim([0 600])
xlim([Bayes_Model_Parameters(2,1) - 4.5*sqrt(Cov_Bayes_Parameters(2,1)) Bayes_Model_Parameters(2,1) + 4.5*sqrt(Cov_Bayes_Parameters(2,1)) ])
hold on

% Visualise 99% Credibility Interval For Parameter 2

x = Bayes_Model_Parameters(2,1);
y = 25;
errhigh = z_score*sqrt(Cov_Bayes_Parameters(2,1));
errlow = z_score*sqrt(Cov_Bayes_Parameters(2,1));
errorbar(x,y,errlow,errhigh,'horizontal','ro','LineWidth',2,'MarkerFaceColor','r','MarkerSize',8)
hold on
plot(k_v/A,y,'bx','LineWidth',3,'MarkerFaceColor','b','MarkerSize',10)
text(Bayes_Model_Parameters(2,1) - 0.00045,55,'(k_v/A)_M_A_P','FontSize',12)
text(k_v/A,80,'(k_v/A)_T_r_u_e','FontSize',12)
xlabel('k_v/A (m^0^.^5/min)')
set(gca,'YTick',[],'Box','off')
set(gca,'YColor',[1 1 1])
hold off

% Algorithm Execution Time

fprintf('\n\tGaussian Process Hyperparameter Optimisation Time: %0.4f seconds\n',t_end_optimisation)
fprintf('\n\tRegression Time: %0.4f seconds\n',t_end_Bayes)
fprintf('\n\tElapsed Algorithm Time: %0.4f seconds\n',t_end_Bayes + t_end_optimisation)

% Joint Parameter Credibilty Interval
% ->  Ellipsoid Representation

figure(8 + run_repeat)
[EigenVec, EigenVal] = eig(Sn); % Calculate Eigenvalues And Eigenvectors

[Largest_EigenVec_Index, Num] = find(EigenVal == max(max(EigenVal))); % Obtain The Index Of The Largest Eigenvector
Largest_EigenVec = EigenVec(:,Largest_EigenVec_Index);

Largest_EigenVal = max(max(EigenVal)); % Obtain The Largest Eigenvalue

if (Largest_EigenVec_Index == 1) % Obtain The Smallest Eigenvector And EigenValue
    
    Smallest_EigenVal = max(EigenVal(:,2));
    Smallest_Eigen_Vec = EigenVec(:,2);
    
else
    
    Smallest_EigenVal = max(EigenVal(:,1));
    Smallest_Eigen_Vec = EigenVec(:,1);
    
end

Angle = atan2(Largest_EigenVec(2),Largest_EigenVec(1)); % Calculate The Angle Between The Cartesian x-axis And The Largest Eigenvector

if Angle < 0
    
    Angle = Angle + 2*pi; % Shift Angle To Be Between 0 and 2pi
    
end

Average = Bayes_Model_Parameters'; % Extract Mean Inferred Parameter Values

Chi_Value = sqrt(9.210); % Obtain Radius For The Error Ellipse
Theta_Grid = linspace(0,2*pi,10000);
A_Mean = Average(1);
kv_A_Mean = Average(2);

A1 = Chi_Value*sqrt(Largest_EigenVal);
B1 = Chi_Value*sqrt(Smallest_EigenVal); 

Ellipse_X_r = A1*cos(Theta_Grid); % Ellipse Cartesian x-axis Coordinates
Ellipse_Y_r = B1*sin(Theta_Grid); % Ellipse Cartesian y-axis Coordinates

R = [cos(Angle) sin(Angle); -sin(Angle) cos(Angle)]; % Ellipse Rotation Matrix

r_Ellipse = [Ellipse_X_r;Ellipse_Y_r]'*R; % Rotate Ellipse

Bayes_Ellipse = plot(r_Ellipse(:,1) + A_Mean,r_Ellipse(:,2) + kv_A_Mean,'r','LineWidth',2); % Visualise Ellipse
hold on
plot(Bayes_Model_Parameters(1,1),Bayes_Model_Parameters(2,1),'o','Color','r','LineWidth',2,'MarkerSize',8,'MarkerFaceColor','r')
xlabel('1/A (m^-^2)')
ylabel('k_v/A (m^0^.^5/min)')
%title('99% Joint Parameter Confidence/Credibility Region')
set(gca,'Box','off')
hold on

%% Nonlinear Least Squares ODE Regression (Gauss-Newton Implementation)
%  Algorithm 2

% Data Required For Regression

Output_Data = L_Sensor.L_Noise.Data;
Input_t = L_Sensor.L_Noise.Time;

% Start Algorithm Execution Stopwatch

tic;

% NSIG To Establish Convergence

NSIG = 4;

% Pre-Allocate Memory For Least Squares Objective Function

Least_Squares_Objective = zeros(n,1);

% Pre-Allocate Memory For Updated Model Parameters Per Iteration

Frequentist_Model_Parameters = zeros(n,length(Parameter_Guess));

% Pre-Allocate Memory For Numerical Integration Results

L_Time_Range = zeros(length(Output_Data),1);
L_Results = zeros(length(Output_Data),1);
GR1 = zeros(length(Output_Data),1);
GR2 = zeros(length(Output_Data),1);

% Pre-Allocate Memory For Numerical Integration Results - Bisection Rule

L_Time_Range_rho_1 = zeros(length(Output_Data),1);
L_Results_rho_1 = zeros(length(Output_Data),1);
L_Time_Range_rho_halved = zeros(length(Output_Data),1);
L_Results_rho_halved = zeros(length(Output_Data),1);

% Pre-Alloctate Memory For Convergence Evaluation

L_Time_Range_rho = zeros(length(Output_Data),1);
L_Results_rho = zeros(length(Output_Data),1);

for re_regress = 1:1:1 % Can Repeat Regression If Required
    
    % Pre-Allocate Memory For Least Squares Objective Function
    
    Least_Squares_Objective = zeros(n,1);

    % Pre-Allocate Memory For Updated Model Parameters
    
    Frequentist_Model_Parameters = zeros(n,length(Parameter_Guess));

    % Pre-Allocate Memory For Numerical Integration Results
    
    L_Time_Range = zeros(length(Output_Data),1);
    L_Results = zeros(length(Output_Data),1);
    GR1 = zeros(length(Output_Data),1);
    GR2 = zeros(length(Output_Data),1);

    % Pre-Allocate Memory For Numerical Integration Results - Bisection Rule
    
    L_Time_Range_rho_1 = zeros(length(Output_Data),1);
    L_Results_rho_1 = zeros(length(Output_Data),1);
    L_Time_Range_rho_halved = zeros(length(Output_Data),1);
    L_Results_rho_halved = zeros(length(Output_Data),1);

    % Pre-Alloctate Memory For Convergence Evaluation
    
    L_Time_Range_rho = zeros(length(Output_Data),1);
    L_Results_rho = zeros(length(Output_Data),1);

    for j = 1:1:n
        
        Frequentist_Model_Parameters(j,1:length(Parameter_Guess)) = (Parameter_Guess)';

        i = 0; % Initialise Input Disturbance Counter
        Intial_L = [L_initial;0;0]; % Initial Value For "Experimental" Conditions
        
        K = Parameter_Guess.*eye(length(Parameter_Guess),length(Parameter_Guess));

        for tstart = 0:1:Sim_Time

            i = i + 1; % Increment Input Disturbance Counter
            tspan = [tstart tstart + 0.5 tstart + 1]; % Numerical Integration Time Span

            % Set Up Matrix Differential Equation
            
            Matrix_Diff_Equation = @(t,y) ...
                [Parameter_Guess(1,1)*F0_Sensor.F0_Noise.Data(i,1) - Parameter_Guess(2,1)*sqrt(y(1)); ... % L = y(1)
                -(Parameter_Guess(2,1)/(2*sqrt(y(1))))*y(2) + F0_Sensor.F0_Noise.Data(i,1)*K(1,1); ... % GR1 = y(2)
                -(Parameter_Guess(2,1)/(2*sqrt(y(1))))*y(3) - sqrt(y(1))*K(2,2)]; % GR2 = y(3)

            % Use ode45 To Numerically Integrate Matrix Differential Equation
            
            [t_temp, y_temp] = ode45(Matrix_Diff_Equation,tspan,Intial_L);
            L_Time_Range(tstart + 1,1) = t_temp(1,1);
            Intial_L = [y_temp(3,1) y_temp(3,2) y_temp(3,3)];
            L_Results(tstart + 1,1) = y_temp(1,1);
            GR1(tstart + 1,1) = y_temp(1,2);
            GR2(tstart + 1,1) = y_temp(1,3);       

        end

        % Compute Integrated Model Values
        
        t_sampling_period = L_Time_Range(L_Sensor.L_Noise.Time + 1,1);
        D = 1; % Sensitivity Matrix
        L_Model = D*L_Results(t_sampling_period + 1,1); % Extract ode Model Predicted Results

        % Evaluate Least Squares Objective Function
        
        Least_Squares_Objective(j,1) = sum(((Output_Data) - L_Model).^2);

        % Set Up Sensitivity Matrices - Jacobian (Vectorised)
        
        J = [GR1 GR2]; % <- From ode45 Numerical Integration

        % Set Up Linear Equations To Minimise The Simple Least Squares Error Function
        % -> Use The Reduced Sensitivity Matrix Gr 
       
        A_R = ((J)')*(D')*D*J;
        b_R = ((J)')*(D')*(Output_Data - L_Model);

        % Step Update
        
        delta_k_R = A_R\b_R; % Solve System Of Linear Equations

        % Bisection Rule For Determining The Stepping Parameter rho
        % -> Performing a One Dimensional Line Search

        % Set Up Matrix Differential Equation For rho = 1
        
        k_rho_1 = 1*K*delta_k_R;

        i = 0; % Initialise Input Disturbance Counter
        Intial_L = [L_initial;0;0]; % Initial Value For "Experimental" Conditions

        for tstart = 0:1:Sim_Time

            i = i + 1; % Increment Input Disturbance Counter
            tspan = [tstart tstart + 0.5 tstart + 1]; % Numerical Integration Time Span

            % Set Up Matrix Differential Equation For rho = 1
            
            Matrix_Diff_Equation_rho_1 = @(t,y) ...            
                 [(Parameter_Guess(1,1) + k_rho_1(1,1))*F0_Sensor.F0_Noise.Data(i,1) - (Parameter_Guess(2,1) + k_rho_1(2,1))*sqrt(y(1)); ... % L = y(1)
                -((Parameter_Guess(2,1) + k_rho_1(2,1))/(2*sqrt(y(1))))*y(2) + F0_Sensor.F0_Noise.Data(i,1)*K(1,1); ... % GR1 = y(2)
                -((Parameter_Guess(2,1) + k_rho_1(2,1))/(2*sqrt(y(1))))*y(3) - sqrt(y(1))*K(2,2)]; % GR2 = y(3)

            % Use ode45 To Numerically Integrate Matrix Differential Equation
            
            [t_temp_rho_1, y_temp_rho_1] = ode45(Matrix_Diff_Equation_rho_1,tspan,Intial_L);
            L_Time_Range_rho_1(tstart + 1,1) = t_temp_rho_1(1,1);
            Intial_L = [y_temp_rho_1(3,1) y_temp_rho_1(3,2) y_temp_rho_1(3,3)];
            L_Results_rho_1(tstart + 1,1) = y_temp_rho_1(1,1);

        end

        % Compute Integrated Model Values
        
        t_sampling_period_rho_1 = L_Time_Range_rho_1(L_Sensor.L_Noise.Time + 1,1);
        D = 1; % Sensitivity Matrix
        L_Model_rho_1 = D*L_Results_rho_1(t_sampling_period_rho_1 + 1,1); % Extract ode Model Predicted Results

        if sum(((Output_Data) - L_Model_rho_1).^2) < Least_Squares_Objective(j,1)

            rho = 1; % Stepping Parameter Size

        else

            rho = 1; % Stepping Parameter Size

            for u = 1:1:max_trials

                rho = 0.5*rho; % Keep Halving Stepping Parameter Size

                % Set Up Matrix Differential Equation For New rho
                
                k_rho_halved = rho*K*delta_k_R;

                i = 0; % Initialise Input Disturbance Counter
                Intial_L = [L_initial;0;0]; % Initial Value For "Experimental" Conditions

                for tstart = 0:1:Sim_Time

                    i = i + 1; % Increment Input Disturbance Counter                
                    tspan = [tstart tstart + 0.5 tstart + 1]; % Numerical Integration Time Span

                    % Set Up Matrix Differential Equation
                    
                    Matrix_Diff_Equation_rho_halved = @(t,y) ...                    
                        [(Parameter_Guess(1,1) + k_rho_halved(1,1))*F0_Sensor.F0_Noise.Data(i,1) - (Parameter_Guess(2,1) + k_rho_halved(2,1))*sqrt(y(1)); ... % L = y(1)
                        -((Parameter_Guess(2,1) + k_rho_halved(2,1))/(2*sqrt(y(1))))*y(2) + F0_Sensor.F0_Noise.Data(i,1)*K(1,1); ... % GR1 = y(2)
                        -((Parameter_Guess(2,1) + k_rho_halved(2,1))/(2*sqrt(y(1))))*y(3) - sqrt(y(1))*K(2,2)]; % GR2 = y(3)

                    % Use ode45 To Numerically Integrate Matrix Differential Equation
                    
                    [t_temp_rho_halved, y_temp_rho_halved] = ode45(Matrix_Diff_Equation_rho_halved,tspan,Intial_L);
                    L_Time_Range_rho_halved(tstart + 1,1) = t_temp_rho_halved(1,1);
                    Intial_L = [y_temp_rho_halved(3,1) y_temp_rho_halved(3,2) y_temp_rho_halved(3,3)];
                    L_Results_rho_halved(tstart + 1,1) = y_temp_rho_halved(1,1);

                end

                % Compute Integrated Model Values
                
                t_sampling_period_rho_halved = L_Time_Range_rho_halved(L_Sensor.L_Noise.Time + 1,1);
                D = 1; % Sensitivity Matrix
                L_Model_rho_halved = D*L_Results_rho_halved(t_sampling_period_rho_halved + 1,1); % Extract ode Model Predicted Results

                if sum(((Output_Data) - L_Model_rho_halved).^2) < Least_Squares_Objective(j,1)

                    break;

                end           

            end

        end

        Parameter_Guess = Parameter_Guess + rho*K*delta_k_R;
        fprintf('\nParameter Updates Per Iteration: [%0.4f,%0.4f]',Parameter_Guess(1,1),Parameter_Guess(2,1))

        if (1/length(Parameter_Guess))*sum(abs(K*delta_k_R./Parameter_Guess)) <= 10^(-NSIG) % Break Iteration If Convergence Is Achieved

            Frequentist_Model_Parameters(j + 1,1:length(Parameter_Guess)) = (Parameter_Guess)';

            i = 0; % Initialise Input Disturbance Counter
            Intial_L = [L_initial;0;0]; % Initial Value For "Experimental" Conditions    

            for tstart = 0:1:Sim_Time

                i = i + 1; % Increment Input Disturbance Counter
                tspan = [tstart tstart + 0.5 tstart + 1]; % Numerical Integration Time Span

                % Set Up Matrix Differential Equation
                
                Matrix_Diff_Equation = @(t,y) ...
                    [Parameter_Guess(1,1)*F0_Sensor.F0_Noise.Data(i,1) - Parameter_Guess(2,1)*sqrt(y(1)); ... % L = y(1)
                    -(Parameter_Guess(2,1)/(2*sqrt(y(1))))*y(2) + F0_Sensor.F0_Noise.Data(i,1)*K(1,1); ... % GR1 = y(2)
                    -(Parameter_Guess(2,1)/(2*sqrt(y(1))))*y(3) - sqrt(y(1))*K(2,2)]; % GR2 = y(3)

                % Use ode45 To Numerically Integrate Matrix Differential Equation
                
                [t_temp, y_temp] = ode45(Matrix_Diff_Equation,tspan,Intial_L);
                L_Time_Range(tstart + 1,1) = t_temp(1,1);
                Intial_L = [y_temp(3,1) y_temp(3,2) y_temp(3,3)];
                L_Results(tstart + 1,1) = y_temp(1,1);
                GR1(tstart + 1,1) = y_temp(1,2);
                GR2(tstart + 1,1) = y_temp(1,3);    

            end

            % Compute Integrated Model Values
            
            t_sampling_period = L_Time_Range(L_Sensor.L_Noise.Time + 1,1);
            D = 1; % Sensitivity Matrix
            L_Model = D*L_Results(t_sampling_period + 1,1); % Extract ode Model Predicted Results

            % Evaluate Least Squares Objective Function
            
            Least_Squares_Objective(j + 1,1) = sum(((Output_Data) - L_Model).^2);
            Cov_A = (Least_Squares_Objective(j + 1,1)/(N - length(Parameter_Guess))).*inv((([(1/K(1,1)).*GR1 (1/K(2,2)).*GR2])')*[(1/K(1,1)).*GR1 (1/K(2,2)).*GR2]);

            break;

        end
               
    end
    
   % Parameter_Guess = Frequentist_Model_Parameters(j + 1,1:length(Parameter_Guess))';
       
end

% End Of Algorithm Execution Time Stopwatch

t_end_Frequentist = toc;

Frequentist_Model_Parameters = (Frequentist_Model_Parameters(j + 1,1:length(Parameter_Guess)))';

% Visualise If Least Squares Objective Function Has Converged To a Minimum

figure(9 + run_repeat)
plot(1:j+1,Least_Squares_Objective(1:j+1),'kx--','LineWidth',2,'MarkerSize',12)
title('Least Squares Objective Function')
xlabel('i^t^h iteration')
ylabel('LS(w)')
set(gca,'XTick',1:j+1)
xlim([1 j+1])
set(gca,'Box','off')

%% Statistical Properties: Nonlinear Least Squares ODE Regression (Gauss-Newton Implementation)
% Algorithm (2)

% Ground Truth Comparison - Quality of Parameter Estimates

fprintf('\n\nFrequentist Results (based on Maximum Likelihood Estimates)\n\n')
fprintf('\t1/A: %0.4f (1/m^2)\n',Frequentist_Model_Parameters(1,1))
fprintf('\tk_v/A: %0.4f (m^0.5/min)\n',Frequentist_Model_Parameters(2,1))
fprintf('\tLevel Noise Variance: %0.9f (m^2)\n',(Least_Squares_Objective(j + 1,1)/(N - length(Parameter_Guess))))
fprintf('\tLevel Sensor Noise Standard Deviation: %0.5f (m)\n\n',sqrt((Least_Squares_Objective(j + 1,1)/(N - length(Parameter_Guess)))))

fprintf('\t1/A: %% Error: %0.2f%% \n',((abs(Frequentist_Model_Parameters(1,1) - (1/A)))/(1/A))*100)
fprintf('\tk_v/A: %% Error: %0.2f%% \n',((abs(Frequentist_Model_Parameters(2,1) - (k_v/A)))/(k_v/A))*100)
fprintf('\tLevel Sensor Noise Standard Deviation %% Error: %0.2f%% \n',((abs((sqrt((Least_Squares_Objective(j + 1,1)/(N - length(Parameter_Guess))))) - Level_Sensor_SDev))/(Level_Sensor_SDev))*100)

% Parameter Marginal Confidence Interval

Cov_Frequentist_Parameters =  diag(Cov_A);

fprintf('\n\t99%% Marginal Confidence Interval For 1/A: [%0.4f (1/m^2), %0.4f (1/m^2)]',Frequentist_Model_Parameters(1,1) - z_score*sqrt(Cov_Frequentist_Parameters(1,1)), Frequentist_Model_Parameters(1,1) + z_score*sqrt(Cov_Frequentist_Parameters(1,1)))
fprintf('\n\t99%% Marginal Confidence Interval For k_v/A: [%0.4f (m^0.5/min), %0.4f (m^0.5/min)]\n',Frequentist_Model_Parameters(2,1) - z_score*sqrt(Cov_Frequentist_Parameters(2,1)), Frequentist_Model_Parameters(2,1) + z_score*sqrt(Cov_Frequentist_Parameters(2,1)))

% -> Visualise 99% Marginal Confidence Interval For Parameter 1

figure(10 + run_repeat)

x = Frequentist_Model_Parameters(1,1);
y = 35;
errhigh = z_score*sqrt(Cov_Frequentist_Parameters(1,1));
errlow = z_score*sqrt(Cov_Frequentist_Parameters(1,1));
errorbar(x,y,errlow,errhigh,'horizontal','o','Color',[0.4660 0.6740 0.1880],'LineWidth',2,'MarkerFaceColor',[0.4660 0.6740 0.1880],'MarkerSize',8)
hold on
plot(1/A,y,'bx','LineWidth',3,'MarkerFaceColor','b','MarkerSize',10)
text(Frequentist_Model_Parameters(1,1) - 0.0004,35.15,'(1/A)_M_L','FontSize',12)
text(1/A - 0.0005,35.15,'(1/A)_T_r_u_e','FontSize',12)
xlabel('1/A (m^-^2)')
set(gca,'YTick',[],'Box','off')
set(gca,'YColor',[1 1 1])
hold off

% Visualise 99% Marginal Confidence Interval For Parameter 2

figure(11 + run_repeat)

x = Frequentist_Model_Parameters(2,1);
y = 35;
errhigh = z_score*sqrt(Cov_Frequentist_Parameters(2,1));
errlow = z_score*sqrt(Cov_Frequentist_Parameters(2,1));
errorbar(x,y,errlow,errhigh,'horizontal','o','Color',[0.4660 0.6740 0.1880],'LineWidth',2,'MarkerFaceColor',[0.4660 0.6740 0.1880],'MarkerSize',8)
hold on
plot(k_v/A,y,'bx','LineWidth',3,'MarkerFaceColor','b','MarkerSize',10)
text(Frequentist_Model_Parameters(2,1) - 0.00075,35.15,'(k_v/A)_M_L','FontSize',12)
text(k_v/A - 0.0005,35.15,'(k_v/A)_T_r_u_e','FontSize',12)
xlabel('k_v/A (m^0^.^5/min)')
set(gca,'YTick',[],'Box','off')
set(gca,'YColor',[1 1 1])
hold off

% Algorithm Execution Time

fprintf('\n\tElapsed Algorithm Time: %0.4f seconds',t_end_Frequentist)

% Joint Parameter Confidence Interval
% ->  Ellipsoid Representation

figure(8 + run_repeat)
F_dist = 4.80;
p_length = length(Frequentist_Model_Parameters);

[EigenVec, EigenVal] = eig(Cov_A); % Calculate Eigenvalues And Eigenvectors

[Largest_EigenVec_Index, num] = find(EigenVal == max(max(EigenVal))); % Obtain The Index Of The Largest Eigenvector
Largest_EigenVec = EigenVec(:,Largest_EigenVec_Index);

Largest_EigenVal = max(max(EigenVal)); % Obtain The Largest Eigenvalue

if (Largest_EigenVec_Index == 1) % Obtain The Smallest Eigenvector And EigenValue
    
    Smallest_EigenVal = max(EigenVal(:,2));
    Smallest_EigenVec = EigenVec(:,2);
    
else
    
    Smallest_EigenVal = max(EigenVal(:,1));
    Smallest_EigenVec = EigenVec(:,1);
    
end

Angle = atan2(Largest_EigenVec(2),Largest_EigenVec(1)); % Calculate The Angle Between The Cartesian x-axis And The Largest Eigenvector

if Angle < 0
    
    Angle = Angle + 2*pi; % Shift Angle To Be Between 0 and 2pi
    
end

Average = Frequentist_Model_Parameters'; % Extract Mean Inferred Parameter Values

Chi_Value = sqrt(p_length*F_dist); % Obtain Radius For The Error Ellipse
Theta_Grid = linspace(0,2*pi,10000);
A_Mean = Average(1);
kv_A_Mean = Average(2);

A1 = Chi_Value*sqrt(Largest_EigenVal);
B1 = Chi_Value*sqrt(Smallest_EigenVal);

Ellipse_X_r = A1*cos(Theta_Grid); % Ellipse Cartesian x-axis Coordinates
Ellipse_Y_r = B1*sin(Theta_Grid); % Ellipse Cartesian y-axis Coordinates

R = [cos(Angle) sin(Angle); -sin(Angle) cos(Angle)]; % Ellipse Rotation Matrix

r_Ellipse = [Ellipse_X_r;Ellipse_Y_r]'*R; % Rotate Ellipse

Freq_Ellipse = plot(r_Ellipse(:,1) + A_Mean,r_Ellipse(:,2) + kv_A_Mean,'Color',[0.4660, 0.6740, 0.1880],'LineWidth',2); % Visualise Ellipse
hold on
plot(Frequentist_Model_Parameters(1,1),Frequentist_Model_Parameters(2,1),'o','Color',[0.4660, 0.6740, 0.1880],'LineWidth',2,'MarkerSize',8,'MarkerFaceColor',[0.4660, 0.6740, 0.1880])
hold on
plot(1/A,k_v/A,'bx','LineWidth',3,'MarkerFaceColor','b','MarkerSize',10)
legend('Bayesian 99% Epplise','[(1/A)_M_A_P  (k_v/A)_M_A_P]^T','Frequentist 99% Ellispe','[(1/A)_M_L  (k_v/A)_M_L]^T','[(1/A)_T_r_u_e  (k_v/A)_T_r_u_e]^T','Location','SouthEast')
xlabel('1/A (m^-^2)')
ylabel('k_v/A (m^0^.^5/min)')
%title('99% Joint Parameter Confidence/Credibility Region')
legend('boxoff') 
set(get(get(Bayes_Ellipse,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
set(get(get(Freq_Ellipse,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
hold off

% Visualise The Expected Mean Response

figure(12 + run_repeat)

% -> Evaluate Covariance

Cov_y = ((Least_Squares_Objective(j + 1,1)/(N - length(Parameter_Guess))))*D*J/((J')*J)*D*(J');

Mean_Response_Range = linspace(0,Sim_Time,length(dL_Predict_t))';
S2 = diag(Cov_y);
plot(Mean_Response_Range,L_Model + z_score*sqrt(S2),'o','Color',[0 0 0],'MarkerSize',1.5,'MarkerFaceColor',[0 0 0])
hold on
Lower_Limit = plot(Mean_Response_Range,L_Model - z_score*sqrt(S2),'o','Color',[0 0 0],'MarkerSize',1.5,'MarkerFaceColor',[0 0 0]);
hold on
plot(L_Sensor.L_Noise_Free.Time,L_Sensor.L_Noise_Free.Data,'b--','LineWidth',2.5)
hold on
plot(L_Sensor.L_Noise.Time,L_Sensor.L_Noise.Data,'kx','MarkerSize',8,'LineWidth',2)
%title('Draining Tank Dynamic Response: Random Input in F_0')
xlabel('Time (min)')
ylabel('L (m)')
hold on
plot(Mean_Response_Range,L_Model,'Color',[0.4660, 0.6740, 0.1880],'LineWidth',2)
x_Steady = linspace(0,Sim_Time,Sim_Time/5);
hold on
Steady = plot(x_Steady,L_initial.*ones(length(x_Steady),1),'m.','LineWidth',2,'MarkerSize',10);
legend('99% Upper/Lower Confidence Limit','99% Lower EMR Limit','Underlying State Variable Dynamic Response','Liquid Level Sensor Measurements','State Variable Dynamic Response (ML Estimate)','True Underlying Steady State','Location','NorthEast')
set(get(get(Lower_Limit,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
set(get(get(Steady,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
legend('Box','Off')
set(gca,'Box','off')
ylim([6.8 7.1])
hold off