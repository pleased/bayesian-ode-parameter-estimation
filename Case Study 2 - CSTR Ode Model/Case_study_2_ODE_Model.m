%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

         %%%% Ordinary Differential Equation Regression %%%%     
            %%%% -> Linear In The ODE Model Parameters %%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%% Case Study 2: Isothermal, Constant Volume CSTR (Example 3.2)

%%%% Reference: Marlin, T E. 2000. Process Control: Designing Processes and
%%%% Control Systems for Dynamic Performance. 2nd ed. Boston: McGraw-Hill.

%% Clear Memory And Command Window

close all; 
clear; 
clc;

%% Isothermal, Constant Volume CSTR Model

% -> Derived From First Principles (Conservation of Mass)
% -> No Input Disturbance Structure Assumed
% -> Input Disturbance In CA0 Modeled With a Draw From a Gaussian Process
%    Between a Normal Operating Conditions Bandwidth
% -> Reaction Kinetics: First Order
% -> ODE Model is Linear In The Model State Variable CA
% -> ODE Model is Linear In The Model Parameters

% Dynamic Model Simulation Information

V = 2.1; % m^3 - Reactor Volume
F = 0.085; % m^3/min - Reactor Inlet/Outlet Flowrate
CA0_initial = 0.925; % - mole/m^3 Initial Reactor Inlet Concentration
delta_CA0 = 0.0925; % - mole/m^3 Disturbance Step Change Magnitude in Reactor Inlet Concentration
k_reaction = 0.040; % - min^-1 Reaction Rate Constant
CA_initial = (F/(F + (k_reaction*V)))*CA0_initial; % - Initial Reactor Outlet Concentration

% Simulation Run Time

Sim_Time = 120; % Minutes

% Simulation Ground Truth Parameter Values

fprintf('\nTrue Parameter Values (Set During Simulation)\n\n')
fprintf('\tF/V: %0.4f (1/min)\n',F/V)
fprintf('\tk: %0.4f (1/min)\n',k_reaction)

%% Load CA0 Input Disturbance Data
%  Input Disturbance Generated Independently From A Draw From A Gaussian
%  Process

load('Case_Study_2_Input_Disturbance.mat')

% Read MATLAB Data To Simulink

RandomCA0.time = Case_Study_2_Input_Disturbance(:,1);
RandomCA0.signals.values = Case_Study_2_Input_Disturbance(:,2);
RandomCA0.signals.dimensions = 1;

%% Data Generation Process - Numerical Integration Performed In Simulink

% Sensor Measurement Sampling Time

SampleTime = 1; % Minutes

% Sensor Measurement Details

% Flow Sensor - Data Not Used In Any Regression Techniques

rng(1,'Twister') % Ensure Repeatability
Flow_Sensor_Seed = randi(1*10^9,1,1);
Flow_Sensor_Var = 1.5*10^-8;
Flow_Sensor_SDev = sqrt(Flow_Sensor_Var);

% Inlet Concentration Sensor

rng(2,'Twister') % Ensure Repeatability
CA0_Sensor_Seed = randi(1*10^9,1,1);
CA0_Sensor_Var = 7*10^-6;
CA0_Sensor_SDev = sqrt(CA0_Sensor_Var);

fprintf('\tCA0 Noise Standard Deviation: %0.5f (mole/m^3)\n',CA0_Sensor_SDev)

% Outlet Concentration Sensor

rng(3,'Twister') % Ensure Repeatability
CA_Sensor_Seed = randi(1*10^9,1,1);
CA_Sensor_Var = 2*10^-6;
CA_Sensor_SDev = sqrt(CA_Sensor_Var);

fprintf('\tCA Noise Standard Deviation: %0.5f (mole/m^3)\n',CA_Sensor_SDev)

% -> Run Simulink Model From MATLAB

open('Case_Study_2_Linear_ODE_Regression.slx')
sim('Case_Study_2_Linear_ODE_Regression.slx')
close_system('Case_Study_2_Linear_ODE_Regression.slx')

% -> Visualise Simulink Data Generation Results

% Inlet Concentration Sensor Measurements

figure(1)
plot(CA0_Sensor.CA0_Noise_Free.Time,CA0_Sensor.CA0_Noise_Free.Data,'b--','LineWidth',2)
hold on
plot(CA0_Sensor.CA0_Noise.Time,CA0_Sensor.CA0_Noise.Data,'kx','MarkerSize',10,'LineWidth',2)
%title('C_A_0 Exogenous Input Disturbance')
xlabel('Time (min)')
ylabel('C_A_0 (mole/m^3)')
hold on
x_Steady = linspace(0,Sim_Time,Sim_Time/5);
plot(x_Steady,CA0_initial.*ones(length(x_Steady),1),'m.','LineWidth',2,'MarkerSize',10)
hold on
x_Upper_Limit = linspace(0,Sim_Time,1000);
plot(x_Upper_Limit,1.05*CA0_initial*ones(length(x_Upper_Limit),1),'m--','LineWidth',2) % Upper NOC Threshold Value
hold on
x_Lower_Limit = linspace(0,Sim_Time,1000);
Lower = plot(x_Lower_Limit,0.95*CA0_initial*ones(length(x_Upper_Limit),1),'m--','LineWidth',2); % Lower NOC Threshold Value
legend('Underlying Exogenous Input Disturbance','Inlet Concentration Sensor Measurements','True Underlying Steady State','Upper/Lower NOC Limit','Lower NOC Limit','Location','SouthEast')
legend('boxoff')
set(get(get(Lower,'Annotation'),'LegendInformation'),'IconDisplayStyle','off')
axis([0 Sim_Time 0.835 1.005])
set(gca,'Box','off')
hold off

% Outlet Concentration Sensor Measurements

figure(2)
plot(CA_Sensor.CA_Noise_Free.Time,CA_Sensor.CA_Noise_Free.Data,'b--','LineWidth',2)
hold on
plot(CA_Sensor.CA_Noise.Time,CA_Sensor.CA_Noise.Data,'kx','MarkerSize',10,'LineWidth',2)
hold on
x_steady_state = linspace(0,Sim_Time,Sim_Time/5);
plot(x_steady_state,CA_initial.*ones(length(x_steady_state),1),'m.','LineWidth',2,'MarkerSize',10) 
hold on
x_Upper_Limit = linspace(0,Sim_Time,Sim_Time);
plot(x_Upper_Limit,0.4885.*ones(length(x_Upper_Limit),1),'m--','LineWidth',2) % Upper NOC Threshold Value
hold on
x_Lower_Limit = linspace(0,Sim_Time,Sim_Time);
Lower = plot(x_Lower_Limit,0.4420.*ones(length(x_Lower_Limit),1),'m--','LineWidth',2); % Lower NOC Threshold Value
%title('CSTR Dynamic Reponse: Random Input in C_A_0')
xlabel('Time (min)')
ylabel('C_A (mole/m^3)')
legend('Underlying State Variable Dynamic Response','Outlet Concentration Sensor Measurements','True Underlying Steady State','Upper/Lower NOC Limit','Lower NOC Limit','Location','SouthEast')
legend('boxoff')
set(get(get(Lower,'Annotation'),'LegendInformation'),'IconDisplayStyle','off')
axis([0 Sim_Time 0.420 0.50])
set(gca,'Box','off')
hold off

% Number Of Concentration Sensor Measurements Used

N = length(CA_Sensor.CA_Noise.Data);

%% Initial Model Parameter Guess And Number Of Algorithm Iterations 

% -> Intial Guess Condition Used For:
    % (1) Gaussian Process ODE Regression (Algorithm 4)
    % (2) Nonlinear Least Squares ODE Regression (Gauss-Newton 
    % Implementation) (Algorithm 2)
    
rng('Shuffle','Twister')
Parameter_1 = ((1/10)*(F/V)) + (((10)*(F/V)) - ((1/10)*(F/V))).*rand(1,1);
rng('Shuffle','Twister')
Parameter_2 = ((1/10)*(k_reaction)) + (((10)*(k_reaction)) - ((1/10)*(k_reaction))).*rand(1,1);

% Populate Vector Containing Initial Model Parameter Guesses

Parameter_Guess = [Parameter_1;Parameter_2];

fprintf('\nInitial Parameter Guess [%0.4f,%0.4f]\n',Parameter_1,Parameter_2)

% -> Number Of Trial Iterations:
    % (2) Nonlinear Least Squares ODE Regression (Gauss-Newton 
    % Implementation) (Algorithm 2)
    
n = 2000;

% -> Empirical Iterations/Trials
    % (2) Nonlinear Least Squares ODE Regression (Gauss-Newton 
    % Implementation) (Algorithm 2)
    
max_trials = 500;

%% Gaussian Process ODE Regression
%  (Algorithm 4)

% Start Algorithm Execution Stopwatch - Optimisation Routine

tic;

% -> Optimise Gaussian Process Kernel Hyperparameters Via Gradient Ascent

% Outlet Concentration Data

Output_Data_CA = CA_Sensor.CA_Noise.Data;
Input_t_CA = CA_Sensor.CA_Noise.Time';

% Inlet Disturbance CA0 Data
 
Output_Data_CA0 = CA0_Sensor.CA0_Noise.Data;  
Input_t_CA0 = CA0_Sensor.CA0_Noise.Time';

% Collect Data In a Vector For Joint Optimisation

Output_Data = [Output_Data_CA;Output_Data_CA0];
Input_t = [Input_t_CA Input_t_CA0];

% Pre-allocate Memory For 10 Optimisation Routines
% - > 10 Unknown Gaussian Process Parameters + Log Marginal Likelihood Function
% Value

Max_GP_Objective = zeros(10,11);

% Number Of Gradient Ascent Iterations

GA_Iterations = 3000;

% Stepping Parameter Size

Gamma = 0.01;

for run_repeat = 1:1:10
    
    fprintf('\nGaussian Process Hyperparameter Optimisation: %0.0f',run_repeat)
    
    % Randomly Initialise Each Log Unknown Gaussian Process Parameters
    
    % Outlet Concentration Gaussian Process Parameters
    % Constrained To Be Positive
    
    rng('Shuffle','Twister')
    theta_1_CA = -0.96 + 0.2.*randn(1);
    rng('Shuffle','Twister')
    theta_2_CA = -9.0 + 0.2.*randn(1);
    rng('Shuffle','Twister')
    theta_3_CA = -13 + 0.1.*randn(1);
    rng('Shuffle','Twister')
    theta_4_CA = 15.2 + 0.2.*randn(1);
    rng('Shuffle','Twister')
    theta_5_CA = 5.90 + 0.2.*randn(1);
    
    % Inlet Disturbance CA0 Gaussian Process Parameters
    % Constrained To Be Positive
    
    rng('Shuffle','Twister')
    theta_1_CA0 = -0.02 + 0.2.*randn(1);
    rng('Shuffle','Twister')
    theta_2_CA0 = -8.53 + 0.2.*randn(1);
    rng('Shuffle','Twister')
    theta_3_CA0 = -12 + 0.1.*randn(1);
    rng('Shuffle','Twister')
    theta_4_CA0 = 15.3 + 0.2.*randn(1);
    rng('Shuffle','Twister')
    theta_5_CA0 = 4.26 + 0.2.*randn(1);
        
    % Gaussian Process Parameters
    
    % Outlet Concentration Gaussian Process Parameters
    
    sigf_square_1_CA = exp(theta_1_CA);
    sigf_square_2_CA = exp(theta_2_CA);
    noise_var_data_CA = exp(theta_3_CA);
    sqrt(noise_var_data_CA);
    l_square_1_CA = exp(theta_4_CA);
    l_square_2_CA = exp(theta_5_CA);
    
    % Inlet Disturbance CA0 Gaussian Process Parameters
    
    sigf_square_1_CA0 = exp(theta_1_CA0);
    sigf_square_2_CA0 = exp(theta_2_CA0);
    noise_var_data_CA0 = exp(theta_3_CA0);
    sqrt(noise_var_data_CA0);
    l_square_1_CA0 = exp(theta_4_CA0);
    l_square_2_CA0 = exp(theta_5_CA0);
    
    % Pre-allocate Memory For Kernel Matrices
    
    % Outlet Concentration Gaussian Process Matrices
    
    B_Data_CA = zeros(length(Input_t_CA),length(Input_t_CA));
    B_theta_1_CA = zeros(length(Input_t_CA),length(Input_t_CA));
    B_theta_2_CA = zeros(length(Input_t_CA),length(Input_t_CA));
    B_theta_3_CA = zeros(length(Input_t_CA),length(Input_t_CA));
    B_theta_4_CA = zeros(length(Input_t_CA),length(Input_t_CA));
    B_theta_5_CA = zeros(length(Input_t_CA),length(Input_t_CA));
    
    % Inlet Disturbance CA0 Gaussian Process Matrices
    
    B_Data_CA0 = zeros(length(Input_t_CA0),length(Input_t_CA0));
    B_theta_1_CA0 = zeros(length(Input_t_CA0),length(Input_t_CA0));
    B_theta_2_CA0 = zeros(length(Input_t_CA0),length(Input_t_CA0));
    B_theta_3_CA0 = zeros(length(Input_t_CA0),length(Input_t_CA0));
    B_theta_4_CA0 = zeros(length(Input_t_CA0),length(Input_t_CA0));
    B_theta_5_CA0 = zeros(length(Input_t_CA0),length(Input_t_CA0));

    % Pre-allocate Memory For Gaussian Process Objective Function
    
    GP_Objective = zeros(GA_Iterations,1);
    
    % Construct Kernel Matrix With Data For The Outlet Concentration
    
    j = 1;
    
    for i = 1:1:length(Input_t_CA)
        
        g = 1;
        
        for z = 1:1:length(Input_t_CA)
            
            B_Data_CA(j,g) = sigf_square_1_CA*exp((-1/(2*(l_square_1_CA)))*((Input_t_CA(1,j) - Input_t_CA(1,g)))^2) + ...
                        + sigf_square_2_CA*exp((-1/(2*(l_square_2_CA)))*((Input_t_CA(1,j) - Input_t_CA(1,g)))^2);
            
            g = g + 1;
            
        end
        
        j = j + 1;
        
    end
    
    B_Data_CA = B_Data_CA + noise_var_data_CA.*eye(length(Input_t_CA),length(Input_t_CA));

    % Construct Kernel Matrix With Data For The Inlet Concentration
    
    j = 1;
    
    for i = 1:1:length(Input_t_CA0)
        
        g = 1;
        
        for z = 1:1:length(Input_t_CA0)
            
            B_Data_CA0(j,g) = sigf_square_1_CA0*exp((-1/(2*(l_square_1_CA0)))*((Input_t_CA0(1,j) - Input_t_CA0(1,g)))^2) + ...
                        + sigf_square_2_CA0*exp((-1/(2*(l_square_2_CA0)))*((Input_t_CA0(1,j) - Input_t_CA0(1,g)))^2);
            
            g = g + 1;
            
        end
        
        j = j + 1;
        
    end
    
    B_Data_CA0 = B_Data_CA0 + noise_var_data_CA0.*eye(length(Input_t_CA),length(Input_t_CA));
    
    B_Data = [B_Data_CA zeros(length(Input_t_CA),length(Input_t_CA));zeros(length(Input_t_CA0),length(Input_t_CA0)) B_Data_CA0];
         
    % Evaluate Gausian Process Objective Function For Initial Guess Of
    % Gaussian Process Hyperparameters
    
    GP_Objective(1,1) = -0.5*(2*sum(log(diag(chol(B_Data))))) ...
                        -0.5*((Output_Data)')/(B_Data)*Output_Data...
                        -0.5*length(Input_t)*log(2*pi);
                    
    for p = 1:1:GA_Iterations
        
    % Construct Kernel Matrix With Data For The Outlet Concentration
    
    j = 1;
    
    for i = 1:1:length(Input_t_CA)
        
        g = 1;
        
        for z = 1:1:length(Input_t_CA)
            
            B_Data_CA(j,g) = sigf_square_1_CA*exp((-1/(2*(l_square_1_CA)))*((Input_t_CA(1,j) - Input_t_CA(1,g))^2)) + ...
                        + sigf_square_2_CA*exp((-1/(2*(l_square_2_CA)))*((Input_t_CA(1,j) - Input_t_CA(1,g))^2));
            
            g = g + 1;
            
        end
        
        j = j + 1;
        
    end
    
    B_Data_CA = B_Data_CA + noise_var_data_CA.*eye(length(Input_t_CA),length(Input_t_CA));
    
    % Construct Kernel Matrix With Data For The Inlet Disturbance CA0
    
    j = 1;
    
    for i = 1:1:length(Input_t_CA0)
        
        g = 1;
        
        for z = 1:1:length(Input_t_CA0)
            
            B_Data_CA0(j,g) = sigf_square_1_CA0*exp((-1/(2*(l_square_1_CA0)))*((Input_t_CA0(1,j) - Input_t_CA0(1,g)))^2) + ...
                        + sigf_square_2_CA0*exp((-1/(2*(l_square_2_CA0)))*((Input_t_CA0(1,j) - Input_t_CA0(1,g)))^2);
            
            g = g + 1;
            
        end
        
        j = j + 1;
        
    end
    
    B_Data_CA0 = B_Data_CA0 + noise_var_data_CA0.*eye(length(Input_t_CA),length(Input_t_CA));
    
    B_Data = [B_Data_CA zeros(length(Input_t_CA),length(Input_t_CA));zeros(length(Input_t_CA0),length(Input_t_CA0)) B_Data_CA0]; 
    
    % Outlet Concentration Derivative Matrices
    
    % Construct Theta_1_CA Derivative Matrix
    
    j = 1;
        
    for i = 1:1:length(Input_t_CA)
        
        g = 1;
        
        for z = 1:1:length(Input_t_CA)
            
            B_theta_1_CA(j,g) = sigf_square_1_CA*exp((-1/(2*(l_square_1_CA)))*((Input_t_CA(1,j) - Input_t_CA(1,g))^2));

            g = g + 1;
            
        end
        
        j = j + 1;
        
    end       
    
    % Construct Theta_2_CA Derivative Matrix
    
    j = 1;
        
    for i = 1:1:length(Input_t_CA)
        
        g = 1;
        
        for z = 1:1:length(Input_t_CA)
            
            B_theta_2_CA(j,g) = sigf_square_2_CA*exp((-1/(2*(l_square_2_CA)))*((Input_t_CA(1,j) - Input_t_CA(1,g))^2));

            g = g + 1;
            
        end
        
        j = j + 1;
        
    end    
    
    % Construct Theta_3_CA Derivative Matrix
    
    B_theta_3_CA = noise_var_data_CA.*eye(length(Input_t_CA),length(Input_t_CA));
    
    % Construct Theta_4_CA Derivative Matrix
    
    j = 1;
        
    for i = 1:1:length(Input_t_CA)
        
        g = 1;
        
        for z = 1:1:length(Input_t_CA)
            
            B_theta_4_CA(j,g) = ((sigf_square_1_CA*((Input_t_CA(1,j) - Input_t_CA(1,g))^2))/(2*l_square_1_CA))*exp((-1/(2*(l_square_1_CA)))*((Input_t_CA(1,j) - Input_t_CA(1,g))^2));

            g = g + 1;
            
        end
        
        j = j + 1;
        
    end         
    
    % Construct Theta_5_CA Derivative Matrix
    
    j = 1;
        
    for i = 1:1:length(Input_t_CA)
        
        g = 1;
        
        for z = 1:1:length(Input_t_CA)
            
            B_theta_5_CA(j,g) = ((sigf_square_2_CA*((Input_t_CA(1,j) - Input_t_CA(1,g))^2))/(2*l_square_2_CA))*exp((-1/(2*(l_square_2_CA)))*((Input_t_CA(1,j) - Input_t_CA(1,g))^2));

            g = g + 1;
            
        end
        
        j = j + 1;
        
    end         

    % Inlet Disturbance CA0 Derivative Matrices   
    
    % Construct Theta_1_CA0 Derivative Matrix
    
    j = 1;
        
    for i = 1:1:length(Input_t_CA0)
        
        g = 1;
        
        for z = 1:1:length(Input_t_CA0)
            
            B_theta_1_CA0(j,g) = sigf_square_1_CA0*exp((-1/(2*(l_square_1_CA0)))*((Input_t_CA0(1,j) - Input_t_CA0(1,g))^2));

            g = g + 1;
            
        end
        
        j = j + 1;
        
    end      
    
    % Construct Theta_2_CA0 Derivative Matrix
    
    j = 1;
        
    for i = 1:1:length(Input_t_CA0)
        
        g = 1;
        
        for z = 1:1:length(Input_t_CA0)
            
            B_theta_2_CA0(j,g) = sigf_square_2_CA0*exp((-1/(2*(l_square_2_CA0)))*((Input_t_CA0(1,j) - Input_t_CA0(1,g))^2));

            g = g + 1;
            
        end
        
        j = j + 1;
        
    end   
    
    % Construct Theta_3_CA0 Derivative Matrix
    
    B_theta_3_CA0 = noise_var_data_CA0.*eye(length(Input_t_CA0),length(Input_t_CA0));    
    
    % Construct Theta_4_CA0 Derivative Matrix
    
    j = 1;
        
    for i = 1:1:length(Input_t_CA0)
        
        g = 1;
        
        for z = 1:1:length(Input_t_CA0)
            
            B_theta_4_CA0(j,g) = ((sigf_square_1_CA0*((Input_t_CA0(1,j) - Input_t_CA0(1,g))^2))/(2*l_square_1_CA0))*exp((-1/(2*(l_square_1_CA0)))*((Input_t_CA0(1,j) - Input_t_CA0(1,g))^2));

            g = g + 1;
            
        end
        
        j = j + 1;
        
    end     
    
    % Construct Theta_5_CA0 Derivative Matrix
    
    j = 1;
        
    for i = 1:1:length(Input_t_CA0)
        
        g = 1;
        
        for z = 1:1:length(Input_t_CA0)
            
            B_theta_5_CA0(j,g) = ((sigf_square_2_CA0*((Input_t_CA0(1,j) - Input_t_CA0(1,g))^2))/(2*l_square_2_CA0))*exp((-1/(2*(l_square_2_CA0)))*((Input_t_CA0(1,j) - Input_t_CA0(1,g))^2));

            g = g + 1;
            
        end
        
        j = j + 1;
        
    end     

    % Calculate alpha
    
    alpha = (B_Data)\Output_Data;
    
    % Outlet Concentration Gaussian Process Parameter Updates
    % -> Gradient Ascent
    
    new_theta_1_CA = theta_1_CA + Gamma*(0.5*trace(((alpha*(alpha')) - (inv([B_Data_CA zeros(length(Input_t_CA),length(Input_t_CA));zeros(length(Input_t_CA0),length(Input_t_CA0)) B_Data_CA0])))*([B_theta_1_CA zeros(length(Input_t_CA),length(Input_t_CA));zeros(length(Input_t_CA0),length(Input_t_CA0)) B_theta_1_CA0])));
    new_theta_2_CA = theta_2_CA + Gamma*(0.5*trace(((alpha*(alpha')) - (inv([B_Data_CA zeros(length(Input_t_CA),length(Input_t_CA));zeros(length(Input_t_CA0),length(Input_t_CA0)) B_Data_CA0])))*([B_theta_2_CA zeros(length(Input_t_CA),length(Input_t_CA));zeros(length(Input_t_CA0),length(Input_t_CA0)) B_theta_2_CA0])));
    new_theta_3_CA = theta_3_CA + Gamma*(0.5*trace(((alpha*(alpha')) - (inv([B_Data_CA zeros(length(Input_t_CA),length(Input_t_CA));zeros(length(Input_t_CA0),length(Input_t_CA0)) B_Data_CA0])))*([B_theta_3_CA zeros(length(Input_t_CA),length(Input_t_CA));zeros(length(Input_t_CA0),length(Input_t_CA0)) B_theta_3_CA0])));
    new_theta_4_CA = theta_4_CA + Gamma*(0.5*trace(((alpha*(alpha')) - (inv([B_Data_CA zeros(length(Input_t_CA),length(Input_t_CA));zeros(length(Input_t_CA0),length(Input_t_CA0)) B_Data_CA0])))*([B_theta_4_CA zeros(length(Input_t_CA),length(Input_t_CA));zeros(length(Input_t_CA0),length(Input_t_CA0)) B_theta_4_CA0])));
    new_theta_5_CA = theta_5_CA + Gamma*(0.5*trace(((alpha*(alpha')) - (inv([B_Data_CA zeros(length(Input_t_CA),length(Input_t_CA));zeros(length(Input_t_CA0),length(Input_t_CA0)) B_Data_CA0])))*([B_theta_5_CA zeros(length(Input_t_CA),length(Input_t_CA));zeros(length(Input_t_CA0),length(Input_t_CA0)) B_theta_5_CA0])));
    
    % Inlet Disturbance CA0 Gaussian Process Parameter Updates
    % -> Gradient Ascent
    
    new_theta_1_CA0 = theta_1_CA0 + Gamma*(0.5*trace(((alpha*(alpha')) - (inv([B_Data_CA zeros(length(Input_t_CA),length(Input_t_CA));zeros(length(Input_t_CA0),length(Input_t_CA0)) B_Data_CA0])))*([B_theta_1_CA zeros(length(Input_t_CA),length(Input_t_CA));zeros(length(Input_t_CA0),length(Input_t_CA0)) B_theta_1_CA0])));
    new_theta_2_CA0 = theta_2_CA0 + Gamma*(0.5*trace(((alpha*(alpha')) - (inv([B_Data_CA zeros(length(Input_t_CA),length(Input_t_CA));zeros(length(Input_t_CA0),length(Input_t_CA0)) B_Data_CA0])))*([B_theta_2_CA zeros(length(Input_t_CA),length(Input_t_CA));zeros(length(Input_t_CA0),length(Input_t_CA0)) B_theta_2_CA0])));
    new_theta_3_CA0 = theta_3_CA0 + Gamma*(0.5*trace(((alpha*(alpha')) - (inv([B_Data_CA zeros(length(Input_t_CA),length(Input_t_CA));zeros(length(Input_t_CA0),length(Input_t_CA0)) B_Data_CA0])))*([B_theta_3_CA zeros(length(Input_t_CA),length(Input_t_CA));zeros(length(Input_t_CA0),length(Input_t_CA0)) B_theta_3_CA0])));
    new_theta_4_CA0 = theta_4_CA0 + Gamma*(0.5*trace(((alpha*(alpha')) - (inv([B_Data_CA zeros(length(Input_t_CA),length(Input_t_CA));zeros(length(Input_t_CA0),length(Input_t_CA0)) B_Data_CA0])))*([B_theta_4_CA zeros(length(Input_t_CA),length(Input_t_CA));zeros(length(Input_t_CA0),length(Input_t_CA0)) B_theta_4_CA0])));
    new_theta_5_CA0 = theta_5_CA0 + Gamma*(0.5*trace(((alpha*(alpha')) - (inv([B_Data_CA zeros(length(Input_t_CA),length(Input_t_CA));zeros(length(Input_t_CA0),length(Input_t_CA0)) B_Data_CA0])))*([B_theta_5_CA zeros(length(Input_t_CA),length(Input_t_CA));zeros(length(Input_t_CA0),length(Input_t_CA0)) B_theta_5_CA0])));   
    
    % Outlet Concentration Gaussian Process Log Parameter Assignment
    
    theta_1_CA = new_theta_1_CA;
    theta_2_CA = new_theta_2_CA;
    theta_3_CA = new_theta_3_CA;
    theta_4_CA = new_theta_4_CA;
    theta_5_CA = new_theta_5_CA;
    
    % Inlet Disturbance CA0 Gaussian Process Log Parameter Assignment
    
    theta_1_CA0 = new_theta_1_CA0;
    theta_2_CA0 = new_theta_2_CA0;
    theta_3_CA0 = new_theta_3_CA0;
    theta_4_CA0 = new_theta_4_CA0;
    theta_5_CA0 = new_theta_5_CA0;
    
    % Outlet Concentration Gaussian Process Parameters
    
    sigf_square_1_CA = exp(theta_1_CA);
    sigf_square_2_CA = exp(theta_2_CA);
    noise_var_data_CA = exp(theta_3_CA);
    l_square_1_CA = exp(theta_4_CA);
    l_square_2_CA = exp(theta_5_CA);
    
    % Inlet Disturbance CA0 Gaussian Process Parameters
    
    sigf_square_1_CA0 = exp(theta_1_CA0);
    sigf_square_2_CA0 = exp(theta_2_CA0);
    noise_var_data_CA0 = exp(theta_3_CA0);
    l_square_1_CA0 = exp(theta_4_CA0);
    l_square_2_CA0 = exp(theta_5_CA0);    
    
    % Outlet Concentration Gaussian Process Parameters Used To Construct
    % Matrices
    
    sigf_estimated_1_CA = sqrt(exp(theta_1_CA));
    sigf_estimated_2_CA = sqrt(exp(theta_2_CA));
    noise_var_data_estimated_CA = exp(theta_3_CA);
    l_estimated_1_CA = sqrt(exp(theta_4_CA));
    l_estimated_2_CA = sqrt(exp(theta_5_CA));
    
    % Inlet Disturbance CA0 Gaussian Process Parameters Used To Construct
    % Matrices
    
    sigf_estimated_1_CA0 = sqrt(exp(theta_1_CA0));
    sigf_estimated_2_CA0 = sqrt(exp(theta_2_CA0));
    noise_var_data_estimated_CA0 = exp(theta_3_CA0);
    l_estimated_1_CA0 = sqrt(exp(theta_4_CA0));
    l_estimated_2_CA0 = sqrt(exp(theta_5_CA0));    
    
    % Evaluate Gaussian Process Objective Function To Establish Whether
    % Convergence Is Reached After Each Iteration
    
    GP_Objective(p + 1,1) = -0.5*(2*sum(log(diag(chol(B_Data))))) ...
                            -0.5*((Output_Data)')/(B_Data)*Output_Data...
                            -0.5*length(Input_t)*log(2*pi);  
                    
    if p >= 2 && abs(abs(GP_Objective(p + 1,1)) - abs(GP_Objective(p,1))) < 0.1
        
        break;
        
    end
     
    end
    
    % Save Optimised Parameter Values For Each Optimisation Routine
    
    Max_GP_Objective(run_repeat,:) = [sigf_estimated_1_CA sigf_estimated_2_CA noise_var_data_estimated_CA l_estimated_1_CA l_estimated_2_CA ...
        sigf_estimated_1_CA0 sigf_estimated_2_CA0 noise_var_data_estimated_CA0 l_estimated_1_CA0 l_estimated_2_CA0 GP_Objective(p + 1,1)];
    
%     % Visualise If Objective Function Maximum Is Reached
%     figure(run_repeat + 17)
%     plot(0:p,GP_Objective(1:p + 1,1),'k-','LineWidth',2)
%     axis([0 p -Inf Inf])
%     xlabel('p^t^h iteration')
%     ylabel('Log Marginal Likelihood')
    
end

% Extract Maximum Gaussian Process Objective Function Value With Associated Optimised Parameters

[Max_Num,Max_Index] = max(Max_GP_Objective(:,11));

% Outlet Concentration Gaussian Process Parameters Used To Construct Kernel
% Matrices

sigf_estimated_1_CA = Max_GP_Objective(Max_Index,1);
sigf_estimated_2_CA = Max_GP_Objective(Max_Index,2);
noise_var_data_estimated_CA = Max_GP_Objective(Max_Index,3);
l_estimated_1_CA = Max_GP_Objective(Max_Index,4);
l_estimated_2_CA = Max_GP_Objective(Max_Index,5);

% Inlet Disturbance CA0 Gaussian Process Parameters Used To Construct
% Kernel Matrices

sigf_estimated_1_CA0 = Max_GP_Objective(Max_Index,6);
sigf_estimated_2_CA0 = Max_GP_Objective(Max_Index,7);
noise_var_data_estimated_CA0 = Max_GP_Objective(Max_Index,8);
l_estimated_1_CA0 = Max_GP_Objective(Max_Index,9);
l_estimated_2_CA0 = Max_GP_Objective(Max_Index,10);

% End Of Algorithm Execution Time Stopwatch - Optimisation Routine

t_end_optimisation = toc;

% Start Algorithm Execution Stopwatch - ode Regression

tic;

% Construct Gaussian Process To Infer The Underlying State Variable CA

Predict_Past = 0;
Predict_Future = 0;

% State Variable CA Prediction Range

CA_Predict_t = linspace(0 - Predict_Past, Sim_Time + Predict_Future,length(Input_t_CA));
CA0_Predict_t = linspace(0 - Predict_Past, Sim_Time + Predict_Future,length(Input_t_CA0));

% Pre-Allocate Memory For Kernel Matrices For State Variable CA

B_CA_aa = zeros(length(CA_Predict_t),length(CA_Predict_t));
B_CA_ab = zeros(length(CA_Predict_t),length(Input_t_CA));
B_CA_bb = zeros(length(Input_t_CA),length(Input_t_CA));
B_CA0_bb = zeros(length(Input_t_CA0),length(Input_t_CA0));

% Populate Kernel Matrices In An Elementwise Manner

j = 1;

for i = 1:1:length(CA_Predict_t)

    g = 1;

    for z = 1:1:length(CA_Predict_t)

        B_CA_aa(j,g) = ((sigf_estimated_1_CA)^2)*exp((-1/(2*(((l_estimated_1_CA))^2)))*((CA_Predict_t(1,j) - CA_Predict_t(1,g))^2)) + ...
                    + ((sigf_estimated_2_CA)^2)*exp((-1/(2*(((l_estimated_2_CA))^2)))*((CA_Predict_t(1,j) - CA_Predict_t(1,g))^2));

        g = g + 1;

    end

    j = j + 1;

end

% Addition Of "Jitter" For Numerical Stability

B_CA_aa = B_CA_aa + (1*10^-12)*eye(length(CA_Predict_t),length(CA_Predict_t));

j = 1;

for i = 1:1:length(CA_Predict_t)

    g = 1;

    for z = 1:1:length(Input_t_CA)

        B_CA_ab(j,g) = ((sigf_estimated_1_CA)^2)*exp((-1/(2*(((l_estimated_1_CA))^2)))*((CA_Predict_t(1,j) - Input_t_CA(1,g))^2)) + ...
                    + ((sigf_estimated_2_CA)^2)*exp((-1/(2*(((l_estimated_2_CA))^2)))*((CA_Predict_t(1,j) - Input_t_CA(1,g))^2));

        g = g + 1;

    end

    j = j + 1;

end

B_ab_CA = [B_CA_ab zeros(length(CA_Predict_t),length(Input_t_CA))];
B_ba_CA = (B_ab_CA)';

j = 1;

for i = 1:1:length(Input_t_CA)

    g = 1;

    for z = 1:1:length(Input_t_CA)

        B_CA_bb(j,g) = ((sigf_estimated_1_CA)^2)*exp((-1/(2*(((l_estimated_1_CA))^2)))*((Input_t_CA(1,j) - Input_t_CA(1,g))^2)) + ...
                    + ((sigf_estimated_2_CA)^2)*exp((-1/(2*(((l_estimated_2_CA))^2)))*((Input_t_CA(1,j) - Input_t_CA(1,g))^2));

        g = g + 1;

    end

    j = j + 1;

end

CA_bb_Total = B_CA_bb + noise_var_data_estimated_CA.*eye(length(Input_t_CA),length(Input_t_CA));

j = 1;

for i = 1:1:length(Input_t_CA0)

    g = 1;

    for z = 1:1:length(Input_t_CA0)

        B_CA0_bb(j,g) = ((sigf_estimated_1_CA0)^2)*exp((-1/(2*(((l_estimated_1_CA0))^2)))*((Input_t_CA0(1,j) - Input_t_CA0(1,g))^2)) + ...
                    + ((sigf_estimated_2_CA0)^2)*exp((-1/(2*(((l_estimated_2_CA0))^2)))*((Input_t_CA0(1,j) - Input_t_CA0(1,g))^2));

        g = g + 1;

    end

    j = j + 1;

end

CA0_bb_Total = B_CA0_bb + noise_var_data_estimated_CA0.*eye(length(Input_t_CA0),length(Input_t_CA0));

B_bb_Total_CA = [CA_bb_Total zeros(length(Input_t_CA),length(Input_t_CA));zeros(length(Input_t_CA0),length(Input_t_CA0)) CA0_bb_Total];

% Use Standard Results For Conditioning On A Gaussian Distribution To
% Obtain a Distribution Over CA Function Values

mu_CA = (B_ab_CA)/(B_bb_Total_CA)*Output_Data;
covariance_CA = B_CA_aa - (B_ab_CA)/(B_bb_Total_CA)*B_ba_CA;

% Visualise Inferred CA State Variable Function Values - MAP Estimate Used

figure(3 + run_repeat)
S2 = diag(covariance_CA);
f = [mu_CA + 2.576*sqrt(S2);flip(mu_CA - 2.576*sqrt(S2),1)];
h = fill([CA_Predict_t'; flip(CA_Predict_t',1)], f, [6 6 6]/10, 'EdgeColor', [5 5 5]/8);
set(h,'facealpha',1);
hold on
plot(CA_Sensor.CA_Noise_Free.Time,CA_Sensor.CA_Noise_Free.Data,'b--','LineWidth',2)
hold on
plot(CA_Sensor.CA_Noise.Time,CA_Sensor.CA_Noise.Data,'kx','MarkerSize',8,'LineWidth',2)
hold on
plot(CA_Predict_t,mu_CA,'r','LineWidth',2)
hold on
x_steady_state = linspace(0,Sim_Time,Sim_Time/5);
Steady = plot(x_steady_state,CA_initial.*ones(length(x_steady_state),1),'m.','LineWidth',2,'MarkerSize',10);
hold on
x_Upper_Limit = linspace(0,Sim_Time,Sim_Time);
Upper = plot(x_Upper_Limit,0.4885.*ones(length(x_Upper_Limit),1),'m--','LineWidth',2); % Upper NOC Threshold Value
hold on
x_Lower_Limit = linspace(0,Sim_Time,Sim_Time);
Lower = plot(x_Lower_Limit,0.4420.*ones(length(x_Lower_Limit),1),'m--','LineWidth',2); % Lower NOC Threshold Value
%title('CSTR Dynamic Reponse: Random Input in C_A_0')
xlabel('Time (min)')
ylabel('C_A (mole/m^3)')
legend('99% Credibility Interval','Underlying State Variable Dynamic Response','Outlet Concentration Sensor Measurements','C_A (State Variable) - Gaussian Process MAP Estimate','True Underlying Steady State','Upper/Lower NOC Limit','Lower NOC Limit','Location','SouthEast')
legend('boxoff')
set(get(get(Lower,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
set(get(get(Upper,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
set(get(get(Steady,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
axis([0 Sim_Time 0.42 0.50])
set(gca,'Box','off')
hold off

% Construct Gaussian Process To Infer Underlying Input Disturbance CA0

% Pre-Allocate Memory For Kernel Matrices For Input Disturbance CA0

B_CA0_aa = zeros(length(CA0_Predict_t),length(CA0_Predict_t));
B_CA0_ab = zeros(length(CA0_Predict_t),length(Input_t_CA0));

% Populate Kernel Matrices In An Elementwise Manner

j = 1;

for i = 1:1:length(CA_Predict_t)

    g = 1;

    for z = 1:1:length(CA_Predict_t)

        B_CA0_aa(j,g) = ((sigf_estimated_1_CA0)^2)*exp((-1/(2*(((l_estimated_1_CA0))^2)))*((CA0_Predict_t(1,j) - CA0_Predict_t(1,g))^2)) + ...
                    + ((sigf_estimated_2_CA0)^2)*exp((-1/(2*(((l_estimated_2_CA0))^2)))*((CA0_Predict_t(1,j) - CA0_Predict_t(1,g))^2));

        g = g + 1;

    end

    j = j + 1;

end

% Addition Of "Jitter" For Numerical Stability

B_CA0_aa = B_CA0_aa + (1*10^-12)*eye(length(CA_Predict_t),length(CA_Predict_t));

j = 1;

for i = 1:1:length(CA0_Predict_t)

    g = 1;

    for z = 1:1:length(Input_t_CA0)

        B_CA0_ab(j,g) = ((sigf_estimated_1_CA0)^2)*exp((-1/(2*(((l_estimated_1_CA0))^2)))*((CA0_Predict_t(1,j) - Input_t_CA0(1,g))^2)) + ...
                    + ((sigf_estimated_2_CA0)^2)*exp((-1/(2*(((l_estimated_2_CA0))^2)))*((CA0_Predict_t(1,j) - Input_t_CA0(1,g))^2));

        g = g + 1;

    end

    j = j + 1;

end

B_ab_CA0 = [zeros(length(CA0_Predict_t),length(Input_t_CA0)) B_CA0_ab];
B_ba_CA0 = (B_ab_CA0)';

B_bb_Total_CA0 = [CA_bb_Total zeros(length(Input_t_CA),length(Input_t_CA));zeros(length(Input_t_CA0),length(Input_t_CA0)) CA0_bb_Total];

% Use Standard Results For Conditioning On A Gaussian Distribution To
% Obtain a Distribution Over CA0 Function Values

mu_CA0 = (B_ab_CA0)/(B_bb_Total_CA0)*Output_Data;
covariance_CA0 = B_CA0_aa - (B_ab_CA0)/(B_bb_Total_CA0)*B_ba_CA0;

% Visualise Inferred CA0 State Variable Function Values - MAP Estimate Used

figure(4 + run_repeat)
S2_CA0 = diag(covariance_CA0);
f = [mu_CA0 + 2.576*sqrt(S2_CA0); flip(mu_CA0 - 2.576*sqrt(S2_CA0),1)];
h = fill([CA0_Predict_t'; flip(CA0_Predict_t',1)], f, [6 6 6]/10, 'EdgeColor', [5 5 5]/8);
set(h,'facealpha',1);
hold on
plot(CA0_Sensor.CA0_Noise_Free.Time,CA0_Sensor.CA0_Noise_Free.Data,'b--','LineWidth',2)
hold on
plot(CA0_Sensor.CA0_Noise.Time,CA0_Sensor.CA0_Noise.Data,'kx','MarkerSize',8,'LineWidth',2)
hold on
plot(CA0_Predict_t,mu_CA0,'r','LineWidth',2)
hold on
%title('C_A_0 Exogenous Input Disturbance')
xlabel('Time (min)')
ylabel('C_A_O (mole/m^3)')
hold on
x_Steady = linspace(0,Sim_Time,Sim_Time/5);
Steady = plot(x_Steady,CA0_initial.*ones(length(x_Steady),1),'m.','LineWidth',2,'MarkerSize',10);
hold on
x_Upper_Limit = linspace(0,Sim_Time,1000);
Upper = plot(x_Upper_Limit,1.05*CA0_initial*ones(length(x_Upper_Limit),1),'m--','LineWidth',2); % Upper NOC Threshold Value
hold on
x_Lower_Limit = linspace(0,Sim_Time,1000);
Lower = plot(x_Lower_Limit,0.95*CA0_initial*ones(length(x_Upper_Limit),1),'m--','LineWidth',2); % Lower NOC Threshold Value
legend('boxoff')
legend('99% Credibility Interval','Underlying Exogenous Input Disturbance','Inlet Concentration Sensor Measurements','C_A_0 (Exogenous Input) - Gaussian Process MAP Estimate','True Underlying Steady State','Upper NOC Limit','Lower NOC Limit','Location','SouthEast')
axis([0 Sim_Time 0.83 1.005])
set(gca,'Box','off')
set(get(get(Lower,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
set(get(get(Upper,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
set(get(get(Steady,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
hold off

% Construct Gaussian Process To Infer Underlying State Variable Derivative

% State Variable Derivative d(CA)/dt Prediction Range

dCA_Predict_t = linspace(0 - Predict_Past, Sim_Time + Predict_Future,length(Input_t_CA));

% Pre-Allocate Memory For Kernel Matrices For State Variable Derivative

B_CA_Dev_aa = zeros(length(dCA_Predict_t),length(dCA_Predict_t));
B_CA_Dev_ab = zeros(length(dCA_Predict_t),length(Input_t_CA));

j = 1;

for i = 1:1:length(dCA_Predict_t)

    g = 1;

    for z = 1:1:length(dCA_Predict_t)        

        B_CA_Dev_aa(j,g) = (-sigf_estimated_1_CA^2/((l_estimated_1_CA^4)))*(((dCA_Predict_t(1,g))^2) - (2*(dCA_Predict_t(1,j))*(dCA_Predict_t(1,g))) + ((dCA_Predict_t(1,j))^2) - ((l_estimated_1_CA^2)))*exp((-1/(2*(l_estimated_1_CA^2)))*((dCA_Predict_t(1,j) - dCA_Predict_t(1,g))^2)) + ...
                    (-sigf_estimated_2_CA^2/((l_estimated_2_CA^4)))*(((dCA_Predict_t(1,g))^2) - (2*(dCA_Predict_t(1,j))*(dCA_Predict_t(1,g))) + ((dCA_Predict_t(1,j))^2) - ((l_estimated_2_CA^2)))*exp((-1/(2*(l_estimated_2_CA^2)))*((dCA_Predict_t(1,j) - dCA_Predict_t(1,g))^2));                       
       
                g = g + 1;

    end

    j = j + 1;

end

% Addition Of "Jitter" For Numerical Stability

B_CA_Dev_aa = B_CA_Dev_aa + (1*10^-12).*eye(length(dCA_Predict_t),length(dCA_Predict_t)); 

j = 1;

for i = 1:1:length(dCA_Predict_t)

    g = 1;

    for z = 1:1:length(Input_t_CA)        

        B_CA_Dev_ab(j,g) = (-(sigf_estimated_1_CA^2)/((l_estimated_1_CA^2)))*((dCA_Predict_t(1,j) - Input_t_CA(1,g)))*exp((-1/(2*(l_estimated_1_CA^2)))*((dCA_Predict_t(1,j) - Input_t_CA(1,g))^2)) + ...
                    (-(sigf_estimated_2_CA^2)/((l_estimated_2_CA^2)))*((dCA_Predict_t(1,j) - Input_t_CA(1,g)))*exp((-1/(2*(l_estimated_2_CA^2)))*((dCA_Predict_t(1,j) - Input_t_CA(1,g))^2));
                       
        g = g + 1;

    end

    j = j + 1;

end

B_ab_CA_Dev = [B_CA_Dev_ab zeros(length(dCA_Predict_t),length(Input_t_CA0))];

B_ba_CA_Dev = (B_ab_CA_Dev)';

B_CA_Dev_bb_Total = [CA_bb_Total zeros(length(Input_t_CA),length(Input_t_CA));zeros(length(Input_t_CA0),length(Input_t_CA0)) CA0_bb_Total];

% Use Standard Results For Conditioning On A Gaussian Distribution To
% Obtain a Distribution Over State Variable Derivative Function Values

mu_dCA = (B_ab_CA_Dev)/(B_CA_Dev_bb_Total)*Output_Data;
covariance_dCA = B_CA_Dev_aa - (B_ab_CA_Dev)/(B_CA_Dev_bb_Total)*B_ba_CA_Dev;

% Visualise Inferred State Variable Derivative Function Values - MAP
% Estimate Used

figure(5 + run_repeat)
S2_CA = diag(covariance_dCA);
f = [mu_dCA + 2.576*sqrt(S2_CA);flip(mu_dCA - 2.576*sqrt(S2_CA),1)];
h = fill([dCA_Predict_t'; flip(dCA_Predict_t',1)], f, [6 6 6]/10, 'EdgeColor', [5 5 5]/8);
set(h,'facealpha',1);
hold on
plot(CA_Derivative_Information.Time,CA_Derivative_Information.Data,'b--','LineWidth',2)
hold on
plot(dCA_Predict_t,mu_dCA','r','LineWidth',2)
%title('Inferred State Derivative')
xlabel('Time (min)')
ylabel('C_A^'' (mole/[m^3 min])')
set(gca,'Box','off')
legend('99% Credibility Interval','Underlying State Derivative (Numerical Integration)','State Derivative - Gaussian Process MAP Estimate','Location','SouthEast')
legend('boxoff')
hold off

% Prior Hyperparameters

m0 = Parameter_Guess;
S0 = eye(length(m0),length(m0));

% Format The Required Basis Functions

Phi = [(mu_CA0 - mu_CA) -mu_CA]; 

% Regression Covariance Matrix

Regress_Covariance = diag(covariance_dCA).*eye(length(dCA_Predict_t),length(dCA_Predict_t));

% Posterior Distribution Results

An = ((inv(S0)) + ((Phi')/(Regress_Covariance)*Phi)); % Precision Matrix
Sn = inv(An); % Posterior Distribution Covariance Matrix
wn = (An)\(((Phi')/(Regress_Covariance)*mu_dCA) + (S0\(m0))); % Posterior Distribution Mean Vector

Phi = [(CA0_Sensor.CA0_Noise.Data - CA_Sensor.CA_Noise.Data) -CA_Sensor.CA_Noise.Data];

% End Of Algorithm Execution Time Stopwatch - ode Regression

t_end_Bayes = toc;

Bayes_Model_Parameters = wn;

%% Statistical Properties: Gaussian Process ODE Regression
% Algorithm 4

% Ground Truth Comparison - Qaulity Of Parameter Estimates

fprintf('\n\nBayesian Results (based on MAP Estimates)\n\n')
fprintf('\tF/V: %0.4f (1/min)\n',Bayes_Model_Parameters(1,1))
fprintf('\tk: %0.4f (1/min)\n',Bayes_Model_Parameters(2,1))
fprintf('\tCA0 Noise Variance: %0.9f (mole^2/m^6)\n',noise_var_data_estimated_CA0)
fprintf('\tCA Noise Variance: %0.9f (mole^2/m^6)\n',noise_var_data_estimated_CA)
fprintf('\tCA0 Noise Standard Deviation: %0.5f (mole/m^3)\n',sqrt(noise_var_data_estimated_CA0))
fprintf('\tCA Noise Standard Deviation: %0.5f (mole/m^3)\n\n',sqrt(noise_var_data_estimated_CA))

fprintf('\tF/V %% Error: %0.2f%% \n',((abs(Bayes_Model_Parameters(1,1) - (F/V)))/(F/V))*100)
fprintf('\tk %% Error: %0.2f%% \n',((abs(Bayes_Model_Parameters(2,1) - (k_reaction)))/(k_reaction))*100)
fprintf('\tCA0 Noise Standard Deviation %% Error: %0.2f%% \n',((abs(sqrt(noise_var_data_estimated_CA0) - CA0_Sensor_SDev))/(CA0_Sensor_SDev))*100)
fprintf('\tCA Noise Standard Deviation %% Error: %0.2f%% \n',((abs(sqrt(noise_var_data_estimated_CA) - CA_Sensor_SDev))/(CA_Sensor_SDev))*100)

% Parameter Marginal Credibility Interval

Cov_Bayes_Parameters =  diag(Sn);

z_score = 2.576; % z_score value - 99% Confidence/Credibility
fprintf('\n\t99%% Marginal Credibility Interval For F/V: [%0.4f (1/min), %0.4f (1/min)]',Bayes_Model_Parameters(1,1) - z_score*sqrt(Cov_Bayes_Parameters(1,1)), Bayes_Model_Parameters(1,1) + z_score*sqrt(Cov_Bayes_Parameters(1,1)))
fprintf('\n\t99%% Marginal Credibility Interval For k: [%0.4f (1/min), %0.4f (1/min)]\n',Bayes_Model_Parameters(2,1) - z_score*sqrt(Cov_Bayes_Parameters(2,1)), Bayes_Model_Parameters(2,1) + z_score*sqrt(Cov_Bayes_Parameters(2,1)))

% -> Visualise Parameter 1 Inference Results

figure(6 + run_repeat)

% Visualise Marginal Posterior Distribution over Parameter 1

x = linspace(0.94*Bayes_Model_Parameters(1,1),1.06*Bayes_Model_Parameters(1,1),1000);
plot(x,normpdf(x,Bayes_Model_Parameters(1,1),sqrt(Cov_Bayes_Parameters(1,1))) + 100,'r','LineWidth',2)
set(gca,'YTick',[],'Box','off')
set(gca,'YColor',[1 1 1])
ylim([0 1000])
xlim([Bayes_Model_Parameters(1,1) - 3.5*sqrt(Cov_Bayes_Parameters(1,1)) Bayes_Model_Parameters(1,1) + 3.5*sqrt(Cov_Bayes_Parameters(1,1)) ])
hold on

% Visualise 99% Credibility Interval For Parameter 1

x = Bayes_Model_Parameters(1,1);
y = 35;
errhigh = z_score*sqrt(Cov_Bayes_Parameters(1,1));
errlow = z_score*sqrt(Cov_Bayes_Parameters(1,1));
errorbar(x,y,errlow,errhigh,'horizontal','ro','LineWidth',2,'MarkerFaceColor','r','MarkerSize',8)
hold on
plot(F/V,y,'bx','LineWidth',3,'MarkerFaceColor','b','MarkerSize',10)
text(Bayes_Model_Parameters(1,1) - 0.00045,95,'(F/V)_M_A_P','FontSize',12)
text(F/V + 0.00005,95,'(F/V)_T_r_u_e','FontSize',12)
xlabel('F/V (1/min)')
set(gca,'YTick',[],'Box','off')
set(gca,'YColor',[1 1 1])
hold off

% -> Visualise Parameter 2 Inference Results

figure(7 + run_repeat)

% Visualise Marginal Posterior Distribution over Parameter 2

x = linspace(0.94*Bayes_Model_Parameters(2,1),1.06*Bayes_Model_Parameters(2,1),1000);
plot(x,normpdf(x,Bayes_Model_Parameters(2,1),sqrt(Cov_Bayes_Parameters(2,1))) + 100,'r','LineWidth',2)
set(gca,'YTick',[],'Box','off')
set(gca,'YColor',[1 1 1])
ylim([0 1000])
xlim([Bayes_Model_Parameters(2,1) - 3.5*sqrt(Cov_Bayes_Parameters(2,1)) Bayes_Model_Parameters(2,1) + 3.5*sqrt(Cov_Bayes_Parameters(2,1)) ])
hold on

% Visualise 99% Credibility Interval For Parameter 2

x = Bayes_Model_Parameters(2,1);
y = 35;
errhigh = z_score*sqrt(Cov_Bayes_Parameters(2,1));
errlow = z_score*sqrt(Cov_Bayes_Parameters(2,1));
errorbar(x,y,errlow,errhigh,'horizontal','ro','LineWidth',2,'MarkerFaceColor','r','MarkerSize',8)
hold on
plot(k_reaction,y,'bx','LineWidth',3,'MarkerFaceColor','b','MarkerSize',10)
text(Bayes_Model_Parameters(2,1) - 0.00035,85,'k_M_A_P','FontSize',12)
text(k_reaction + 0.00005,85,'k_T_r_u_e','FontSize',12)
xlabel('k (1/min)')
set(gca,'YTick',[],'Box','off')
set(gca,'YColor',[1 1 1])
hold off

% Algorithm Execution Time

fprintf('\n\tGaussian Process Hyperparameter Optimisation Time: %0.4f seconds\n',t_end_optimisation)
fprintf('\n\tRegression Time: %0.4f seconds\n',t_end_Bayes)
fprintf('\n\tElapsed Algorithm Time: %0.4f seconds\n',t_end_Bayes + t_end_optimisation)

% Joint Parameter Credibilty Interval
% ->  Ellipsoid Representation

figure(8 + run_repeat)
[EigenVec, EigenVal] = eig(Sn); % Calculate Eigenvalues And Eigenvectors

[Largest_EigenVec_Index, Num] = find(EigenVal == max(max(EigenVal))); % Obtain The Index Of The Largest Eigenvector
Largest_EigenVec = EigenVec(:,Largest_EigenVec_Index);

Largest_EigenVal = max(max(EigenVal)); % Obtain The Largest Eigenvalue

if (Largest_EigenVec_Index == 1) % Obtain The Smallest Eigenvector And EigenValue
    
    Smallest_EigenVal = max(EigenVal(:,2));
    Smallest_Eigen_Vec = EigenVec(:,2);
    
else
    
    Smallest_EigenVal = max(EigenVal(:,1));
    Smallest_Eigen_Vec = EigenVec(:,1);
    
end

Angle = atan2(Largest_EigenVec(2),Largest_EigenVec(1)); % Calculate The Angle Between The Cartesian x-axis And The Largest Eigenvector

if Angle < 0
    
    Angle = Angle + 2*pi; % Shift Angle To Be Between 0 and 2pi
    
end

Average = Bayes_Model_Parameters'; % Extract Mean Inferred Parameter Values

Chi_Value = sqrt(9.210); % Obtain Radius For The Error Ellipse
Theta_Grid = linspace(0,2*pi,400000);
F_V_Mean = Average(1);
k_Mean = Average(2);

A1 = Chi_Value*sqrt(Largest_EigenVal);
B1 = Chi_Value*sqrt(Smallest_EigenVal); 

Ellipse_X_r = A1*cos(Theta_Grid); % Ellipse Cartesian x-axis Coordinates
Ellipse_Y_r = B1*sin(Theta_Grid); % Ellipse Cartesian y-axis Coordinates

R = [cos(Angle) sin(Angle); -sin(Angle) cos(Angle)]; % Ellipse Rotation Matrix

r_Ellipse = [Ellipse_X_r;Ellipse_Y_r]'*R; % Rotate Ellipse

Bayes_Ellipse = plot(r_Ellipse(:,1) + F_V_Mean,r_Ellipse(:,2) + k_Mean,'r','LineWidth',3); % Visualise Ellipse
hold on
plot(Bayes_Model_Parameters(1,1),Bayes_Model_Parameters(2,1),'o','Color','r','LineWidth',2,'MarkerSize',8,'MarkerFaceColor','r')
ylabel('k (1/min)')
xlabel('F/V (1/min)')
set(gca,'Box','off')
hold on

%% Nonlinear Least Squares ODE Regression (Gauss-Newton Implementation)
%  Algorithm 2

% Data Required For Regression

Output_Data = CA_Sensor.CA_Noise.Data;
Input_t = CA_Sensor.CA_Noise.Time;

% Start Algorithm Execution Stopwatch

tic;

% NSIG To Establish Convergence

NSIG = 4;

% Pre-Allocate Memory For Least Squares Objective Function

Least_Squares_Objective = zeros(n,1);

% Pre-Allocate Memory For Updated Model Parameters Per Iteration

Frequentist_Model_Parameters = zeros(n,length(Parameter_Guess));

% Pre-Allocate Memory For Numerical Integration Results

CA_Time_Range = zeros(length(Output_Data),1);
CA_Results = zeros(length(Output_Data),1);
GR1 = zeros(length(Output_Data),1);
GR2 = zeros(length(Output_Data),1);

% Pre-Allocate Memory For Numerical Integration Results - Bisection Rule

CA_Time_Range_rho_1 = zeros(length(Output_Data),1);
CA_Results_rho_1 = zeros(length(Output_Data),1);
CA_Time_Range_rho_halved = zeros(length(Output_Data),1);
CA_Results_rho_halved = zeros(length(Output_Data),1);

% Pre-Alloctate Memory For Convergence Evaluation

CA_Time_Range_rho = zeros(length(Output_Data),1);
CA_Results_rho = zeros(length(Output_Data),1);

for re_regress = 1:1:1 % Can Repeat Regression If Required
    
    % Pre-Allocate Memory For Least Squares Objective Function
    
    Least_Squares_Objective = zeros(n,1);

    % Pre-Allocate Memory For Updated Model Parameters
    
    Frequentist_Model_Parameters = zeros(n,length(Parameter_Guess));

    % Pre-Allocate Memory For Numerical Integration Results
    
    CA_Time_Range = zeros(length(Output_Data),1);
    CA_Results = zeros(length(Output_Data),1);
    GR1 = zeros(length(Output_Data),1);
    GR2 = zeros(length(Output_Data),1);

    % Pre-Allocate Memory For Numerical Integration Results - Bisection Rule
    
    CA_Time_Range_rho_1 = zeros(length(Output_Data),1);
    CA_Results_rho_1 = zeros(length(Output_Data),1);
    CA_Time_Range_rho_halved = zeros(length(Output_Data),1);
    CA_Results_rho_halved = zeros(length(Output_Data),1);

    % Pre-Alloctate Memory For Convergence Evaluation
    
    CA_Time_Range_rho = zeros(length(Output_Data),1);
    CA_Results_rho = zeros(length(Output_Data),1);

    for j = 1:1:n

        Frequentist_Model_Parameters(j,1:length(Parameter_Guess)) = (Parameter_Guess)';

        i = 0; % Initialise Input Disturbance Counter
        Intial_CA = [CA_initial;0;0]; % Intial Value For "Experimental" Conditions
        
        K = Parameter_Guess.*eye(length(Parameter_Guess),length(Parameter_Guess));

        for tstart = 0:1:Sim_Time

            i = i + 1; % Increment Input Disturbance Counter
            
            tspan = [tstart tstart + 0.5 tstart + 1]; % Numerical Integration Time Span

            % Set Up Matrix Differential Equation
            
            Matrix_Diff_Equation = @(t,y) ...
                [Parameter_Guess(1,1)*((CA0_Sensor.CA0_Noise.Data(i,1)) - y(1)) - Parameter_Guess(2,1)*y(1); ... % CA = y(1)
                -(Parameter_Guess(1,1) + Parameter_Guess(2,1))*y(2) + ((CA0_Sensor.CA0_Noise.Data(i,1)) - y(1))*K(1,1); ... % GR1 = y(2)
                -(Parameter_Guess(1,1) + Parameter_Guess(2,1))*y(3) - y(1)*K(2,2)]; % GR2 = y(3)

            % Use ode45 To Numerically Integrate Matrix Differential Equation
            
            [t_temp, y_temp] = ode45(Matrix_Diff_Equation,tspan,Intial_CA);
            CA_Time_Range(tstart + 1,1) = t_temp(1,1);
            Intial_CA = [y_temp(3,1) y_temp(3,2) y_temp(3,3)];
            CA_Results(tstart + 1,1) = y_temp(1,1);
            GR1(tstart + 1,1) = y_temp(1,2);
            GR2(tstart + 1,1) = y_temp(1,3);       

        end

        % Compute Integrated Model Values
        
        t_sampling_period = CA_Time_Range(CA_Sensor.CA_Noise.Time + 1,1);
        D = 1; % Sensitivity Matrix
        CA_Model = D*CA_Results(t_sampling_period + 1,1); % Extract ode Model Predicted Results

        % Evaluate Least Squares Objective Function
        
        Least_Squares_Objective(j,1) = sum(((Output_Data) - CA_Model).^2);

        % Set Up Sensitivity Matrices - Jacobian (Vectorised)
        
        J = [GR1 GR2]; % <- From ode45 Numerical Integration

        % Set Up Linear Equations To Minimise The Simple Least Squares Error Function
        % -> Use The Reduced Sensitivity Matrix Gr       

        A_R = ((J)')*(D')*D*J;
        b_R = ((J)')*(D')*(Output_Data - CA_Model);

        % Step Update
        
        delta_k_R = (A_R)\b_R; % Solve System Of Linear Equations

        % Bisection Rule For Determining The Stepping Parameter Size rho
        % -> Performing a One Dimensional Line Search

        % Set Up Matrix Differential Equation For rho = 1
        
        k_rho_1 = 1*K*delta_k_R;

        i = 0; % Initialise Input Disturbance Counter
        
        Intial_CA = [CA_initial;0;0]; % Intial Value For "Experimental" Conditions

        for tstart = 0:1:Sim_Time

            i = i + 1; % Increment Input Disturbance Counter
            tspan = [tstart tstart + 0.5 tstart + 1]; % Numerical Integration Time Span

            % Set Up Matrix Differential Equation For rho = 1
            
            Matrix_Diff_Equation_rho_1 = @(t,y) ...
                [(Parameter_Guess(1,1) + k_rho_1(1,1))*((CA0_Sensor.CA0_Noise.Data(i,1)) - y(1)) - (Parameter_Guess(2,1) + k_rho_1(2,1))*y(1); ... % CA = y(1)
                -((Parameter_Guess(1,1) + k_rho_1(1,1)) + (Parameter_Guess(2,1) + k_rho_1(2,1)))*y(2) + ((CA0_Sensor.CA0_Noise.Data(i,1)) - y(1))*K(1,1); ... % GR1 = y(2)
                -((Parameter_Guess(1,1) + k_rho_1(1,1)) + (Parameter_Guess(2,1) + k_rho_1(2,1)))*y(3) - y(1)*K(2,2)]; % GR2 = y(3)

            % Use ode45 To Numerically Integrate Matrix Differential Equation
            
            [t_temp_rho_1, y_temp_rho_1] = ode45(Matrix_Diff_Equation_rho_1,tspan,Intial_CA);
            CA_Time_Range_rho_1(tstart + 1,1) = t_temp_rho_1(1,1);
            Intial_CA = [y_temp_rho_1(3,1) y_temp_rho_1(3,2) y_temp_rho_1(3,3)];
            CA_Results_rho_1(tstart + 1,1) = y_temp_rho_1(1,1);

        end

        % Compute Integrated Model Values
        
        t_sampling_period_rho_1 = CA_Time_Range_rho_1(CA_Sensor.CA_Noise.Time + 1,1);
        D = 1; % Sensitivity Matrix
        CA_Model_rho_1 = D*CA_Results_rho_1(t_sampling_period_rho_1 + 1,1); % Extract ode Model Predicted Results

        if sum(((Output_Data) - CA_Model_rho_1).^2) < Least_Squares_Objective(j,1)

            rho = 1; % Stepping Parameter Size

        else

            rho = 1; % Stepping Parameter Size

            for u = 1:1:max_trials

                rho = 0.5*rho; % Keep Halving Stepping Parameter Size

                % Set Up Matrix Differential Equation For New rho
                
                k_rho_halved = rho*K*delta_k_R;

                i = 0; % Initialise Input Disturbance Counter
                Intial_CA = [CA_initial;0;0]; % Intial Value For "Experimental" Conditions

                for tstart = 0:1:Sim_Time

                    i = i + 1; % Increment Input Disturbance Counter                
                    tspan = [tstart tstart + 0.5 tstart + 1]; % Numerical Integration Time Span

                    % Set Up Matrix Differential Equation
                    
                    Matrix_Diff_Equation_rho_halved = @(t,y) ...
                        [(Parameter_Guess(1,1) + k_rho_halved(1,1))*((CA0_Sensor.CA0_Noise.Data(i,1)) - y(1)) - (Parameter_Guess(2,1) + k_rho_halved(2,1))*y(1); ... % CA = y(1)
                        -((Parameter_Guess(1,1) + k_rho_halved(1,1)) + (Parameter_Guess(2,1) + k_rho_halved(2,1)))*y(2) + ((CA0_Sensor.CA0_Noise.Data(i,1)) - y(1))*K(1,1); ... % GR1 = y(2)
                        -((Parameter_Guess(1,1) + k_rho_halved(1,1)) + (Parameter_Guess(2,1) + k_rho_halved(2,1)))*y(3) - y(1)*K(2,2)]; % GR2 = y(3)

                    % Use ode45 To Numerically Integrate Matrix Differential Equation
                    
                    [t_temp_rho_halved, y_temp_rho_halved] = ode45(Matrix_Diff_Equation_rho_halved,tspan,Intial_CA);
                    CA_Time_Range_rho_halved(tstart + 1,1) = t_temp_rho_halved(1,1);
                    Intial_CA = [y_temp_rho_halved(3,1) y_temp_rho_halved(3,2) y_temp_rho_halved(3,3)];
                    CA_Results_rho_halved(tstart + 1,1) = y_temp_rho_halved(1,1);

                end

                % Compute Integrated Model Values
                
                t_sampling_period_rho_halved = CA_Time_Range_rho_halved(CA_Sensor.CA_Noise.Time + 1,1);
                D = 1; % Sensitivity Matrix
                CA_Model_rho_halved = D*CA_Results_rho_halved(t_sampling_period_rho_halved + 1,1); % Extract ode Model Predicted Results

                if sum(((Output_Data) - CA_Model_rho_halved).^2) < Least_Squares_Objective(j,1)

                    break;

                end           

            end

        end

        Parameter_Guess = Parameter_Guess + rho*K*delta_k_R;        
        fprintf('\nParameter Updates Per Iteration: [%0.4f,%0.4f]',Parameter_Guess(1,1),Parameter_Guess(2,1))

        if (1/length(Parameter_Guess))*sum(abs(K*delta_k_R./Parameter_Guess)) <= 10^(-NSIG) % Break Iteration If Convergence Is Achieved

            Frequentist_Model_Parameters(j + 1,1:length(Parameter_Guess)) = (Parameter_Guess)';

            i = 0; % Initialise Input Disturbance Counter
            Intial_CA = [CA_initial;0;0]; % Intial Value For "Experimental" Conditions    

            for tstart = 0:1:Sim_Time

                i = i + 1; % Increment Input Disturbance Counter
                tspan = [tstart tstart + 0.5 tstart + 1]; % Numerical Integration Time Span

                % Set Up Matrix Differential Equation
                
                Matrix_Diff_Equation = @(t,y) ...
                    [Parameter_Guess(1,1)*((CA0_Sensor.CA0_Noise.Data(i,1)) - y(1)) - Parameter_Guess(2,1)*y(1); ... % CA = y(1)
                    -(Parameter_Guess(1,1) + Parameter_Guess(2,1))*y(2) + ((CA0_Sensor.CA0_Noise.Data(i,1)) - y(1))*K(1,1); ... % GR1 = y(2)
                    -(Parameter_Guess(1,1) + Parameter_Guess(2,1))*y(3) - y(1)*K(2,2)]; % GR2 = y(3)

                % Use ode45 To Numerically Integrate Matrix Differential Equation
                
                [t_temp, y_temp] = ode45(Matrix_Diff_Equation,tspan,Intial_CA);
                CA_Time_Range(tstart + 1,1) = t_temp(1,1);
                Intial_CA = [y_temp(3,1) y_temp(3,2) y_temp(3,3)];
                CA_Results(tstart + 1,1) = y_temp(1,1);
                GR1(tstart + 1,1) = y_temp(1,2);
                GR2(tstart + 1,1) = y_temp(1,3);    

            end

            % Compute Integrated Model Values
            
            t_sampling_period = CA_Time_Range(CA_Sensor.CA_Noise.Time + 1,1);
            D = 1; % Sensitivity Matrix
            CA_Model = D*CA_Results(t_sampling_period + 1,1); % Extract ode Model Predicted Results

            % Evaluate Least Squares Objective Function
            
            Least_Squares_Objective(j + 1,1) = sum(((Output_Data) - CA_Model).^2);
            Cov_A = (Least_Squares_Objective(j + 1,1)/(N - length(Parameter_Guess))).*inv((([(1/K(1,1)).*GR1 (1/K(2,2)).*GR2])')*[(1/K(1,1)).*GR1 (1/K(2,2)).*GR2]);

            break;

        end

    end
    
    Parameter_Guess = Frequentist_Model_Parameters(j + 1,1:length(Parameter_Guess))';
    
end

% End Of Algorithm Execution Time Stopwatch

t_end_Frequentist = toc;

Frequentist_Model_Parameters = (Frequentist_Model_Parameters(j + 1,1:length(Parameter_Guess)))';

% Visualise If Least Squares Objective Function Has Converged To a Minimum

figure(9 + run_repeat)
plot(1:j+1,Least_Squares_Objective(1:j+1),'kx--','LineWidth',2,'MarkerSize',12)
title('Least Squares Objective Function')
xlabel('i^t^h iteration')
ylabel('LS(w)')
set(gca,'XTick',1:j+1)
xlim([1 j+1])
set(gca,'Box','off')
hold off

%% Statistical Properties: Nonlinear Least Squares ODE Regression (Gauss-Newton Implementation)
% Algorithm (2)

% Ground Truth Comparison - Quality of Parameter Estimates

fprintf('\n\nFrequentist Results (based on Maximum Likelihood Estimates)\n\n')
fprintf('\tF/V: %0.4f (1/min)\n',Frequentist_Model_Parameters(1,1))
fprintf('\tk: %0.4f (1/min)\n',Frequentist_Model_Parameters(2,1))
fprintf('\tCA Noise Variance: %0.9f (mole^2/m^6)\n',(Least_Squares_Objective(j + 1,1)/(N - length(Parameter_Guess))))
fprintf('\tCA Noise Standard Deviation: %0.5f (mole/m^3)\n\n',sqrt((Least_Squares_Objective(j + 1,1)/(N - length(Parameter_Guess)))))

fprintf('\tProcess Gain %% Error: %0.2f%% \n',((abs(Frequentist_Model_Parameters(1,1) - (F/V)))/(F/V))*100)
fprintf('\tProcess Time Constant %% Error: %0.2f%% \n',((abs(Frequentist_Model_Parameters(2,1) - (k_reaction)))/(k_reaction))*100)
fprintf('\tCA Noise Standard Deviation %% Error: %0.2f%% \n',((abs((sqrt((Least_Squares_Objective(j + 1,1)/(N - length(Parameter_Guess))))) - CA_Sensor_SDev))/(CA_Sensor_SDev))*100)

% Parameter Marginal Confidence Interval

Cov_Frequentist_Parameters =  diag(Cov_A);

fprintf('\n\t99%% Marginal Confidence Interval For F/V: [%0.4f (1/min), %0.4f (1/min)]',Frequentist_Model_Parameters(1,1) - z_score*sqrt(Cov_Frequentist_Parameters(1,1)), Frequentist_Model_Parameters(1,1) + z_score*sqrt(Cov_Frequentist_Parameters(1,1)))
fprintf('\n\t99%% Marginal Confidence Interval For k: [%0.4f (1/min), %0.4f (1/min)]\n',Frequentist_Model_Parameters(2,1) - z_score*sqrt(Cov_Frequentist_Parameters(2,1)), Frequentist_Model_Parameters(2,1) + z_score*sqrt(Cov_Frequentist_Parameters(2,1)))

% -> Visualise 99% Marginal Confidence Interval For Parameter 1

figure(10 + run_repeat)

x = Frequentist_Model_Parameters(1,1);
y = 35;
errhigh = z_score*sqrt(Cov_Frequentist_Parameters(1,1));
errlow = z_score*sqrt(Cov_Frequentist_Parameters(1,1));
errorbar(x,y,errlow,errhigh,'horizontal','o','Color',[0.4660 0.6740 0.1880],'LineWidth',2,'MarkerFaceColor',[0.4660 0.6740 0.1880],'MarkerSize',8)
hold on
plot(F/V,y,'bx','LineWidth',3,'MarkerFaceColor','b','MarkerSize',10)
text(Frequentist_Model_Parameters(1,1) - 0.00075,35.1,'(F/V)_M_L','FontSize',12)
text(F/V - 0.0005,35.1,'(F/V)_T_r_u_e','FontSize',12)
xlabel('F/V (1/min)')
xlim([0.039 0.05])
set(gca,'YTick',[],'Box','off')
set(gca,'YColor',[1 1 1])
hold off

% Visualise 99% Marginal Confidence Interval For Parameter 2

figure(11 + run_repeat)

x = Frequentist_Model_Parameters(2,1);
y = 35;
errhigh = z_score*sqrt(Cov_Frequentist_Parameters(2,1));
errlow = z_score*sqrt(Cov_Frequentist_Parameters(2,1));
errorbar(x,y,errlow,errhigh,'horizontal','o','Color',[0.4660 0.6740 0.1880],'LineWidth',2,'MarkerFaceColor',[0.4660 0.6740 0.1880],'MarkerSize',8)
hold on
plot(k_reaction,y,'bx','LineWidth',3,'MarkerFaceColor','b','MarkerSize',10)
text(Frequentist_Model_Parameters(2,1) - 0.0004,35.15,'k_M_L','FontSize',12)
text(k_reaction - 0.0005,35.15,'k_T_r_u_e','FontSize',12)
xlabel('k (1/min)')
set(gca,'YTick',[],'Box','off')
set(gca,'YColor',[1 1 1])
hold off

% Algorithm Execution Time

fprintf('\n\tElapsed Algorithm Time: %0.4f seconds',t_end_Frequentist)

% Joint Parameter Confidence Interval
% ->  Ellipsoid Representation

figure(8 + run_repeat)
F_dist = 4.80;
p_length = length(Frequentist_Model_Parameters);

[EigenVec, EigenVal] = eig(Cov_A); % Calculate Eigenvalues And Eigenvectors

[Largest_EigenVec_Index, num] = find(EigenVal == max(max(EigenVal))); % Obtain The Index Of The Largest Eigenvector
Largest_EigenVec = EigenVec(:,Largest_EigenVec_Index);

Largest_EigenVal = max(max(EigenVal)); % Obtain The Largest Eigenvalue

if (Largest_EigenVec_Index == 1) % Obtain The Smallest Eigenvector And EigenValue
    
    Smallest_EigenVal = max(EigenVal(:,2));
    Smallest_EigenVec = EigenVec(:,2);
    
else
    
    Smallest_EigenVal = max(EigenVal(:,1));
    Smallest_EigenVec = EigenVec(:,1);
    
end

Angle = atan2(Largest_EigenVec(2),Largest_EigenVec(1)); % Calculate The Angle Between The Cartesian x-axis And The Largest Eigenvector

if Angle < 0
    
    Angle = Angle + 2*pi; % Shift Angle To Be Between 0 and 2pi
    
end

Average = Frequentist_Model_Parameters'; % Extract Mean Inferred Parameter Values

Chi_Value = sqrt(p_length*F_dist); % Obtain Radius For The Error Ellipse
Theta_Grid = linspace(0,2*pi,400000);
Kp_Mean = Average(1);
tau_Mean = Average(2);

A1 = Chi_Value*sqrt(Largest_EigenVal);
B1 = Chi_Value*sqrt(Smallest_EigenVal);

Ellipse_X_r = A1*cos(Theta_Grid); % Ellipse Cartesian x-axis Coordinates
Ellipse_Y_r = B1*sin(Theta_Grid); % Ellipse Cartesian y-axis Coordinates

R = [cos(Angle) sin(Angle); -sin(Angle) cos(Angle)]; % Ellipse Rotation Matrix

r_Ellipse = [Ellipse_X_r;Ellipse_Y_r]'*R; % Rotate Ellipse

Freq_Ellipse = plot(r_Ellipse(:,1) + Kp_Mean,r_Ellipse(:,2) + tau_Mean,'Color',[0.4660, 0.6740, 0.1880],'LineWidth',3); % Visualise Ellipse
hold on
plot(Frequentist_Model_Parameters(1,1),Frequentist_Model_Parameters(2,1),'o','Color',[0.4660, 0.6740, 0.1880],'LineWidth',2,'MarkerSize',8,'MarkerFaceColor',[0.4660, 0.6740, 0.1880])
ylabel('k (1/min)')
xlabel('F/V (1/min)')
set(gca,'Box','off')
plot(F/V,k_reaction,'bx','LineWidth',3,'MarkerFaceColor','b','MarkerSize',10)
legend('Bayesian 99% Epplise','[(F/V)_M_A_P k_M_A_P]^T','Frequentist 99% Ellispe','[(F/V)_M_L k_M_L]^T','[(F/V)_T_r_u_e k_T_r_u_e]^T','Location','SouthEast')
legend('boxoff') 
%title('99% Joint Parameter Confidence/Credibility Region')
set(get(get(Bayes_Ellipse,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
set(get(get(Freq_Ellipse,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
hold off

% Visualise The Expected Mean Response

figure(12 + run_repeat)

% -> Evaluate Covariance

Cov_y = ((Least_Squares_Objective(j + 1,1)/(N - length(Parameter_Guess))))*D*J/((J')*J)*D*(J');

Mean_Response_Range = linspace(0,Sim_Time,length(dCA_Predict_t))';
S2 = diag(Cov_y);
plot(Mean_Response_Range,CA_Model + z_score*sqrt(S2),'o','Color',[0 0 0],'MarkerSize',1.5,'MarkerFaceColor',[0 0 0]);
hold on
Lower_Limit = plot(Mean_Response_Range,CA_Model - z_score*sqrt(S2),'o','Color',[0 0 0],'MarkerSize',1.5,'MarkerFaceColor',[0 0 0]);
hold on
plot(CA_Sensor.CA_Noise_Free.Time,CA_Sensor.CA_Noise_Free.Data,'b--','LineWidth',2.5)
hold on
plot(CA_Sensor.CA_Noise.Time,CA_Sensor.CA_Noise.Data,'kx','MarkerSize',8,'LineWidth',2)
%title('CSTR Dynamic Reponse: Random Input in C_A_0')
xlabel('Time (min)')
ylabel('C_A (mole/m^3)')
hold on
plot(Mean_Response_Range,CA_Model,'Color',[0.4660, 0.6740, 0.1880],'LineWidth',2)
hold on
x_steady_state = linspace(0,Sim_Time,Sim_Time/5);
Steady = plot(x_steady_state,CA_initial.*ones(length(x_steady_state),1),'m.','LineWidth',2,'MarkerSize',10);
hold on
x_Upper_Limit = linspace(0,Sim_Time,Sim_Time);
Upper = plot(x_Upper_Limit,0.4885.*ones(length(x_Upper_Limit),1),'m--','LineWidth',2); % Upper NOC Threshold Value
hold on
x_Lower_Limit = linspace(0,Sim_Time,Sim_Time);
Lower  = plot(x_Lower_Limit,0.4420.*ones(length(x_Lower_Limit),1),'m--','LineWidth',2); % Lower NOC Threshold Value
legend('99% Upper/Lower Confidence Limit','99% EMR Lower Confidence Limit','Underlying State Variable Dynamic Response','Outlet Concentration Sensor Measurements','State Variable Dynamic Response (ML Estimate)','True Underlying Steady State','Upper/Lower NOC Limit','Lower NOC Limit','Location','SouthEast')
legend('boxoff')
set(get(get(Lower,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
set(get(get(Upper,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
set(get(get(Lower_Limit,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
set(get(get(Steady,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
axis([0 Sim_Time 0.42 0.50])
set(gca,'Box','off')
hold off